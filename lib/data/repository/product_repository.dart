import 'package:app_sstock/data/model/user_model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/shared_preference.dart';

class ProductRepository {
  static Future<ApiResponse> getListProduct() async {
    ApiResponse response = await Network.instance.post(
      url: ApiConstant.listProduct,
    );
    return response;
  }

  static Future<ApiResponse> getExpectedProfit(param) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.getExpectedProfit, params: param);
    return response;
  }

  static Future<ApiResponse> checkBorrowMaximum(param) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.checkBorrowMaximum, params: param);
    return response;
  }

  static Future<ApiResponse> getDailyProfit(param) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.dailyProfit, params: param);
    return response;
  }

  static Future<ApiResponse> getListStatus() async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.getListStatus);
    return response;
  }

  static Future<ApiResponse> getDetailProduct(id, uId) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.productDetail, params: {"pId": id, "uId": uId});
    return response;
  }

  static Future<ApiResponse> getPolicyProduct(id) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.getDocument, params: {"pId": id});
    return response;
  }

  static Future<ApiResponse> buyProduct(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.buyProduct, params: param);
    return response;
  }

  static Future<ApiResponse> getContract(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.getContract, params: param);
    return response;
  }

  static Future<ApiResponse> getProfit(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.getProfit, params: param);
    return response;
  }

  static Future<ApiResponse> withdraw(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.withdraw, params: param);
    return response;
  }

  static Future<ApiResponse> soldStock(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.soldStock, params: param);
    return response;
  }

  static Future<ApiResponse> borrow(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.borrow, params: param);
    return response;
  }

  static Future<ApiResponse> closeAccount() async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.closeAccount, params: {"cid": user.id});
    return response;
  }

  static Future<ApiResponse> listPackageUnfinished() async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.listPackageUnfinished, params: {"cid": user.id});
    return response;
  }

  static Future<ApiResponse> getListQuestion() async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.question);
    return response;
  }

  static Future<ApiResponse> cancelRequest(param) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.cancelRequest, params: param);
    return response;
  }

  static Future<ApiResponse> getPool(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.getPool, params: param);
    return response;
  }

  static Future<ApiResponse> listHistoryProduct(param) async {
    // UserModel user = await SharedPreferenceUtil.getUserInfo();
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.listHistoryProduct, params: param);
    return response;
  }

  static Future<ApiResponse> listAsset() async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.listAsset, params: {"uid": user.id});
    return response;
  }

  static Future<ApiResponse> listPackageDue(param) async {
    ApiResponse response =
        await Network.instance.post(url: ApiConstant.packageDue, params: param);
    return response;
  }

  static Future<ApiResponse> getInvestPerfomance(param) async {
    ApiResponse response = await Network.instance
        .post(url: ApiConstant.getInvestPerfomance, params: param);
    return response;
  }
}
