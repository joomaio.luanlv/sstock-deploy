import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/device_util.dart';
import 'package:app_sstock/utils/shared_preference.dart';

class NotificationRepository {
  static Future<ApiResponse> getListNofication() async {
    Map<String, dynamic> request = Map();
    request["cusId"] = (await SharedPreferenceUtil.getUserInfo()).id;
    ApiResponse response = await Network.instance.post(url: ApiConstant.getNotifies, params: request);
    return response;
  }

  static Future<ApiResponse> readListNofication(mid) async {
    Map<String, dynamic> request = Map();
    request["cusId"] = (await SharedPreferenceUtil.getUserInfo()).id;
    request["notifyId"] = mid;
    ApiResponse response = await Network.instance.post(url: ApiConstant.readListNofication, params: request);
    return response;
  }

  static Future<ApiResponse> regFirebase() async {
    String? userName = (await SharedPreferenceUtil.getUserInfo()).userName;
    String? device = await DeviceUtil.getFcmToken();
    Map<String, dynamic> request = {"userName": userName, "firebaseToken": device};
    ApiResponse response = await Network.instance.post(url: ApiConstant.updateFirebaseToken, params: request);
    return response;
  }
}
