import 'package:app_sstock/data/model/user_model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/shared_preference.dart';

class AuthRepository {
  static Future<ApiResponse> login(Map<String, dynamic>? params) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.login, params: params);
    return response;
  }

  static Future<ApiResponse> getUserInfo(int? id) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.customerInfo, params: {
      "cid": id,
    });
    return response;
  }

  static Future<ApiResponse> updateUserInfo(Map<String, dynamic> params) async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    params["uid"] = user.id;
    ApiResponse response = await Network.instance.post(url: ApiConstant.updateInformation, params: params);
    return response;
  }

  static Future<ApiResponse> CreateOrEditOfCusBankInfo(Map<String, dynamic> params) async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    params["cid"] = user.id;
    ApiResponse response = await Network.instance.post(url: ApiConstant.createOrEditOfCusBankInfo, params: params);
    return response;
  }

  static Future<ApiResponse> register(Map<String, dynamic>? params) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.register, params: params);
    return response;
  }

  static Future<ApiResponse> resetPasword(Map<String, dynamic>? params) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.resetPasword, params: params);
    return response;
  }

  static Future<ApiResponse> sendOTP(Map<String, dynamic>? params) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.sendOTP, params: params);
    return response;
  }
  static Future<ApiResponse> logout({String? userName}) async {
    ApiResponse response = await Network.instance.post(url: ApiConstant.logout, params: {
      "username":userName
    });
    return response;
  }
}
