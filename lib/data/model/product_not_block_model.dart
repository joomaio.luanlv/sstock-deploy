class ProductNotBlockModel {
  ProductNotBlockModel({
    this.id,
    this.name,
    this.logo,
    this.originValue
  });

  final int? id;
  final String? name;
  final int? originValue;
  final String? logo;

  factory ProductNotBlockModel.fromMap(Map<String, dynamic> json) => ProductNotBlockModel(
        id: json["Id"] == null ? null : json["Id"],
        originValue: json["OriginValue"] == null ? null : json["OriginValue"],
        name: json["Name"] == null ? null : json["Name"],
        logo: json["Image"] == null ? null : json["Image"],
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "OriginValue": originValue == null ? null : originValue,
        "Name": name == null ? null : name,
        "Image": logo == null ? null : logo,
      };
}