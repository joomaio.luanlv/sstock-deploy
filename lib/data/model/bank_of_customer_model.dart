class BankOfCustomerModel {
  int? id;
  String? bankName;
  String? accountNumber;
  String? accountName;
  bool? active;
  bool? isDefault;
  int? bankOnline_Id;

  static List<BankOfCustomerModel> listFromMap(str) =>
      List<BankOfCustomerModel>.from(
          (str).map((x) => BankOfCustomerModel.fromJson(x)));

  BankOfCustomerModel(
      {this.id,
      this.bankName,
      this.accountNumber,
      this.accountName,
      this.active,
      this.isDefault,
      this.bankOnline_Id});

  factory BankOfCustomerModel.fromMap(
          Map<String, dynamic> json) =>
      BankOfCustomerModel(
          id: json["Id"] == null ? null : json["Id"],
          bankName: json["BankName"] == null ? null : json["BankName"],
          accountNumber:
              json["AccountNumber"] == null ? null : json["AccountNumber"],
          accountName: json["AccountName"] == null ? null : json["AccountName"],
          active: json["Active"] == null ? null : json["Active"],
          isDefault: json["IsDefault"] == null ? null : json["IsDefault"],
          bankOnline_Id:
              json["BankOnline_Id"] == null ? null : json["BankOnline_Id"]);

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "BankName": bankName == null ? null : bankName,
        "AccountNumber": accountNumber == null ? null : accountNumber,
        "AccountName": accountName == null ? null : accountName,
        "Active": active == null ? null : active,
        "IsDefault": isDefault == null ? null : isDefault,
        "BankOnline_Id": bankOnline_Id == null ? null : bankOnline_Id,
      };

  BankOfCustomerModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    bankName = json['BankName'];
    accountNumber = json['AccountNumber'];
    accountName = json['AccountName'];
    active = json['Active'];
    isDefault = json['IsDefault'];
    bankOnline_Id = json["BankOnline_Id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['BankName'] = this.bankName;
    data['AccountNumber'] = this.accountNumber;
    data['AccountName'] = this.accountName;
    data['Active'] = this.active;
    data['IsDefault'] = this.isDefault;
    data['BankOnline_Id'] = this.bankOnline_Id;
    return data;
  }
}
