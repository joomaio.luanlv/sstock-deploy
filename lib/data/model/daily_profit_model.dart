import 'dart:convert';

class DailyProfitModel {
  DailyProfitModel({
    this.interestDay,
    this.day,
    this.month,
    this.year,
    this.interestNum,
    this.date,
    this.interestValue,
    this.vnIndex
  });

  final String? interestDay;
  final int? day;
  final int? month;
  final int? year;
  final DateTime? date;
  final String? interestNum;
  final double? interestValue;
  final bool? vnIndex;

  static List<DailyProfitModel> listFromMap(str) => List<DailyProfitModel>.from(
      (str).map((x) => DailyProfitModel.fromMap(x)));

  static String listToMap(List<DailyProfitModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory DailyProfitModel.fromMap(Map<String, dynamic> json) =>
      DailyProfitModel(
        interestDay: json["InterestDay"] == null ? null : json["InterestDay"],
        day: json["Day"] == null ? null : json["Day"],
        date: new DateTime((json["Year"] ?? 2022),(json["Month"] ?? 01) , (json["Day"] ?? 01)),
        interestNum: json["InterestNum"] == null ? null : json["InterestNum"],
        interestValue: json["InterestValue"] == null ? null : json["InterestValue"].toDouble(),
        vnIndex: json["vnIndex"] == null ? null : json["vnIndex"],
      );

  Map<String, dynamic> toMap() => {
        "InterestDay": interestDay == null ? null : interestDay,
        "Day": day == null ? null : day,
        "Date": date == null ? null : date,
        "InterestNum": interestNum == null ? null : interestNum,
        "InterestValue": interestValue == null ? null : interestValue,
        "vnIndex": vnIndex == null ? null : vnIndex,
      };
}
