// To parse this JSON data, do
//
//     final policyModel = policyModelFromMap(jsonString);

import 'dart:convert';

import 'package:app_sstock/constants.dart';
import 'package:app_sstock/utils/common.dart';

class PolicyModel {
  PolicyModel({
    this.id,
    this.name,
    this.description,
    this.createDate,
    this.linkDownload,
    this.active,
    this.createDateFormated,
  });

  final int? id;
  final String? name;
  final String? description;
  final String? createDate;
  final String? linkDownload;
  final bool? active;
  final DateTime? createDateFormated;

  static List<PolicyModel> listFromMap(str) => List<PolicyModel>.from((str).map((x) => PolicyModel.fromMap(x)));

  static String policyModelToMap(List<PolicyModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory PolicyModel.fromMap(Map<String, dynamic> json) => PolicyModel(
        id: json["Id"] == null ? null : json["Id"],
        name: json["Name"] == null ? null : json["Name"],
        description: json["Description"],
        createDate: json["CreateDate"] == null ? null : json["CreateDate"],
        linkDownload: json["LinkDownload"] == null ? null : json["LinkDownload"],
        active: json["Active"] == null ? null : json["Active"],
        createDateFormated: json["CreateDateFormated"] == null
            ? null
            : Common.parserDate(json["CreateDateFormated"], format: FormatDate.full),
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "Name": name == null ? null : name,
        "Description": description,
        "CreateDate": createDate == null ? null : createDate,
        "LinkDownload": linkDownload == null ? null : linkDownload,
        "Active": active == null ? null : active,
        "CreateDateFormated": createDateFormated == null ? null : createDateFormated,
      };
}
