class RegisterUserModel {
  String? fullName;
  String? otp;
  String? mobilePhone;
  String? email;
  String? password;
  String? username;
  String? parentcode;
  String? identityNumber;

  RegisterUserModel();

  RegisterUserModel.fromJson(Map<String, dynamic> json) {
    fullName = json['fullName'];
    otp = json['otp'];
    mobilePhone = json['mobilePhone'];
    email = json['email'];
    password = json['password'];
    username = json['username'];
    parentcode = json['parentcode'];
    identityNumber = json['identityNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fullName'] = this.fullName;
    data['otp'] = this.otp;
    data['mobilePhone'] = this.mobilePhone;
    data['email'] = this.email;
    data['password'] = this.password;
    data['username'] = this.username;
    data['parentcode'] = this.parentcode;
    data['identityNumber'] = this.identityNumber;
    return data;
  }
}
