import 'dart:convert';

class QuestionModel {
  QuestionModel({
    this.question,
    this.answer
  });

  final String? question;
  final String? answer;

  static List<QuestionModel> listFromMap(str) => List<QuestionModel>.from((str).map((x) => QuestionModel.fromMap(x)));

  static String listToMap(List<QuestionModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory QuestionModel.fromMap(Map<String, dynamic> json) => QuestionModel(
        question: json["Question"] == null ? null : json["Question"],
        answer: json["Answer"] == null ? null : json["Answer"],
      );

  Map<String, dynamic> toMap() => {
        "Question": question == null ? null : question,
        "Answer": answer == null ? null : answer,
      };
}