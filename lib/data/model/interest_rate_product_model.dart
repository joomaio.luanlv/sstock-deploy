// To parse this JSON data, do
//
//     final interestRateProductModel = interestRateProductModelFromMap(jsonString);

class InterestRateProductModel {
  InterestRateProductModel({
    this.key,
    this.colorCode,
    this.items,
    this.banks,
  });

  final String? key;
  final String? colorCode;
  final List<InterestRate>? items;
  final List<Bank>? banks;

  static List<InterestRateProductModel> listFromMap(str) =>
      List<InterestRateProductModel>.from(
          (str).map((x) => InterestRateProductModel.fromMap(x)));

  factory InterestRateProductModel.fromMap(Map<String, dynamic> json) =>
      InterestRateProductModel(
        key: json["Key"] == null ? null : json["Key"],
        colorCode: json["ColorCode"] == null ? null : json["ColorCode"],
        items: json["Items"] == null
            ? null
            : List<InterestRate>.from(
                json["Items"].map((x) => InterestRate.fromMap(x))),
        banks: json["Banks"] == null
            ? null
            : List<Bank>.from(json["Banks"].map((x) => Bank.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "Key": key == null ? null : key,
        "ColorCode": colorCode == null ? null : colorCode,
        "Items": items == null
            ? null
            : List<dynamic>.from(items!.map((x) => x.toMap())),
        "Banks": banks == null
            ? null
            : List<dynamic>.from(banks!.map((x) => x.toMap())),
      };
}

class Bank {
  Bank({
    this.period,
    this.bankIr,
    this.irid,
  });

  final int? period;
  final double? bankIr;
  final int? irid;

  factory Bank.fromMap(Map<String, dynamic> json) => Bank(
        period: json["Period"] == null ? null : json["Period"],
        bankIr: json["BankIR"] == null ? null : json["BankIR"].toDouble(),
        irid: json["irid"] == null ? null : json["irid"],
      );

  Map<String, dynamic> toMap() => {
        "Period": period == null ? null : period,
        "BankIR": bankIr == null ? null : bankIr,
        "irid": irid == null ? null : irid,
      };
}

class InterestRate {
  InterestRate({
    this.period,
    this.interestRate,
    this.irid,
    this.addIR,
    this.stockList,
    this.periodType,
  });

  final String? stockList;
  int? period;
  final double? interestRate;
  final double? addIR;
  final int? irid;
  final String? periodType;

  factory InterestRate.fromMap(Map<String, dynamic> json) => InterestRate(
        stockList: json["StockList"] == null ? null : json["StockList"],
        period: json["Period"] == null ? null : json["Period"],
        interestRate: json["InterestRate"] == null
            ? null
            : json["InterestRate"].toDouble(),
        addIR: json["AddIR"] == null ? null : json["AddIR"].toDouble(),
        irid: json["irid"] == null ? null : json["irid"],
        periodType: json["PeriodType"] == null ? null : json["PeriodType"],
      );

  Map<String, dynamic> toMap() => {
        "StockList": stockList,
        "Period": period == null ? null : period,
        "InterestRate": interestRate == null ? null : interestRate,
        "AddIR": addIR == null ? null : addIR,
        "irid": irid == null ? null : irid,
        "PeriodType": periodType == null ? null : periodType,
      };
}

class ChartInfoModel {
  ChartInfoModel({
    this.colorCode,
    this.key,
    this.items,
  });

  final String? colorCode;
  final String? key;
  final List<DataChartTime>? items;

  static List<ChartInfoModel> listFromMap(str) =>
      List<ChartInfoModel>.from((str).map((x) => ChartInfoModel.fromMap(x)));

  factory ChartInfoModel.fromMap(Map<String, dynamic> json) => ChartInfoModel(
      colorCode:
          json["ColorCode"] == null ? null : json["ColorCode"].toString(),
      key: json["Key"] == null ? null : json["Key"].toString(),
      items: json["Items"] == null
          ? null
          : List<DataChartTime>.from(
              json["Items"].map((x) => DataChartTime.fromMap(x))));

  Map<String, dynamic> toMap() => {
        "ColorCode": colorCode == null ? null : colorCode,
        "Key": key == null ? null : key,
        "Items": items == null
            ? null
            : List<dynamic>.from(items!.map((x) => x.toMap()))
      };
}

class DataChartTime {
  final DateTime? time;
  final double? value;

  DataChartTime({
    this.time,
    this.value,
  });

  static List<DataChartTime> listFromMap(str) =>
      List<DataChartTime>.from((str).map((x) => DataChartTime.fromMap(x)));

  factory DataChartTime.fromMap(Map<String, dynamic> json) => DataChartTime(
        time: json["Time"] == null ? null : DateTime.parse(json["Time"]),
        value: json["Value"] == null ? null : json["Value"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "Time": time == null ? null : time,
        "Value": value == null ? null : value,
      };
}
