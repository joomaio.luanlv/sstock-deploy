// To parse this JSON data, do
//
//     final productModel = productModelFromMap(jsonString);

import 'dart:convert';

import 'package:app_sstock/data/model/model.dart';

class ProductModel {
  ProductModel({
    this.id = -1,
    this.catId,
    this.name,
    this.sort,
    this.appDescription,
    this.description,
    this.logo,
    this.StockList,
    this.active = false,
    this.interestRates,
    this.productCat,
    this.colorCode,
    this.isBorrow,
    this.interestRateProductModel,
    this.interestBorrows,
    this.stockPrices,
    this.isIndefinite,
    this.productNotBlocks
  });

  final int id;
  final int? catId;
  final String? name;
  final int? sort;
  final dynamic appDescription;
  final String? description;
  final String? logo;
  final String? StockList;
  final bool active;
  final List<dynamic>? interestRates;
  final List<InterestBorrowModel>? interestBorrows;
  final List<StockPriceModel>? stockPrices;
  final List<ProductNotBlockModel>? productNotBlocks;
  final dynamic productCat;
  final String? colorCode;
  final bool? isBorrow;
  final bool? isIndefinite;
  InterestRateProductModel? interestRateProductModel;

  static List<ProductModel> listFromMap(str) => List<ProductModel>.from((str).map((x) => ProductModel.fromMap(x)));

  static String listToMap(List<ProductModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory ProductModel.fromMap(Map<String, dynamic> json) => ProductModel(
        id: json["Id"] == null ? null : json["Id"],
        catId: json["Cat_Id"] == null ? null : json["Cat_Id"],
        name: json["Name"] == null ? null : json["Name"],
        sort: json["Sort"] == null ? null : json["Sort"],
        appDescription: json["AppDescription"],
        description: json["Description"] == null ? null : json["Description"],
        StockList: json["StockList"] == null ? null : json["StockList"],
        logo: json["Logo"] == null ? null : json["Logo"],
        active: json["Active"] == null ? null : json["Active"],
        isBorrow: json["IsBorrow"] == null ? null : json["IsBorrow"],
        isIndefinite: json["IsIndefinite"] == null ? null : json["IsIndefinite"],
        colorCode: json["ColorCode"] == null ? null : json["ColorCode"],
        interestRates: json["InterestRates"] == null ? null : List<dynamic>.from(json["InterestRates"].map((x) => x)),
        interestBorrows: json["InterestBorrows"] == null ? null : List<InterestBorrowModel>.from(json["InterestBorrows"].map((x) => InterestBorrowModel.fromMap(x))),
        stockPrices: json["StockPrices"] == null ? null : List<StockPriceModel>.from(json["StockPrices"].map((x) => StockPriceModel.fromMap(x))),
        productNotBlocks: json["ProductNotBlocks"] == null ? null : List<ProductNotBlockModel>.from(json["ProductNotBlocks"].map((x) => ProductNotBlockModel.fromMap(x))),
        productCat: json["ProductCat"],
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "Cat_Id": catId == null ? null : catId,
        "Name": name == null ? null : name,
        "Sort": sort == null ? null : sort,
        "AppDescription": appDescription,
        "Description": description == null ? null : description,
        "StockList": StockList == null ? null : StockList,
        "Logo": logo == null ? null : logo,
        "Active": active == null ? null : active,
        "IsBorrow": isBorrow == null ? null : isBorrow,
        "IsIndefinite": isIndefinite == null ? null : isIndefinite,
        "InterestRates": interestRates == null ? null : List<dynamic>.from(interestRates!.map((x) => x)),
        "InterestBorrows": interestBorrows == null ? null : List<InterestBorrowModel>.from(interestBorrows!.map((x) => x.toMap())),
        "StockPrices": stockPrices == null ? null : List<StockPriceModel>.from(stockPrices!.map((x) => x.toMap())),
        "ProductNotBlocks": productNotBlocks == null ? null : List<ProductNotBlockModel>.from(productNotBlocks!.map((x) => x.toMap())),
        "ProductCat": productCat,
      };
}
