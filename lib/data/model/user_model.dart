// To parse this JSON data, do
//
//     final userModel = userModelFromMap(jsonString);

import 'dart:convert';

import 'model.dart';

class UserModel {
  UserModel({
    required this.id,
    this.fullName,
    this.identityNumber,
    this.address,
    this.cccd,
    this.provideDate,
    this.provideDateFormated,
    this.providePlace,
    this.bankName,
    this.accountNumber,
    this.accountName,
    this.mobilePhone,
    this.landPhone,
    this.email,
    this.work,
    this.office,
    this.userName,
    this.passwordHash,
    this.securityStamp,
    this.disable,
    this.pinEnable,
    this.pinHash,
    this.deviceName,
    this.deviceModel,
    this.referralCode,
    this.parentReferralCode,
    this.allowChangeDevice,
    this.createDate,
    this.bankOfCustomers,
    this.customersProducts,
    this.notifications,
  });

  final int? id;
  final String? fullName;
  final dynamic identityNumber;
  final dynamic address;
  final dynamic cccd;
  final dynamic provideDate;
  final dynamic provideDateFormated;
  final dynamic providePlace;
  final String? mobilePhone;
  final String? bankName;
  final String? accountNumber;
  final String? accountName;
  final dynamic landPhone;
  final String? email;
  final dynamic work;
  final dynamic office;
  final String? userName;
  final String? passwordHash;
  final dynamic securityStamp;
  final bool? disable;
  final bool? pinEnable;
  final dynamic pinHash;
  final dynamic deviceName;
  final dynamic deviceModel;
  final String? referralCode;
  final String? parentReferralCode;
  final bool? allowChangeDevice;
  final String? createDate;
  final List<BankOfCustomerModel>? bankOfCustomers;
  final List<dynamic>? customersProducts;
  final List<dynamic>? notifications;

  static UserModel userModelFromMap(str) => UserModel.fromMap(str);

  static String userModelToMap(UserModel data) => json.encode(data.toMap());

  factory UserModel.fromMap(Map<String, dynamic> json) => UserModel(
        id: json["Id"] == null ? null : json["Id"],
        fullName: json["FullName"] == null ? null : json["FullName"],
        identityNumber: json["IdentityNumber"],
        address: json["Address"],
        cccd: json["CCCD"],
        bankName: json["BankName"],
        accountNumber: json["AccountNumber"],
        accountName: json["AccountName"],
        provideDate: json["ProvideDate"],
        provideDateFormated: json["ProvideDateFormated"],
        providePlace: json["ProvidePlace"],
        mobilePhone: json["MobilePhone"] == null ? null : json["MobilePhone"],
        landPhone: json["LandPhone"],
        email: json["Email"] == null ? null : json["Email"],
        work: json["Work"],
        office: json["Office"],
        userName: json["UserName"] == null ? null : json["UserName"],
        passwordHash:
            json["PasswordHash"] == null ? null : json["PasswordHash"],
        securityStamp: json["SecurityStamp"],
        disable: json["Disable"] == null ? null : json["Disable"],
        pinEnable: json["PINEnable"] == null ? null : json["PINEnable"],
        pinHash: json["PINHash"],
        deviceName: json["DeviceName"],
        deviceModel: json["DeviceModel"],
        referralCode:
            json["ReferralCode"] == null ? null : json["ReferralCode"],
        parentReferralCode: json["ParentReferralCode"] == null
            ? null
            : json["ParentReferralCode"],
        allowChangeDevice: json["AllowChangeDevice"] == null
            ? null
            : json["AllowChangeDevice"],
        createDate: json["CreateDate"] == null ? null : json["CreateDate"],
        bankOfCustomers: json["BankOfCustomers"] == null
            ? null
            : List<BankOfCustomerModel>.from(json["BankOfCustomers"]
                .map((x) => BankOfCustomerModel.fromMap(x))),
        customersProducts: json["Customers_Products"] == null
            ? null
            : List<dynamic>.from(json["Customers_Products"].map((x) => x)),
        notifications: json["Notifications"] == null
            ? null
            : List<dynamic>.from(json["Notifications"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "FullName": fullName == null ? null : fullName,
        "IdentityNumber": identityNumber,
        "Address": address,
        "CCCD": cccd,
        "ProvideDate": provideDate,
        "ProvideDateFormated": provideDateFormated,
        "ProvidePlace": providePlace,
        "BankName": bankName,
        "AccountNumber": accountNumber,
        "AccountName": accountName,
        "MobilePhone": mobilePhone == null ? null : mobilePhone,
        "LandPhone": landPhone,
        "Email": email == null ? null : email,
        "Work": work,
        "Office": office,
        "UserName": userName == null ? null : userName,
        "PasswordHash": passwordHash == null ? null : passwordHash,
        "SecurityStamp": securityStamp,
        "Disable": disable == null ? null : disable,
        "PINEnable": pinEnable == null ? null : pinEnable,
        "PINHash": pinHash,
        "DeviceName": deviceName,
        "DeviceModel": deviceModel,
        "ReferralCode": referralCode == null ? null : referralCode,
        "ParentReferralCode":
            parentReferralCode == null ? null : parentReferralCode,
        "AllowChangeDevice":
            allowChangeDevice == null ? null : allowChangeDevice,
        "CreateDate": createDate == null ? null : createDate,
        // "BankOfCustomers": bankOfCustomers == null ? null : List<BankOfCustomerModel>.from(bankOfCustomers!.map((x) => x.toMap())),
        "Customers_Products": customersProducts == null
            ? null
            : List<dynamic>.from(customersProducts!.map((x) => x)),
        "Notifications": notifications == null
            ? null
            : List<dynamic>.from(notifications!.map((x) => x)),
      };
}
