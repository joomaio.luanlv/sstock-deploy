// To parse this JSON data, do
//
//     final PackageDueModel = PackageDueModelFromMap(jsonString);

import 'dart:convert';

import 'package:app_sstock/constants.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';

class PackageDueModel {
  PackageDueModel({
    this.id,
    this.customerId,
    this.interestRateId,
    this.startDate,
    this.endDate,
    this.active,
    this.originValue,
    this.description,
    this.interestRate,
    this.customer,
    this.startDateFormated,
    this.endDateFormated,
    this.productName,
    this.iRate,
    this.profit,
    this.profitComplete,
    this.actionType,
    this.status,
    this.colorCode,
    this.statusFull,
    this.total
  });
  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành
  static const int STATUS_CANCEL = 1;
  static const int STATUS_DONE = 3;
  static const int STATUS_INPROGRESS = 2;
  static const int STATUS_PENDING = 0;
  final int? id;
  final int? customerId;
  final int? interestRateId;
  final String? startDate;
  final String? endDate;
  final bool? active;
  final String? originValue;
  final String? description;
  final dynamic? interestRate;
  final dynamic? customer;
  final DateTime? startDateFormated;
  final DateTime? endDateFormated;
  final String? productName;
  final double? iRate;
  final String? profit;
  final String? profitComplete;
  final String? colorCode;
  final String? total;

  // 0: vẫn đang chạy, 1 : hủy, 2: hoàn thành
  final int? status;
  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành
  final int? statusFull;

  //0: đầu tư, 1: vay
  final int? actionType;

  static List<PackageDueModel> listFromMap(str) => List<PackageDueModel>.from((str).map((x) => PackageDueModel.fromMap(x)));

  static String packageDueModelToMap(List<PackageDueModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory PackageDueModel.fromMap(Map<String, dynamic> json) => PackageDueModel(
        id: json["Id"] == null ? null : json["Id"],
        customerId: json["Customer_Id"] == null ? null : json["Customer_Id"],
        interestRateId: json["InterestRate_Id"] == null ? null : json["InterestRate_Id"],
        startDate: json["StartDate"] == null ? null : json["StartDate"],
        endDate: json["EndDate"] == null ? null : json["EndDate"],
        active: json["Active"] == null ? null : json["Active"],
        originValue: json["OriginValue"] == null ? null : json["OriginValue"].toString(),
        description: json["Description"] == null ? null : json["Description"],
        interestRate: json["InterestRate"],
        customer: json["Customer"],
        startDateFormated: json["StartDateFormated"] == null
            ? null
            : Common.parserDate(json["StartDateFormated"], format: FormatDate.full),
        endDateFormated: json["EndDateFormated"] == null
            ? null
            : Common.parserDate(json["EndDateFormated"], format: FormatDate.full),
        productName: json["ProductName"] == null ? null : json["ProductName"],
        iRate: json["IRate"] == null ? null : json["IRate"].toDouble(),
        profit: json["Profit"] == null ? null : json["Profit"].toString(),
        profitComplete: json["ProfitComplete"] == null ? null : json["ProfitComplete"].toString(),
        actionType: json["ActionType"] == null ? null : json["ActionType"],
        status: json["Status"] == null ? null : json["Status"],
        statusFull: json["StatusFull"] == null ? null : Common.strToInt(json["StatusFull"]?.toString()),
        colorCode: json["ColorCode"] == null ? null : json["ColorCode"],
        total: json["Total"] == null ? null : json["Total"],
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "Customer_Id": customerId == null ? null : customerId,
        "InterestRate_Id": interestRateId == null ? null : interestRateId,
        "StartDate": startDate == null ? null : startDate,
        "EndDate": endDate == null ? null : endDate,
        "Active": active == null ? null : active,
        "OriginValue": originValue == null ? null : originValue,
        "Description": description == null ? null : description,
        "InterestRate": interestRate,
        "Customer": customer,
        "StartDateFormated": startDateFormated == null ? null : startDateFormated?.toIso8601String(),
        "EndDateFormated": endDateFormated == null ? null : endDateFormated?.toIso8601String(),
        "ProductName": productName == null ? null : productName,
        "IRate": iRate == null ? null : iRate,
        "Profit": profit == null ? null : profit,
        "ProfitComplete": profitComplete == null ? null : profitComplete,
        "ActionType": actionType == null ? null : actionType,
        "Status": status == null ? null : status,
        "StatusFull": statusFull == null ? null : statusFull,
        "ColorCode": colorCode == null ? null : colorCode,
        "Total": total == null ? null : total,
      };

  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành
  Map<String, dynamic> getValueState() {
    String title = "";
    Color color = Colors.transparent;
    switch (statusFull) {
      case STATUS_CANCEL:
        title = "Đã huỷ";
        color = AppColors.ffff2e00;
        break;
      case STATUS_DONE:
        title = actionType == 0 ? "Hoàn thành" : "Đã tất toán";
        color = AppColors.ff00A95E;
        break;
      case STATUS_PENDING:
        title = "Chờ duyệt";
        color = AppColors.ff258EFF;
        break;
      case STATUS_INPROGRESS:
        title = actionType == 0 ? "Đang đầu tư" : "Đang vay";
        color = AppColors.ffFFA11A;
        break;
    }
    return {
      "title": title,
      "color": color,
    };
  }
}