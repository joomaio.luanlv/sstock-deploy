class BankInfoModel {
  int? id;
  String? name;
  String? code;
  String? bin;
  String? shortName;
  String? logo;
  int? transferSupported;
  int? lookupSupported;
  int? support;
  int? isTransfer;
  String? swiftCode;

  static List<BankInfoModel> listFromMap(str) =>
      List<BankInfoModel>.from((str).map((x) => BankInfoModel.fromJson(x)));

  BankInfoModel(
      {this.id,
      this.name,
      this.code,
      this.bin,
      this.shortName,
      this.logo,
      this.transferSupported,
      this.lookupSupported,
      this.support,
      this.isTransfer,
      this.swiftCode});

  BankInfoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    code = json['code'];
    bin = json['bin'];
    shortName = json['shortName'];
    logo = json['logo'];
    transferSupported = json['transferSupported'];
    lookupSupported = json['lookupSupported'];
    shortName = json['short_name'];
    support = json['support'];
    isTransfer = json['isTransfer'];
    swiftCode = json['swift_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['code'] = this.code;
    data['bin'] = this.bin;
    data['shortName'] = this.shortName;
    data['logo'] = this.logo;
    data['transferSupported'] = this.transferSupported;
    data['lookupSupported'] = this.lookupSupported;
    data['short_name'] = this.shortName;
    data['support'] = this.support;
    data['isTransfer'] = this.isTransfer;
    data['swift_code'] = this.swiftCode;
    return data;
  }
}
