class StockPriceModel {
  StockPriceModel({
    this.stockCode,
    this.giaThiTruong,
    this.giaCamCo,
  });

  final String? stockCode;
  final double? giaThiTruong;
  final double? giaCamCo;

  factory StockPriceModel.fromMap(Map<String, dynamic> json) => StockPriceModel(
        stockCode: json["StockCode"] == null ? null : json["StockCode"],
        giaCamCo: json["GiaCamCo"] == null ? null : json["GiaCamCo"].toDouble(),
        giaThiTruong: json["GiaThiTruong"] == null ? null : json["GiaThiTruong"].toDouble(),
      );

  Map<String, dynamic> toMap() => {
        "StockCode": stockCode == null ? null : stockCode,
        "GiaCamCo": giaThiTruong == null ? null : giaThiTruong,
        "GiaThiTruong": giaCamCo == null ? null : giaCamCo,
      };
}