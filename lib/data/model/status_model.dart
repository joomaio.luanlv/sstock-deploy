import 'dart:convert';

class StatusModel {
  StatusModel({
    this.status,
    this.statusText,
  });

  final int? status;
  final String? statusText;

  static List<StatusModel> listFromMap(str) =>
      List<StatusModel>.from((str).map((x) => StatusModel.fromMap(x)));

  static String listToMap(List<StatusModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory StatusModel.fromMap(Map<String, dynamic> json) => StatusModel(
        status: json["Status"] == null ? null : json["Status"],
        statusText: json["StatusText"] == null ? null : json["StatusText"],
      );

  Map<String, dynamic> toMap() => {
        "Status": status == null ? null : status,
        "StatusText": statusText == null ? null : statusText,
      };
}