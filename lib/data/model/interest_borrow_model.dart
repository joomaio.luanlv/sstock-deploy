class InterestBorrowModel {
  InterestBorrowModel(
      {this.id, this.name, this.rateBorrow, this.percentBorrow});

  final int? id;
  final String? name;
  final double? rateBorrow;
  final double? percentBorrow;

  factory InterestBorrowModel.fromMap(
          Map<String, dynamic> json) =>
      InterestBorrowModel(
          id: json["Id"] == null ? null : json["Id"],
          name: json["Name"] == null ? null : json["Name"],
          rateBorrow:
              json["RateBorrow"] == null ? null : json["RateBorrow"].toDouble(),
          percentBorrow: json["PercentBorrow"] == null
              ? null
              : json["PercentBorrow"].toDouble());

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "Name": name == null ? null : name,
        "RateBorrow": rateBorrow == null ? null : rateBorrow,
        "PercentBorrow": percentBorrow == null ? null : percentBorrow,
      };
}
