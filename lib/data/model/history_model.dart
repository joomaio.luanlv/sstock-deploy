// To parse this JSON data, do
//
//     final historyModel = historyModelFromMap(jsonString);

import 'dart:convert';

import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:darq/darq.dart';
import 'package:flutter/material.dart';

class HistoryModel {
  HistoryModel({
    this.id,
    this.customerId,
    this.interestRateId,
    this.startDate,
    this.endDate,
    this.active,
    this.originValue,
    this.description,
    this.interestRate,
    this.customer,
    this.productName,
    this.iRate,
    this.profit,
    this.actionType,
    this.status,
    this.colorCode,
    this.statusFull,
    this.total,
    this.statusText = "",
    this.sold,
    this.withdraw,
    this.statusColor,
    this.isCancel,
    this.contractLink
  });
  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành
  static const int STATUS_CANCEL = -1;
  static const int STATUS_DONE = 3;
  static const int STATUS_INPROGRESS = 2;
  static const int STATUS_PENDING = 0;
  final int? id;
  final bool? sold; 
  final bool? withdraw; 
  final bool? isCancel; 
  final int? customerId;
  final int? interestRateId;
  final String? startDate;
  final String? endDate;
  final String statusText;
  final bool? active;
  final String? originValue;
  final String? description;
  final dynamic? interestRate;
  final dynamic? customer;
  final String? productName;
  final String? statusColor;
  final double? iRate;
  final String? profit;
  final String? colorCode;
  final String? total;
  final String? contractLink;

  // 0: vẫn đang chạy, 1 : hủy, 2: hoàn thành
  final int? status;
  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành
  final int? statusFull;

  //0: đầu tư, 1: vay
  final int? actionType;

  static List<HistoryModel> listFromMap(str) =>
      List<HistoryModel>.from((str).map((x) => HistoryModel.fromMap(x)));

  static String historyModelToMap(List<HistoryModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory HistoryModel.fromMap(Map<String, dynamic> json) => HistoryModel(
        id: json["Id"] == null ? null : json["Id"],
        contractLink: json["ContractLink"] == null ? null : json["ContractLink"],
        customerId: json["Customer_Id"] == null ? null : json["Customer_Id"],
        interestRateId:
            json["InterestRate_Id"] == null ? null : json["InterestRate_Id"],
        startDate: json["StartDate"] == null ? null : json["StartDate"],
        endDate: json["EndDate"] == null ? null : json["EndDate"],
        active: json["Active"] == null ? null : json["Active"],
        sold: json["Sold"] == null ? null : json["Sold"],
        withdraw: json["Withdraw"] == null ? null : json["Withdraw"],
        isCancel: json["IsCancel"] == null ? null : json["IsCancel"],
        originValue:
            json["OriginValue"] == null ? null : json["OriginValue"].toString(),
        description: json["Description"] == null ? null : json["Description"],
        statusText: json["StatusText"] == null ? null : json["StatusText"],
        interestRate: json["InterestRate"],
        customer: json["Customer"],
        productName: json["ProductName"] == null ? null : json["ProductName"],
        statusColor: json["StatusColor"] == null ? null : json["StatusColor"],
        iRate: json["IRate"] == null ? null : json["IRate"].toDouble(),
        profit: json["Profit"] == null ? null : json["Profit"].toString(),
        actionType: json["ActionType"] == null ? null : json["ActionType"],
        status: json["Status"] == null ? null : json["Status"],
        statusFull: json["StatusFull"] == null
            ? null
            : Common.strToInt(json["StatusFull"]?.toString()),
        colorCode: json["ColorCode"] == null ? null : json["ColorCode"],
        total: json["Total"] == null ? null : json["Total"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "Id": id == null ? null : id,
        "Customer_Id": customerId == null ? null : customerId,
        "ContractLink": contractLink == null ? null : contractLink,
        "InterestRate_Id": interestRateId == null ? null : interestRateId,
        "StartDate": startDate == null ? null : startDate,
        "EndDate": endDate == null ? null : endDate,
        "Active": active == null ? null : active,
        "Withdraw": withdraw == null ? null : withdraw,
        "IsCancel": isCancel == null ? null : isCancel,
        "Sold": sold == null ? null : sold,
        "OriginValue": originValue == null ? null : originValue,
        "Description": description == null ? null : description,
        "StatusText": statusText == null ? null : statusText,
        "InterestRate": interestRate,
        "Customer": customer,
        "ProductName": productName == null ? null : productName,
        "StatusColor": statusColor == null ? null : statusColor,
        "IRate": iRate == null ? null : iRate,
        "Profit": profit == null ? null : profit,
        "ActionType": actionType == null ? null : actionType,
        "Status": status == null ? null : status,
        "StatusFull": statusFull == null ? null : statusFull,
        "ColorCode": colorCode == null ? null : colorCode,
        "Total": total == null ? null : total,
      };


  static List<String> getStatusList(List<HistoryModel> list) {
    var statusList = list.distinct((e) => e.statusText).select((element, index) => element.statusText).toList();
    statusList.insert(0, "Tất cả");
    return statusList;
  }
  // 0: chờ duyệt, 1 từ chối, 2 đã duyệt, 3 hoàn thành, 4 chờ duyệt rút, 6 chờ duyệt bán
  Map<String, dynamic> getValueState() {
    String title = "";
    Color color = Colors.transparent;
    switch (statusFull) {
      case STATUS_CANCEL:
        title = "Đã huỷ";
        color = AppColors.ffff2e00;
        break;
      case STATUS_DONE:
        title = actionType == 0 ? "Hoàn thành" : "Đã tất toán";
        color = AppColors.ff00A95E;
        break;
      case STATUS_PENDING:
        title = "Chờ duyệt";
        color = AppColors.ff258EFF;
        break;
      case STATUS_INPROGRESS:
        title = actionType == 0 ? "Đang đầu tư" : "Đang vay";
        color = AppColors.ffFFA11A;
        break;
      case 4:
        title = "Chờ duyệt rút";
        color = AppColors.ffFFA11A;
        break;
      case 6:
        title = "Chờ duyệt bán";
        color = AppColors.ffFFA11A;
        break;
    }
    return {
      "title": title,
      "color": color,
    };
  }
}

class AssetModel {
  AssetModel({this.total, this.stockCode, this.totalFormat, this.totalInterestFormat, this.percent});

  final String? stockCode;
  final String? total;
  final String? totalFormat;
  final String? totalInterestFormat;
  final String? percent;

  static List<AssetModel> listFromMap(str) =>
      List<AssetModel>.from((str).map((x) => AssetModel.fromMap(x)));

  static String assetsModelToMap(List<AssetModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory AssetModel.fromMap(Map<String, dynamic> json) => AssetModel(
        stockCode: json["StockCode"] == null ? null : json["StockCode"],
        total: json["Total"] == null ? null : json["Total"].toString(),
        totalFormat: json["TotalFormat"] == null ? null : json["TotalFormat"].toString(),
        totalInterestFormat: json["TotalInterestFormat"] == null ? null : json["TotalInterestFormat"].toString(),
        percent: json["Percent"] == null ? null : json["Percent"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "StockCode": stockCode == null ? null : stockCode,
        "Total": total == null ? null : total,
        "TotalFormat": totalFormat == null ? null : totalFormat,
        "TotalInterestFormat": totalInterestFormat == null ? null : totalInterestFormat,
        "Percent": percent == null ? null : percent,
      };
}

class InvestingModel {
  InvestingModel({
    this.productName,
    this.colorCode,
    this.total,
  });

  // final int? actionType;
  final String? productName;
  final String? colorCode;
  final String? total;

  static List<InvestingModel> listFromMap(str) =>
      List<InvestingModel>.from((str).map((x) => InvestingModel.fromMap(x)));

  static String assetsModelToMap(List<InvestingModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  factory InvestingModel.fromMap(Map<String, dynamic> json) => InvestingModel(
        productName: json["ProductName"] == null ? null : json["ProductName"],
        colorCode: json["ColorCode"] == null ? null : json["ColorCode"],
        total: json["Total"] == null ? null : json["Total"],
      );

  Map<String, dynamic> toMap() => {
        "ProductName": productName == null ? null : productName,
        "ColorCode": colorCode == null ? null : colorCode,
        "Total": total == null ? null : total,
      };
}
