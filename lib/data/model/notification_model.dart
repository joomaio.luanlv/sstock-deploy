import 'package:app_sstock/constants.dart';
import 'package:app_sstock/utils/common.dart';

class NotificationModel {
  int? id;
  int? customerId;
  String? message;
  bool? isViewed;
  String? createDate;
  dynamic dateViewed;
  String? title;
  dynamic type;
  dynamic customer;
  DateTime? createDateFormated;

  NotificationModel(
      {this.id,
      this.customerId,
      this.message,
      this.isViewed,
      this.createDate,
      this.dateViewed,
      this.title,
      this.type,
      this.createDateFormated,
      this.customer});

  static List<NotificationModel> mapToList(data) =>
      List<NotificationModel>.from(data.map((x) => NotificationModel.fromMap(x)));

  NotificationModel.fromMap(Map<String, dynamic> json) {
    id = json['Id'];
    customerId = json['Customer_Id'];
    message = json['Message'];
    isViewed = json['IsViewed'] ?? false;
    createDate = json['CreateDate'];
    dateViewed = json['DateViewed'];
    title = json['Title'];
    type = json['Type'];
    createDateFormated =  json["CreateDateFormated"] == null
        ? null
        : Common.parserDate(json["CreateDateFormated"], format: FormatDate.full);
    customer = json['Customer'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Customer_Id'] = this.customerId;
    data['Message'] = this.message;
    data['IsViewed'] = this.isViewed;
    data['CreateDate'] = this.createDate;
    data['DateViewed'] = this.dateViewed;
    data['Title'] = this.title;
    data['Type'] = this.type;
    data['Customer'] = this.customer;
    return data;
  }
}
