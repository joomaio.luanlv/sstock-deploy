class ApiConstant {
  static final apiHost = "https://sstock.maiatech.com.vn/Api/";
  static final vietqr = "https://api.vietqr.io/v2/";
  static final apiHostImage = "https://sstock.maiatech.com.vn";

  static String getImageHost(String? url) => "${apiHostImage}${url}";
  static final login = "login";
  static final logout = "logout";
  static final register = "register";
  static final resetPasword = "ResetPasword";
  static final sendOTP = "SendOTP";
  static final QuenMatKhau = "";
  static final doiMatKhau = "ChangePassword";
  static final customerInfo = "CustomerInfo";
  static final updateInformation = "UpdateInformation";
  static final createOrEditOfCusBankInfo = "CreateOrEditOfCusBankInfo";
  static final listProduct = "ListProduct?catId=0";
  static final productDetail = "ProductDetail";
  static final getDocument = "GetDocument";
  static final buyProduct = "BuyProduct";
  static final getContract = "GetContract";
  static final getProfit = "GetProfit";
  static final listHistoryProduct = "History";
  static final getInvestPerfomance = "GetInvestPerfomance";
  static final listBank = "banks";
  static final getNotifies = "GetNotifies";
  static final readListNofication = "SetNotifyViewed";
  static final updateFirebaseToken = "UpdateFirebaseToken";
  static final question = "listquestion";
  static final closeAccount = "CloseAccount";
  static final withdraw = "WithdrawStock";
  static final packageDue = "packagedue";
  static final borrow = "Borrow";
  static final soldStock = "SoldStock";
  static final cancelRequest = "cancelrequest";
  static final dailyProfit = "DailyProfit";
  static final listAsset = "ListAssets";
  static final getExpectedProfit = "GetExpectedProfit";
  static final getListStatus = "GetListStatus";
  static final listPackageUnfinished = "ListNotComplete";
  static final checkBorrowMaximum = "CheckBorrowMaximum";
  static final getPool = "GetPool";
}