import 'dart:io';

import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/navigator.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:dio/dio.dart';

final int timeOutRequest = 40000; //30s

class Network {
  final String? baseUrl;
  static BaseOptions options =
      BaseOptions(connectTimeout: timeOutRequest, receiveTimeout: timeOutRequest, baseUrl: ApiConstant.apiHost);
  static final Dio _dio = Dio(options);

  static final Network instance = Network._internal();

  Network._internal({this.baseUrl, int? timeOut}) {
    if (baseUrl?.isNotEmpty ?? false) {
      _dio.options.baseUrl = baseUrl!;
    }
    _dio.options.connectTimeout = timeOut ?? timeOutRequest;
    _dio.interceptors.add(LogInterceptor(responseBody: true, requestHeader: true));
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions myOption, RequestInterceptorHandler handler) async {
      String token = await SharedPreferenceUtil.getToken();
      if (token.isNotEmpty) {
        myOption.headers["Authorization"] = token;
      }
      return handler.next(myOption);
    }));
  }

  factory Network({String? baseUrl, int? timeOut}) {
    return Network._internal(baseUrl: baseUrl ?? ApiConstant.apiHost, timeOut: timeOut ?? 15000);
  }

  Future<ApiResponse> get({required String url, Map<String, dynamic>? params}) async {
    try {
      Response response = await _dio.get(
        url,
        queryParameters: await BaseParamRequest.request(params),
        options: Options(responseType: ResponseType.json),
      );
      return getApiResponse(response);
    } on DioError catch (e) {
      //handle error
      print("DioError: ${e.toString()}");
      return getError(e);
    }
  }

  Future<ApiResponse> post(
      {required String url,
      Map<String, dynamic>? body,
      Map<String, dynamic>? params,
      String contentType = Headers.jsonContentType}) async {
    try {
      Response response = await _dio.post(
        url,
        data: await BaseParamRequest.request(body),
        queryParameters: params,
        options: Options(responseType: ResponseType.json, contentType: contentType),
      );
      return getApiResponse(response);
    } catch (e) {
      print("===post =====${e}");
      return getError(e as DioError);
    }
  }

  ApiResponse getError(DioError e) {
    if (e.response?.statusCode == 401) {
      handleTokenExpired();
    }
    switch (e.type) {
      case DioErrorType.cancel:
      case DioErrorType.connectTimeout:
      case DioErrorType.receiveTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.other:
        return ApiResponse.error(
          "error.connect",
        );
      default:
        return ApiResponse.error(e.message, data: getDataReplace(e.response?.data), code: e.response?.statusCode);
    }
  }

  ApiResponse getApiResponse(Response response) {
    try {
      if (response.data["Error"] == 401) {
        handleTokenExpired();
      }
    } catch (e) {}
    return ApiResponse.success(
        data: response.data,
        code: response.statusCode,
        // message: response.statusMessage,
        error: response.data is Map<String, dynamic> ? response.data["Error"] : null,
        message: response.data is Map<String, dynamic> ? response.data["Msg"] : null);
  }

  void handleTokenExpired() async {
    NavigationService.instance.showDialogTokenExpired();
    // await SharedPreferenceUtil.clearData();
  }

  getDataReplace(data) {
    if (data is String) {
      return data.replaceAll("loi:", "").replaceAll(":loi", "").trim();
    }
    return data;
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
