
class ApiResponse<T> {
  String? message;
  int? code;
  T? data;
  int? error;
  String? errMessage;

  ApiResponse.success({
    this.data,
    this.code,
    this.message,
    this.error,
    this.errMessage
  });

  ApiResponse.error(this.message, {this.data, this.code});

  bool get isSuccess => code != null && code == 200 && error == 0;
}
