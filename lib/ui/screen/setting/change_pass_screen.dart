import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChangePasswordScreen extends StatelessWidget {
  final ChangePasswordCubit changePasswordCubit = ChangePasswordCubit();
  final TextEditingController _oldPasswordText = TextEditingController();
  final TextEditingController _newPasswordText = TextEditingController();
  final TextEditingController _reNewPasswordText = TextEditingController();
  final _formKeyOldPass = GlobalKey<TextFieldState>();
  final _formKeyNewPass = GlobalKey<TextFieldState>();
  final _formKeyConfirmPass = GlobalKey<TextFieldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChangePasswordCubit>(
      create: (_) => changePasswordCubit,
      child: BaseScreen(
        messageNotify: CustomSnackBar<ChangePasswordCubit>(),
        loadingWidget: CustomLoading<ChangePasswordCubit>(),
        title: "Thay đổi mật khẩu",
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: Column(
            children: [
              CustomTextLabel(
                "Vui lòng nhập mật khẩu có độ dài từ 8 ký tự, gồm chữ cái thường, chữ cái Hoa, ký tự đăc biệt.",
                fontSize: 15,
                color: AppColors.ff585858,
              ),
              SizedBox(height: 20),
              CustomTextInput(
                key: _formKeyOldPass,
                isPasswordTF: true,
                hideUnderline: false,
                validator: (String value) {
                  return value.isEmpty ? "change_pass_screen.error_old_password_empty" : "";
                },
                textController: _oldPasswordText,
                title: "change_pass_screen.old_password_hint",
              ),
              SizedBox(height: 20),
              CustomTextInput(
                key: _formKeyNewPass,
                isPasswordTF: true,
                hideUnderline: true,
                validator: (String value) {
                  String errorText = "";
                  if (value.isEmpty) {
                    errorText = "change_pass_screen.error_new_password_empty";
                  } else if (!Common.isPasswordValid(value)) {
                    errorText = "change_pass_screen.error_password_format";
                  }
                  return errorText;
                },
                textController: _newPasswordText,
                title: "change_pass_screen.new_password_hint",
                enableBorder: true,
              ),
              SizedBox(height: 20),
              CustomTextInput(
                key: _formKeyConfirmPass,
                isPasswordTF: true,
                validator: (String value) {
                  String errorText = "";
                  if (value.isEmpty) {
                    errorText = "change_pass_screen.error_renew_password_empty";
                  } else if (value.trim().compareTo(_newPasswordText.text.trim()) != 0) {
                    errorText = "change_pass_screen.error_renew_password_wrong";
                  }
                  return errorText;
                },
                textController: _reNewPasswordText,
                title: "change_pass_screen.renew_password_hint",
              ),
              SizedBox(height: 20),
              Spacer(),
              BaseButton(
                width: double.infinity,
                title: 'Lưu',
                onTap: () {
                  changePassword(context);
                },
              ),
              BlocListener<ChangePasswordCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<bool> && state.data) {
                    CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            content: "change_pass_screen.change_success",
                            titleOK: "Đồng ý",
                            onTapOK: () {})
                        .then((value) {
                      Navigator.pop(context);
                    });
                  }
                },
                child: Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void changePassword(BuildContext context) {
    String _oldPassword = _oldPasswordText.text.trim();
    String _newPassword = _newPasswordText.text.trim();
    bool isValid = true;
    if (!_formKeyOldPass.currentState!.isValid) {
      isValid = false;
    }
    if (!_formKeyNewPass.currentState!.isValid) {
      isValid = false;
    }
    if (!_formKeyConfirmPass.currentState!.isValid) {
      isValid = false;
    }
    if (isValid) {
      changePasswordCubit.changePassword(_oldPassword, _newPassword);
    }
  }
}
