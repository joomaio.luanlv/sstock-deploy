import 'dart:developer';

import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

class RegisterScreen extends StatefulWidget {
  final String? ref;
  const RegisterScreen({Key? key, this.ref}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  // GlobalKey<TextFieldState> keyUserName = GlobalKey();
  // GlobalKey<TextFieldState> keyFullName = GlobalKey();
  GlobalKey<TextFieldState> keyEmail = GlobalKey();
  GlobalKey<TextFieldState> keyPhone = GlobalKey();
  GlobalKey<TextFieldState> keyCode = GlobalKey();
  
  bool isCheck = false;
  var setStateEnableButton;
  RegisterUserModel registerUserModel = RegisterUserModel();

  @override
  Widget build(BuildContext context) {
    
    return BaseScreen(
        customAppBar: BaseScreen.baseAppBarOnlyBack(context),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20.sh),
                BaseScreen.baseTitle(title: "Đăng ký tài khoản"),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
                  child: CustomTextLabel(
                    "Vui lòng điền thông tin cá nhân của quý khách để đăng ký tài khoản.",
                    textAlign: TextAlign.center,
                    color: AppColors.ff585858,
                  ),
                ),
                SizedBox(height: 50.sh),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      CustomTextInput(
                        key: keyPhone,
                        enableBorder: true,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        keyboardType: TextInputType.phone,
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            return "Vui lòng nhập Số điện thoại";
                          }
                          if (!Common.validatePhone(value)) {
                            return "Số điện thoại không đúng định dạng";
                          }
                          return "";
                        },
                        getTextFieldValue: (String value) {
                          registerUserModel.mobilePhone = value;
                        },
                        hintText: "",
                        title: "Số điện thoại",
                      ),
                      CustomTextInput(
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        enableBorder: true,
                        key: keyEmail,
                        keyboardType: TextInputType.emailAddress,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        hintText: "",
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            // return "Vui lòng nhập Email";
                            return "";
                          }
                          if (!Common.validateEmail(value)) {
                            return "Email không đúng định dạng";
                          }
                          return "";
                        },
                        getTextFieldValue: (String value) {
                          registerUserModel.email = value;
                        },
                        title: "Email",
                      ),
                      CustomTextInput(
                        key: keyCode,
                        initData: widget.ref.toString() != 'null' ? widget.ref : "",
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        enableBorder: true,
                        hideUnderline: true,
                        hintText: "",
                        getTextFieldValue: (String value) {
                          registerUserModel.parentcode = value;
                        },
                        title: "Mã tư vấn viên (nếu có)",
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 60),
                CommonWidget.baseQuoteWidget(quote: "Học cách đầu tư là học cách để tiền làm việc cho bạn"),
                SizedBox(height: 100),
                CommonWidget.baseLineWidget(margin: EdgeInsets.symmetric(vertical: 10)),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                  child: Column(
                    children: [
                      BaseCheckBox(
                        isCheck: isCheck,
                        onChanged: (bool isCheck) {
                          setStateEnableButton(() {
                            this.isCheck = isCheck;
                          });
                        },
                        title: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Tôi đã hiểu và xác nhận đồng ý với ',
                                style: TextStyle(color: AppColors.ff585858),
                              ),
                              TextSpan(
                                recognizer: TapGestureRecognizer()..onTap = () => print('Tap Here onTap'),
                                text: ' Điều khoản sử dụng ',
                                style: TextStyle(color: AppColors.ffff2e00),
                              ),
                              TextSpan(
                                text: 'của ứng dụng!',
                                style: TextStyle(color: AppColors.ff585858),
                              ),
                            ],
                          ),
                        ),
                      ),
                      StatefulBuilder(
                        builder: (BuildContext context, void Function(void Function()) setState) {
                          this.setStateEnableButton = setState;
                          return BaseButton(
                            onTap: () {
                              bool valid = true;
                              // if (!keyFullName.currentState!.isValid) {
                              //   valid = false;
                              // }
                              // if (!keyUserName.currentState!.isValid) {
                              //   valid = false;
                              // }
                              if (!keyEmail.currentState!.isValid) {
                                valid = false;
                              }
                              if (!keyPhone.currentState!.isValid) {
                                valid = false;
                              }
                              if (valid) {
                                registerUserModel.username = keyPhone.currentState?.value;
                                Navigator.pushNamed(context, Routes.inputPasswordRegisterScreen,
                                    arguments: registerUserModel);
                              }
                            },
                            enable: isCheck,
                            width: 1.width,
                            margin: EdgeInsets.symmetric(vertical: 10.sw),
                            title: "Tiếp tục",
                          );
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
