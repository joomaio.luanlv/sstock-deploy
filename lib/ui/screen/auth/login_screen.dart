import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/images.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(create: (_) => LoginCubit(), child: _LoginBody());
  }
}

class _LoginBody extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<_LoginBody> {
  GlobalKey<TextFieldState> keyUserName = GlobalKey();
  GlobalKey<TextFieldState> keyPassword = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        messageNotify: CustomSnackBar<LoginCubit>(),
        loadingWidget: CustomLoading<LoginCubit>(),
        backgroundImage: DecorationImage(alignment: Alignment.topCenter, image: AssetImage(AppImages.ic_login_bg)),
        hideAppBar: true,
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 90.sh),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 90),
                    child: Image.asset(
                      AppImages.ic_logo,
                    )),
                SizedBox(height: 40.sh),
                CommonWidget.baseQuoteWidget(
                    quote:
                        "Chỉ có 10% Nhà đầu cơ chiến thắng trên thị trường Tài Chính.\nNhưng 90% nhà đầu tư có kế hoạch "
                        "quản lý vốn vững vàng chiến thắng trên thị trường."),
                SizedBox(height: 50.sh),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      CustomTextInput(
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        enableBorder: true,
                        key: keyUserName,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        hintText: "",
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            return "Vui lòng nhập tên đăng nhập";
                          }
                          return "";
                        },
                        title: "Tên đăng nhập",
                      ),
                      CustomTextInput(
                        key: keyPassword,
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        isPasswordTF: true,
                        enableBorder: true,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            return "Vui lòng nhập mật khẩu";
                          }
                          return "";
                        },
                        hintText: "",
                        title: "Mật khẩu",
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                        alignment: Alignment.centerRight,
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.forgotPasswordScreen,
                                arguments: keyUserName.currentState?.value.toString());
                          },
                          child: CustomTextLabel(
                            "Quên mật khẩu?",
                            fontSize: 16,
                            color: AppColors.ff404A69,
                          ),
                        ),
                      ),
                      BaseButton(
                        onTap: () {
                          bool valid = true;
                          if (!keyUserName.currentState!.isValid) {
                            valid = false;
                          }
                          if (!keyPassword.currentState!.isValid) {
                            valid = false;
                          }
                          if (valid) {
                            BlocProvider.of<LoginCubit>(context)
                                .loginWithUserName(keyUserName.currentState?.value, keyPassword.currentState?.value);
                          }
                        },
                        width: 1.width,
                        margin: EdgeInsets.symmetric(vertical: 10.sw),
                        title: "login.title_login",
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                        child: BaseButton(
                          borderRadius: (10),
                          backgroundColor: Colors.transparent,
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                          onTap: () {
                            Navigator.pushNamed(context, Routes.registerScreen);
                          },
                          child: CustomTextLabel(
                            "Tạo tài khoản mới",
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: AppColors.ff63C856,
                          ),
                        ),
                      ),
                      BlocListener<LoginCubit, BaseState>(
                        listener: (_, state) {
                          if (state is LoadedState) {
                            Navigator.pushReplacementNamed(context, Routes.mainScreen);
                            BlocProvider.of<UserInfoCubit>(context).getUserInfo();
                          }
                        },
                        child: Container(),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
