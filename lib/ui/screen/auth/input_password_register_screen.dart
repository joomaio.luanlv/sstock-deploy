import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/screen/screen.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

class InputPasswordRegisterScreen extends StatelessWidget {
  final RegisterUserModel? registerUserModel;

  const InputPasswordRegisterScreen({Key? key, this.registerUserModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ActionOTPCubit>(
        create: (_) => ActionOTPCubit(),
        child: InputPasswordRegisterBody(
          registerUserModel: registerUserModel,
        ));
    ;
  }
}

class InputPasswordRegisterBody extends StatefulWidget {
  final RegisterUserModel? registerUserModel;

  const InputPasswordRegisterBody({Key? key, this.registerUserModel}) : super(key: key);

  @override
  _InputPasswordRegisterScreenState createState() => _InputPasswordRegisterScreenState();
}

class _InputPasswordRegisterScreenState extends State<InputPasswordRegisterBody> {
  GlobalKey<TextFieldState> keyUserName = GlobalKey();
  GlobalKey<TextFieldState> keyPassword = GlobalKey();
  GlobalKey<TextFieldState> _formKeyConfirmPass = GlobalKey<TextFieldState>();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        messageNotify: CustomSnackBar<ActionOTPCubit>(),
        loadingWidget: CustomLoading<ActionOTPCubit>(),
        customAppBar: BaseScreen.baseAppBarOnlyBack(context),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20.sh),
                BaseScreen.baseTitle(title: "Thông tin tài khoản"),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
                  child: CustomTextLabel(
                    "Vui lòng nhập mật khẩu có độ dài từ 8 ký tự, gồm chữ cái thường, chữ cái hoa, ký tự đặc biệt",
                    textAlign: TextAlign.center,
                    color: AppColors.ff585858,
                  ),
                ),
                SizedBox(height: 50.sh),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      CustomTextInput(
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        enableBorder: true,
                        enabled: false,
                        key: keyUserName,
                        // keyboardType: TextInputType.emailAddress,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        hintText: "",
                        validator: (String value) {
                          return "";
                        },
                        initData: widget.registerUserModel?.username ?? "",
                        title: "Tên đăng nhập",
                      ),
                      CustomTextInput(
                        key: keyPassword,
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        isPasswordTF: true,
                        enableBorder: true,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            return "Vui lòng nhập mật khẩu";
                          }
                          if (!Common.isPasswordValid(value)) {
                            return "change_pass_screen.error_password_format";
                          }
                          return "";
                        },
                        hintText: "",
                        title: "Mật khẩu",
                      ),
                      CustomTextInput(
                        key: _formKeyConfirmPass,
                        margin: EdgeInsets.symmetric(vertical: 10.sh),
                        isPasswordTF: true,
                        enableBorder: true,
                        textController: TextEditingController(),
                        hideUnderline: true,
                        validator: (String value) {
                          if (value.trim().isEmpty) {
                            return "Vui lòng nhập mật khẩu";
                          }
                          if (value.trim().compareTo(keyPassword.currentState?.value.trim() ?? "") != 0) {
                            return "change_pass_screen.error_renew_password_wrong";
                          }
                          return "";
                        },
                        hintText: "",
                        title: "Nhập lại mật khẩu",
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 60),
                CommonWidget.baseQuoteWidget(
                    quote: "Thời điểm đầu tư tốt nhất là 10 năm trước\nThời điểm tốt thứ nhì "
                        "là ngay bây giờ"),
                SizedBox(height: 100),
                CommonWidget.baseLineWidget(margin: EdgeInsets.symmetric(vertical: 10)),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                  child: Column(
                    children: [
                      BaseButton(
                        onTap: () {
                          bool isValid = true;
                          if (!keyPassword.currentState!.isValid) {
                            isValid = false;
                          }
                          if (!_formKeyConfirmPass.currentState!.isValid) {
                            isValid = false;
                          }
                          if (isValid) {
                            widget.registerUserModel?.password = keyPassword.currentState?.value;
                            Navigator.pushNamed(context, Routes.inputOTPScreen,
                                arguments: InputOTPArg(
                                  email: widget.registerUserModel?.email,
                                  mobilePhone: widget.registerUserModel?.mobilePhone,
                                  registerUserModel: widget.registerUserModel,
                                  typeSendOTP: TypeSendOTP.register,
                                )).then((value) {
                              if (value != null) {
                                // widget.registerUserModel?.otp = value.toString();
                                // BlocProvider.of<RegisterCubit>(context).register(widget.registerUserModel);
                              }
                            });
                          }
                        },
                        width: 1.width,
                        margin: EdgeInsets.symmetric(vertical: 10.sw),
                        title: "Xác nhận",
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
