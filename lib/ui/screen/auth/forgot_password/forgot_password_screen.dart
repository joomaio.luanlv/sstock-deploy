import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

class ForgotPasswordScreen extends StatefulWidget {
  final String? email;

  const ForgotPasswordScreen({Key? key, this.email}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  GlobalKey<TextFieldState> keyEmail = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        customAppBar: BaseScreen.baseAppBarOnlyBack(context),
        body: Container(
          child: Column(
            children: [
              SizedBox(height: 20.sh),
              BaseScreen.baseTitle(title: "Nếu bạn quên tiền trong App SSTOCK sẽ nhắc bạn"),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
                child: CustomTextLabel(
                  "Hãy nhập số điện thoại bạn đăng ký để lấy lại mật khẩu nhé!",
                  textAlign: TextAlign.center,
                  color: AppColors.ff585858,
                ),
              ),
              SizedBox(height: 50.sh),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    CustomTextInput(
                      margin: EdgeInsets.symmetric(vertical: 10.sh),
                      enableBorder: true,
                      key: keyEmail,
                      keyboardType: TextInputType.phone,
                      textController: TextEditingController(),
                      hideUnderline: true,
                      hintText: "",
                      initData: widget.email,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Vui lòng nhập Số điện thoại";
                        }
                        if (!Common.validatePhone(value)) {
                          return "Số điện thoại không đúng định dạng";
                        }
                        return "";
                      },
                      title: "Số điện thoại đăng ký",
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                child: BaseButton(
                  onTap: () {
                    if (keyEmail.currentState?.isValid ?? false) {
                      var registerUserModel = RegisterUserModel();
                      registerUserModel.username = keyEmail.currentState?.value;
                      Navigator.pushNamed(context, Routes.inputPasswordForgotScreen, arguments: registerUserModel);
                    }
                  },
                  width: 1.width,
                  margin: EdgeInsets.symmetric(vertical: 10.sw),
                  title: "Tiếp tục",
                ),
              )
            ],
          ),
        ));
  }
}
