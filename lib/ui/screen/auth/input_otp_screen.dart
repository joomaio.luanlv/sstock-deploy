import 'dart:async';

import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/register_user_model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

enum TypeSendOTP { forgotPassword, register }

class InputOTPArg {
  final TypeSendOTP typeSendOTP;
  final String? email;
  final String? mobilePhone;
  final RegisterUserModel? registerUserModel;

  InputOTPArg({required this.typeSendOTP, this.email, this.mobilePhone, this.registerUserModel});
}

class InputOTPScreen extends StatelessWidget {
  final InputOTPArg? inputOTPArg;

  const InputOTPScreen({Key? key, this.inputOTPArg}) : super(key: key);

  // widget.registerUserModel?.otp = value.toString();
  // BlocProvider.of<RegisterCubit>(context).register(widget.registerUserModel);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<SendOTPCubit>(create: (_) => SendOTPCubit()),
          BlocProvider<ActionOTPCubit>(
            create: (_) => ActionOTPCubit(),
          ),
        ],
        child: _InputOTPBody(
          inputOTPArg: inputOTPArg,
        ));
  }
}

class _InputOTPBody extends StatefulWidget {
  final InputOTPArg? inputOTPArg;

  const _InputOTPBody({Key? key, this.inputOTPArg}) : super(key: key);

  @override
  _InputOTPScreenState createState() => _InputOTPScreenState();
}

class _InputOTPScreenState extends State<_InputOTPBody> {
  final _formKeyOtp = GlobalKey<OtpTextFieldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sendOTP();
  }

  @override
  void dispose() {
    super.dispose();
  }

  sendOTP() {
    BlocProvider.of<SendOTPCubit>(context).sendOtp(
      email: widget.inputOTPArg?.email,
      mobilePhone: widget.inputOTPArg?.mobilePhone,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        messageNotify: Stack(
          children: [
            CustomSnackBar<SendOTPCubit>(),
            CustomSnackBar<ActionOTPCubit>(),
          ],
        ),
        loadingWidget: Stack(
          children: [
            CustomLoading<SendOTPCubit>(),
            CustomLoading<ActionOTPCubit>(),
          ],
        ),
        customAppBar: BaseScreen.baseAppBarOnlyBack(context),
        body: Container(
          child: Column(
            children: [
              SizedBox(height: 20.sh),
              BaseScreen.baseTitle(title: "Xác thực OTP"),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
                child: CustomTextLabel(
                  "Mã OTP đã được gửi về email hoặc số điện thoại của quý khách. Vui lòng nhập và xác nhận mã OTP để"
                  " tiếp tục.",
                  textAlign: TextAlign.center,
                  color: AppColors.ff585858,
                ),
              ),
              SizedBox(height: 60),
              Container(
                child: OtpTextField(
                  key: _formKeyOtp,
                  numberOfFields: 6,
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  // fieldWidth: (1.width - 60.sw - (1 * 16)) / 8,
                  textStyle: TextStyle(fontSize: 24),
                  showFieldAsBox: false,
                  onCodeChanged: (String code) {},
                  onSubmit: (String verificationCode) {
                    onSubmitOTP(verificationCode);
                  }, // end onSubmit
                ),
              ),
              SizedBox(height: 20),
              ResendCodeWidget(
                onResendCode: () {
                  sendOTP();
                },
              ),
              Spacer(),
              SizedBox(height: 20),
              CommonWidget.baseQuoteWidget(
                  quote:
                      "Nước có thể chở thuyền, cũng có thể lật thuyền. Tiền có thể giúp ta, cũng có thể hại ta nếu ta không biết cách điều khiển nó"),
              SizedBox(height: 20),
              Spacer(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                child: BaseButton(
                  onTap: () {
                    if (_formKeyOtp.currentState?.valid ?? false) onSubmitOTP(_formKeyOtp.currentState?.value);
                  },
                  width: 1.width,
                  margin: EdgeInsets.symmetric(vertical: 10.sw),
                  title: "Xác nhận",
                ),
              ),
              SizedBox(height: 40),
              BlocListener<ActionOTPCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<bool> && state.data == true) {
                    switch (widget.inputOTPArg?.typeSendOTP) {
                      case TypeSendOTP.forgotPassword:
                        CustomDialog.showDialogConfirm(context,
                                showCancel: false,
                                showIconClose: false,
                                barrierDismissible: false,
                                content: "Chúc mừng bạn đã thay đổi mật khẩu tài khoản thành công!",
                                titleOK: "Đăng nhập ngay",
                                onTapOK: () {})
                            .then((value) {
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            Routes.loginScreen,
                            (route) => false,
                          );
                        });
                        break;
                      case TypeSendOTP.register:
                        CustomDialog.showDialogConfirm(context,
                                showCancel: false,
                                showIconClose: false,
                                barrierDismissible: false,
                                content: "Chúc mừng bạn đã đăng ký tài khoản thành công!",
                                titleOK: "Đăng nhập ngay",
                                onTapOK: () {})
                            .then((value) {
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            Routes.loginScreen,
                            (route) => false,
                          );
                        });
                        break;
                      default:
                    }
                  }
                },
                child: Container(),
              )
            ],
          ),
        ));
  }

  void onSubmitOTP(String? verificationCode) {
    widget.inputOTPArg?.registerUserModel?.otp = verificationCode;
    switch (widget.inputOTPArg?.typeSendOTP) {
      case TypeSendOTP.forgotPassword:
        BlocProvider.of<ActionOTPCubit>(context).forgotPass(widget.inputOTPArg?.registerUserModel);
        break;
      case TypeSendOTP.register:
        BlocProvider.of<ActionOTPCubit>(context).register(widget.inputOTPArg?.registerUserModel);
        break;
      default:
    }
  }
}

class ResendCodeWidget extends StatefulWidget {
  Function? onResendCode;

  ResendCodeWidget({Key? key, this.onResendCode}) : super(key: key);

  @override
  State<ResendCodeWidget> createState() => _ResendCodeWidgetState();
}

class _ResendCodeWidgetState extends State<ResendCodeWidget> {
  Timer? _timer;
  int _start = 30;
  bool resendCode = false;

  @override
  void initState() {
    super.initState();
  }

  void startTimer(int time) {
    _start = time;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            resendCode = true;
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<SendOTPCubit, BaseState>(
          builder: (_, state) {
            if (state is LoadingState) {
              return Container();
            }
            if (state is ErrorState) {
              resendCode = true;
            }
            return renderBody();
          },
        ),
        BlocListener<SendOTPCubit, BaseState>(
          listener: (_, state) {
            if (state is LoadedState<int>) {
              startTimer(state.data * 60);
            }
          },
          child: Container(),
        )
      ],
    );
  }

  renderBody() {
    return resendCode
        ? InkWell(
            onTap: () {
              setState(() {
                resendCode = false;
                widget.onResendCode?.call();
              });
            },
            child: CustomTextLabel(
              "Gửi lại mã",
              fontWeight: FontWeight.w600,
              color: AppColors.base_color,
              textAlign: TextAlign.center,
            ),
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomTextLabel(
                "Gửi lại mã",
                textAlign: TextAlign.center,
              ),
              SizedBox(width: 5),
              CustomTextLabel(
                formatTime(_start),
                color: AppColors.base_color,
                fontWeight: FontWeight.w600,
                textAlign: TextAlign.center,
              ),
            ],
          );
  }

  String formatTime(int startSeconds) {
    int minutes = startSeconds ~/ 60;
    int seconds = (startSeconds % 60);
    return minutes.toString().padLeft(2, "0") + ":" + seconds.toString().padLeft(2, "0");
  }
}
