import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/product/list_question_cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class QuestionScreen extends StatelessWidget {
  const QuestionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<ListQuestionCubit>(
        create: (_) => ListQuestionCubit()..getListQuestion(),
      )
    ], child: QuestionBody());
  }
}

class QuestionBody extends StatelessWidget {
  QuestionBody({
    Key? key,
  }) : super(key: key) {}

  renderListQuestion() {
    return BlocBuilder<ListQuestionCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<QuestionModel>>) {
          return ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return renderItemQuestion(context, state.data[index]);
            },
            itemCount: state.data.length,
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                height: 10,
              );
            },
          );
        }
        return SizedBox.shrink();
      },
    );
  }

  renderItemQuestion(BuildContext context, QuestionModel? questionModel) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: AppColors.ff00A95E,
              borderRadius: BorderRadius.circular(10)),
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
          child: ExpansionTile(
            collapsedTextColor: Colors.white,
            collapsedIconColor: Colors.white,
            textColor: Colors.white,
            iconColor: Colors.white,
            title: Text('${questionModel?.question}'),
            children: <Widget>[
              Html(
                data: """${questionModel?.answer}""",
                style: {
                  "body": Style(
                    fontSize: FontSize(18.0),
                    lineHeight: LineHeight.number(1.3),
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        colorTitle: AppColors.base_color,
        title: "Câu hỏi thường gặp",
        body: SingleChildScrollView(
          child: Column(children: [
            SizedBox(height: 15),
            renderListQuestion(),
            SizedBox(height: 15),
          ]),
        ));
  }
}
