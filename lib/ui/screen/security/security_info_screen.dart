import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/base_screen.dart';
import 'package:app_sstock/ui/widget/common/common_widget.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SecurityInfoScreen extends StatelessWidget {
  const SecurityInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Bảo mật",
      body: SingleChildScrollView(
        child: Column(
          children: [
            renderListAction(context),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  renderListAction(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CommonWidget.renderActionWidget(
              title: "Thay đổi mật khẩu",
              icon: AppImages.ic_setting_change_pass,
              onTap: () {
                Navigator.pushNamed(context, Routes.changePasswordScreen);
              }),
          CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(title: "Cài đặt mã pin", icon: AppImages.ic_setting_pin, onTap: () {}),
          // CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Đăng nhập bằng Sinh trắc học",
          //     rightWidget: Container(
          //         alignment: Alignment.centerRight, height: 10, child: Switch(value: true, onChanged: (bool value) {})),
          //     icon: AppImages.ic_setting_biometric,
          //     onTap: () {}),
          // CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Quản lý thiết bị", icon: AppImages.ic_setting_manage_device, onTap: () {}),
          // CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Xác thực mã pin, sinh trắc học khi giao dịch",
          //     rightWidget: Container(height: 10, child: Switch(value: false, onChanged: (bool value) {})),
          //     icon: AppImages.ic_setting_veri_trans,
          //     onTap: () {}),
          // CommonWidget.baseLineWidget(),
        ],
      ),
    );
  }
}
