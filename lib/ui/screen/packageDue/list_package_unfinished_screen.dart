import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListPackageUnfinishedScreen extends StatelessWidget {
  const ListPackageUnfinishedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ListPackageUnfinishedCubit>(
        create: (_) => ListPackageUnfinishedCubit()..getListPackageUnfinished(),
        child: ListPackageUnfinishedBody());
  }
}

class ListPackageUnfinishedBody extends StatelessWidget {
  final RefreshController controller = RefreshController();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Các khoản đầu tư/vay chưa tất toán",
      loadingWidget: CustomLoading<ListPackageUnfinishedCubit>(),
      hideAppBar: false,
      body: SmartRefresher(
        header: WaterDropMaterialHeader(),
        enablePullUp: false,
        enablePullDown: true,
        onRefresh: () {
          BlocProvider.of<ListPackageUnfinishedCubit>(context)
              .getListPackageUnfinished();
          controller.refreshCompleted();
        },
        controller: controller,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              renderBody(),
            ],
          ),
        ),
      ),
    );
  }

  renderListPackage() {
    return BlocBuilder<ListPackageUnfinishedCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<HistoryModel>>) {
          return ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return renderPackageItem(context, state.data[index]);
            },
            itemCount: state.data.length,
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                height: 15,
              );
            },
          );
        }
        return SizedBox.shrink();
      },
    );
  }

  renderPackageItem(BuildContext context, HistoryModel? historyModel) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0,
              offset: const Offset(2, 2.0),
            ),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: CustomTextLabel(
                              "${historyModel?.startDate} - ${historyModel?.endDate}",
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              color: AppColors.ff404A69,
                            ),
                          ),
                          renderLabel(historyModel)
                        ],
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: CustomTextLabel(historyModel?.productName,
                                maxLines: 2,
                                fontSize: 18,
                                formatCurrency: false,
                                fontWeight: FontWeight.w400,
                                color: AppColors.ff222222),
                          ),
                          Container(
                              alignment: Alignment.topLeft,
                              child: CustomTextLabel(historyModel?.originValue,
                                  maxLines: 2,
                                  fontSize: 16,
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff222222)),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Color getColorByKeyName(String? color) {
    return AppColors.getColorFromHex(color);
  }

  Widget renderLabel(HistoryModel? historyModel) {
    var data = historyModel?.getValueState();
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              color: getColorByKeyName(historyModel?.statusColor),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          child: CustomTextLabel(
            historyModel?.statusText,
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  renderBody() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        return Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: CustomTextLabel(
                              "Vui lòng tất toán tất cả các khoản dưới đây trước khi đóng tài khoản",
                              maxLines: 2,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: AppColors.ff999999))
                    ],
                  ),
                  SizedBox(height: 30),
                  renderListPackage()
                ],
              ),
            ),
            SizedBox(height: 10),
          ],
        );
      },
    );
  }
}
