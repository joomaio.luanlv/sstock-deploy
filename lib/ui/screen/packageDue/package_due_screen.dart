import 'dart:developer';

import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

enum PackageType { All, Borrow, Invest }

class PackageDueScreen extends StatelessWidget {
  const PackageDueScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PackageDueProductCubit>(
        create: (_) => PackageDueProductCubit()
          ..getListPackageDueProduct(
              {"cid": UserInfoBuilderWidget.getUserInfo(context)?.id}),
        child: PackageDueBody());
  }
}

class PackageDueBody extends StatelessWidget {
  GlobalKey<TextFieldState> keyEndDate = GlobalKey();
  final RefreshController controller = RefreshController();
  var setStateEndDate;
  var now = DateTime.now();
  DateFormat formatter = DateFormat('dd/MM/yyyy');
  TextEditingController txtEnd = TextEditingController();

  final List<String> items = [
    'Hôm nay',
    'Ngày mai',
    'Tháng này',
    'Tháng tới',
    'Quý này',
    'Quý tới',
    'Năm này',
    'Năm tới',
  ];
  String? selectedValue = "Hôm nay";

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Dự kiến lãi và gốc đến hạn",
      loadingWidget: CustomLoading<PackageDueProductCubit>(),
      hideAppBar: false,
      body: SmartRefresher(
        header: WaterDropMaterialHeader(),
        enablePullUp: false,
        enablePullDown: true,
        onRefresh: () {
          BlocProvider.of<PackageDueProductCubit>(context)
              .getListPackageDueProduct({
            "cid": UserInfoBuilderWidget.getUserInfo(context)?.id,
            "endDate": txtEnd.text,
          });
          controller.refreshCompleted();
        },
        controller: controller,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              renderBody(),
            ],
          ),
        ),
      ),
    );
  }

  renderListPackage() {
    return BlocBuilder<PackageDueProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<PackageDueModel>>) {
          if (state.data.length > 0) {
            return ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return renderPackageItem(context, state.data[index]);
              },
              itemCount: state.data.length,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 15,
                );
              },
            );
          } else {
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: CustomTextLabel(
                  "Không tìm thấy gói nào đến hạn trong khoảng thời gian này.",
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.base_color,
                  textAlign: TextAlign.center,
                ));
          }
        }
        return SizedBox.shrink();
      },
    );
  }

  renderPackageItem(BuildContext context, PackageDueModel? packageDueModel) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0,
              offset: const Offset(2, 2.0),
            ),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Row(
                            children: [
                              CustomTextLabel(packageDueModel?.startDate,
                                  maxLines: 2,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff999999),
                              SizedBox(width: 3),
                              CustomTextLabel("-",
                                  maxLines: 2,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff999999),
                              SizedBox(width: 3),
                              CustomTextLabel(packageDueModel?.endDate,
                                  maxLines: 2,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff999999)
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: CustomTextLabel(packageDueModel?.productName,
                                maxLines: 2,
                                fontSize: 18,
                                formatCurrency: false,
                                fontWeight: FontWeight.w400,
                                color: AppColors.ff222222),
                          ),
                          Container(
                              alignment: Alignment.topLeft,
                              child: CustomTextLabel(
                                  packageDueModel?.originValue,
                                  maxLines: 2,
                                  fontSize: 16,
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff222222)),
                        ],
                      ),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(children: [
                            CustomTextLabel("Lãi dự kiến",
                                textAlign: TextAlign.left,
                                maxLines: 2,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: AppColors.ff999999),
                            SizedBox(height: 5),
                            Container(
                                child: CustomTextLabel(
                                    "${packageDueModel?.profitComplete}",
                                    maxLines: 2,
                                    fontSize: 16,
                                    formatCurrency: false,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.ff222222)),
                          ]),
                          Column(children: [
                            Container(
                              child: CustomTextLabel("Tổng dự kiến",
                                  maxLines: 2,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff999999),
                            ),
                            SizedBox(height: 5),
                            Container(
                                child: CustomTextLabel(
                                    "${packageDueModel?.total}",
                                    maxLines: 2,
                                    fontSize: 16,
                                    formatCurrency: false,
                                    fontWeight: FontWeight.w400,
                                    color: AppColors.ff222222)),
                          ]),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }

  renderBody() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        var now = DateTime.now();
        DateFormat formatter = DateFormat('dd/MM/yyyy');

        var endDate =
            formatter.format(new DateTime(now.year, now.month, now.day));
        var firstDate = formatter.format(new DateTime(now.year, now.month, 1));

        switch (selectedValue) {
          case 'Hôm nay':
            endDate =
                formatter.format(new DateTime(now.year, now.month, now.day));
            break;
          case 'Ngày mai':
            endDate = formatter
                .format(new DateTime(now.year, now.month, now.day + 1));
            break;
          case 'Tháng này':
            endDate = formatter.format(new DateTime(now.year, now.month + 1, 1)
                .add(const Duration(days: -1)));
            break;
          case 'Tháng tới':
            endDate = formatter.format(new DateTime(now.year, now.month + 2, 1)
                .add(const Duration(days: -1)));
            break;
          case 'Quý này':
            endDate = formatter.format(new DateTime(now.year, now.month + 3, 1)
                .add(const Duration(days: -1)));
            break;
          case 'Quý tới':
            endDate = formatter.format(new DateTime(now.year, now.month + 6, 1)
                .add(const Duration(days: -1)));
            break;
          case 'Năm này':
            endDate = formatter.format(
                new DateTime(now.year + 1, 1, 1).add(const Duration(days: -1)));
            break;
          case 'Năm tới':
            endDate = formatter.format(
                new DateTime(now.year + 2, 1, 1).add(const Duration(days: -1)));
            break;
          default:
            endDate =
                formatter.format(new DateTime(now.year, now.month, now.day));
        }
        txtEnd.text = endDate;

        return Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      CustomTextLabel(
                        "Lãi và gốc sẽ được tính từ ngày bắt đầu tính lãi.",
                        overflow: TextOverflow.clip,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        fontStyle: FontStyle.italic,
                        color: AppColors.ffBDBDBD,
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Column(
                        children: [
                          Container(
                            width: 150,
                            child: CustomTextLabel(
                              "Đến ngày:",
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: AppColors.ff63C856,
                            ),
                          ),
                          new CustomTextInput(
                            key: keyEndDate,
                            enableBorder: false,
                            padding: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 0),
                            isDateTimeTF: true,
                            textController: txtEnd,
                            hideUnderline: true,
                            hintText: "",
                            maxLength: 18,
                            width: 150,
                            keyboardType: TextInputType.text,
                            validator: (String value) {
                              if (value.trim().isEmpty) {
                                return "Vui lòng chọn ngày dự kiến";
                              }
                              try {
                                int.parse(value.replaceAll(",", ""));
                              } catch (e) {
                                return "Ngày dự kiến nhập không đúng định dạng";
                              }
                              return "";
                            },
                            initData: firstDate,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(height: 3),
                          Container(
                            width: 150,
                            child: CustomTextLabel(
                              "Chọn nhanh:",
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: AppColors.ff63C856,
                            ),
                          ),
                          Container(
                            width: 150,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                buttonPadding: EdgeInsets.only(top: 9),
                                hint: Text(
                                  'Hôm nay',
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                                items: items
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ))
                                    .toList(),
                                value: selectedValue,
                                onChanged: (value) {
                                  setState(() {
                                    selectedValue = value as String;
                                    setStateEndDate?.call(() {});
                                  });

                                  switch (selectedValue) {
                                    case 'Hôm nay':
                                      endDate = formatter.format(new DateTime(
                                          now.year, now.month, now.day));
                                      break;
                                    case 'Ngày mai':
                                      endDate = formatter.format(new DateTime(
                                          now.year, now.month, now.day + 1));
                                      break;
                                    case 'Tháng này':
                                      endDate = formatter.format(new DateTime(
                                              now.year, now.month + 1, 1)
                                          .add(const Duration(days: -1)));
                                      break;
                                    case 'Tháng tới':
                                      endDate = formatter.format(new DateTime(
                                              now.year, now.month + 2, 1)
                                          .add(const Duration(days: -1)));
                                      break;
                                    case 'Quý này':
                                      endDate = formatter.format(new DateTime(
                                              now.year, now.month + 3, 1)
                                          .add(const Duration(days: -1)));
                                      break;
                                    case 'Quý tới':
                                      endDate = formatter.format(new DateTime(
                                              now.year, now.month + 6, 1)
                                          .add(const Duration(days: -1)));
                                      break;
                                    case 'Năm này':
                                      endDate = formatter.format(
                                          new DateTime(now.year + 1, 1, 1)
                                              .add(const Duration(days: -1)));
                                      break;
                                    case 'Năm tới':
                                      endDate = formatter.format(
                                          new DateTime(now.year + 2, 1, 1)
                                              .add(const Duration(days: -1)));
                                      break;
                                    default:
                                      endDate = formatter.format(new DateTime(
                                          now.year, now.month, now.day));
                                  }
                                  txtEnd.text = endDate;
                          
                                  BlocProvider.of<PackageDueProductCubit>(
                                          context)
                                      .getListPackageDueProduct({
                                    "cid": UserInfoBuilderWidget.getUserInfo(
                                            context)
                                        ?.id,
                                    "endDate": txtEnd.text,
                                  });
                                },
                                buttonHeight: 40,
                                buttonWidth: 140,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                              child: IconButton(
                            iconSize: 30,
                            icon:
                                Icon(Icons.search_rounded, color: Colors.grey),
                            onPressed: () {
                              BlocProvider.of<PackageDueProductCubit>(context)
                                  .getListPackageDueProduct({
                                "cid":
                                    UserInfoBuilderWidget.getUserInfo(context)
                                        ?.id,
                                "endDate": txtEnd.text,
                                "actionType": 0
                              });
                            },
                          ))
                        ],
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                  ),
                  CommonWidget.baseLineWidget(
                      margin: EdgeInsets.only(top: 10, bottom: 0)),
                  SizedBox(height: 20),
                  renderListPackage()
                ],
              ),
            ),
            SizedBox(height: 10),
          ],
        );
      },
    );
  }
}
