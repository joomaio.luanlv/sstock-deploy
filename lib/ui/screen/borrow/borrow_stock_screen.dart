import 'dart:convert';
import 'dart:developer';
import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/api_constant.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

enum BorrowType { CASH, STOCK }

class BorrowStockScreen extends StatelessWidget {
  final ProductModel? productModel;

  const BorrowStockScreen({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<BorrowCubit>(
            create: (_) => BorrowCubit(),
          ),
          BlocProvider<CheckBorrowMaximumCubit>(
            create: (_) => CheckBorrowMaximumCubit(),
          ),
          BlocProvider<DetailProductCubit>(
            create: (_) => DetailProductCubit()
              ..getDetailProduct(productModel,
                  UserInfoBuilderWidget.getUserInfo(context)?.id ?? 0),
          )
        ],
        child: BorrowStockBody(
          productModel: productModel,
        ));
  }
}

class BorrowStockBody extends StatelessWidget {
  final ProductModel? productModel;

  List<InterestRate> listPeriod = [];
  int indexSelect = 0;
  GlobalKey<TextFieldState> keyMoney = GlobalKey();
  var setStateProfit;
  var setStateMaximumBorrow;
  var setStateStockTypeList;

  GlobalKey<TextFieldState> keyStk = GlobalKey();
  GlobalKey<TextFieldState> keyTenTK = GlobalKey();
  BorrowType? borrowType = BorrowType.CASH;
  final _stockType = GlobalKey<CustomDropDownState>();
  final _customerProduct = GlobalKey<CustomDropDownState>();
  List<BankInfoModel> listData = [];
  List<ProductNotBlockModel>? proListNotBlock =
      []; // danh sách sản phẩm đang đầu tư có thể cầm cố
  List<String> stockTypeList = []; //danh sách chứng khoán có thể vay
  int? selectedIndex = null;
  int? selectedCusProIdIndex = null; //mã gói vay của khách hàng
  int? selectedBorrowIndex = null;
  int? selectedStockTypeIndex = null;
  double? maximumBorrow = 0;
  double? maxBorrow = 0; //Mức vay tối đa

  BorrowStockBody({
    Key? key,
    this.productModel,
  }) : super(key: key) {
    listPeriod = this.productModel?.interestRateProductModel?.items ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<BorrowCubit>(),
      loadingWidget: CustomLoading<BorrowCubit>(),
      title: productModel?.name,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              renderPeriod(),
              SizedBox(height: 20),
              renderStockTypeList(),
              SizedBox(height: 10),
              CustomTextInput(
                getTextFieldValue: (String value) {
                  if (keyMoney.currentState?.isValid ?? false) {
                    setStateProfit?.call(() {});
                  }
                },
                enableBorder: true,
                key: keyMoney,
                textController: TextEditingController(),
                hideUnderline: true,
                hintText: "",
                formatCurrency: true,
                maxLength: 18,
                keyboardType: TextInputType.number,
                validator: (String value) {
                  if (value.trim().isEmpty) {
                    return "Vui lòng nhập số chứng khoán vay";
                  }
                  try {
                    int.parse(value.replaceAll(",", ""));
                  } catch (e) {
                    return "Số chứng khoán nhập không đúng định dạng";
                  }
                  return "";
                },
                title: "Số chứng khoán vay",
              ),
              SizedBox(height: 20),
              renderInfoProfit(context),
              SizedBox(height: 30),
              CommonWidget.baseLineWidget(),
              BaseButton(
                onTap: () {
                  bool valid = true;
                  if (!(keyMoney.currentState?.isValid ?? false)) {
                    valid = false;
                  }
                  if (!(_stockType.currentState?.isValid ?? false)) {
                    valid = false;
                  }
                  if (!(_customerProduct.currentState?.isValid ?? false)) {
                    valid = false;
                  }

                  var borrowNum = Common.strToInt(
                      Common.moneyToInt(
                          keyMoney.currentState?.value.toString()),
                      defaultValue: 0);
                  if (valid) {
                    if (borrowNum > maxBorrow!) {
                      CustomDialog.showDialogConfirm(context,
                          showCancel: false,
                          showIconClose: false,
                          barrierDismissible: false,
                          content:
                              "Gửi yêu cầu vay thất bại, số chứng khoán nhập không được vượt quá mức vay tối đa.",
                          titleOK: "Đóng",
                          onTapOK: () {});
                    } else {
                      BlocProvider.of<BorrowCubit>(context).borrow({
                        "cid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                        "irId": listPeriod[indexSelect].irid,
                        "borrowNum": borrowNum,
                        "stockType": stockTypeList[selectedStockTypeIndex ?? 0]
                            .toString(),
                        "stockCode": "CASH",
                        "borrowType": false,
                        "cproid":
                            proListNotBlock![selectedCusProIdIndex ?? 0].id
                      });
                    }
                  }
                },
                width: double.infinity,
                margin: EdgeInsets.symmetric(vertical: 10),
                title: "Xác nhận",
              ),
              BlocListener<BorrowCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<bool> && state.data == true) {
                    proListNotBlock?.removeWhere((x) =>
                        x.id ==
                        proListNotBlock![selectedCusProIdIndex ?? 0].id);
                    BlocProvider.of<ListHistoryProductCubit>(context)
                        .getListHistoryProduct({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id
                    });
                    CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            showIconClose: false,
                            barrierDismissible: false,
                            content: "Chúc mừng bạn đặt lệnh thành công!",
                            titleOK: "Đóng",
                            onTapOK: () {})
                        .then((value) {
                      Navigator.pop(context);
                    });
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  renderPeriod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextLabel(
          "Vay chứng khoán - Kỳ hạn và tỷ suất Lãi hiện tại",
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: AppColors.base_color,
        ),
        SizedBox(height: 5),
        CustomTextLabel(
          "Tỷ suất lãi suất khi vay chứng khoán là không cố định và có thể thay đổi trong tương lai.",
          fontWeight: FontWeight.w400,
          fontSize: 14,
          fontStyle: FontStyle.italic,
          color: AppColors.ffBDBDBD,
        ),
        SizedBox(height: 5),
        Stack(
          children: [
            StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                return SingleChildScrollView(
                  padding: EdgeInsets.only(right: 30),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: listPeriod
                        .mapIndexed((e, index) => renderItemPeriod(
                            value: e,
                            index: index,
                            onTap: () {
                              setState(() {
                                indexSelect = index;
                              });
                              setStateProfit?.call(() {});
                              setStateStockTypeList?.call(() {});
                              selectedStockTypeIndex = null;
                            }))
                        .toList(),
                  ),
                );
              },
            ),
            Positioned(
              top: 0,
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: AppColors.border,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.keyboard_arrow_right_outlined,
                  size: 20,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget renderStockTypeList() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        proListNotBlock = productModel?.productNotBlocks;
        this.setStateStockTypeList = setState;

        try {
          InterestRate model = listPeriod[indexSelect];
          stockTypeList =
              json.decode(model.stockList ?? "[]").cast<String>().toList();
          var removeStockCash = stockTypeList.remove("CASH");
        } catch (e) {}

        return Column(
          children: [
            CustomDropDown(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: AppColors.ffBDBDBD, width: 1),
                didSelected: (int index) {
                  try {
                    selectedStockTypeIndex = index;
                  } catch (e) {
                    print("===== build =====${e}");
                  }
                  setStateProfit?.call(() {});
                  
                  if (selectedCusProIdIndex != null) {
                    BlocProvider.of<CheckBorrowMaximumCubit>(context)
                        .checkBorrowMaximum({
                      "cproid": proListNotBlock![selectedCusProIdIndex ?? 0].id,
                      "nameIrBorrow": "CKTM",
                      "stockCode": stockTypeList[selectedStockTypeIndex ?? 0].toString(),
                    });
                  }
                },
                key: _stockType,
                isRequired: true,
                hintText: "Chọn loại chứng khoán vay",
                listValues: stockTypeList),
            SizedBox(height: 10),
            CustomDropDown(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: AppColors.ffBDBDBD, width: 1),
                key: _customerProduct,
                selectedIndex: selectedCusProIdIndex,
                renderItem: (int index) {
                  var value = proListNotBlock![index];
                  return Row(
                    children: [
                      BaseNetworkImage(
                        url: ApiConstant.getImageHost(value.logo),
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: CustomTextLabel(
                          value.name,
                          color: AppColors.black,
                          maxLines: 2,
                        ),
                      )
                    ],
                  );
                },
                isRequired: true,
                didSelected: (int index) {
                  try {
                    selectedCusProIdIndex = index;
                    var interestBorrows = productModel?.interestBorrows;
                    var interest = interestBorrows
                        ?.where((x) => x.name == "TMTM")
                        .first
                        .percentBorrow
                        ?.round();
                    maximumBorrow =
                        ((proListNotBlock![selectedCusProIdIndex ?? 0]
                                    .originValue ??
                                0) *
                            ((interest ?? 0) / 100));
                  } catch (e) {
                    print("===build =====${e}");
                  }
                  setStateProfit?.call(() {});

                  if (selectedStockTypeIndex != null) {
                    BlocProvider.of<CheckBorrowMaximumCubit>(context)
                        .checkBorrowMaximum({
                      "cproid": proListNotBlock![selectedCusProIdIndex ?? 0].id,
                      "nameIrBorrow": "CKTM",
                      "stockCode":
                          stockTypeList[selectedStockTypeIndex ?? 0].toString(),
                    });
                  }
                },
                hintText: "Chọn gói đầu tư cầm cố",
                listValues: proListNotBlock?.map((e) => e.name ?? "").toList())
          ],
        );
      },
    );
  }

  Widget renderItemPeriod({onTap, required int index, InterestRate? value}) {
    String title = "";

    int period = value?.period ?? 0;
    if (period <= 30) {
      title = "${value?.period} Ngày";
    } else {
      title = "${Common.doubleWithoutDecimalToInt(period / 30)} Tháng";
    }
    bool isSelect = indexSelect == index;
    return BaseButton(
      borderColor: isSelect ? AppColors.ff63C856 : AppColors.ffDBDBDB,
      margin: EdgeInsets.only(right: 10, top: 5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      backgroundColor: isSelect ? AppColors.ffF3F3F3 : AppColors.white,
      onTap: onTap,
      child: Container(
        height: 80,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: AppColors.black,
            ),
            SizedBox(height: 5),
            CustomTextLabel(
              "${value?.interestRate}%/ năm",
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: AppColors.black,
            ),
          ],
        ),
      ),
    );
  }

  renderInfoProfit(BuildContext context) {
    return BlocBuilder<CheckBorrowMaximumCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<dynamic>) {
          var priceStock = state.data['StockPrice'];
          var InterestPeriod = state.data['BorrowMaximumFormat'];
          maxBorrow = (state.data['BorrowMaximum'] ?? 0).toDouble();
          return StatefulBuilder(
            builder: (BuildContext context,
                void Function(void Function()) setState) {
              this.setStateProfit = setState;
              this.setStateMaximumBorrow = setState;

              int money = 0;
              double profit = 0.0;
              var stockName = selectedStockTypeIndex != null
                  ? stockTypeList[selectedStockTypeIndex ?? 0]
                  : "";

              if (selectedStockTypeIndex == null) {
                money = 0;
                profit = 0;
              } else {
                try {
                  money = Common.strToInt(
                      Common.moneyToInt(
                          keyMoney.currentState?.value.toString()),
                      defaultValue: 0);
                  InterestRate model = listPeriod[indexSelect];
                  // double ratePerMonth = ((model.interestRate ?? 0) / 12);
                  // profit = (money * (((model.period ?? 0) / 30) * ratePerMonth)) / 100;
                  profit = (money * (model.period ?? 0) * ((model.interestRate ?? 0) / 100)) / 365;
                } catch (e) {}
              }

              var interestBorrows = productModel?.interestBorrows;
              var interest = interestBorrows
                  ?.where((x) => x.name == "CKTM")
                  .first
                  .percentBorrow
                  ?.round();

              final numberFormat = NumberFormat("#,###");

              return selectedStockTypeIndex != null
                  ? Column(
                      children: [
                        Row(
                          children: [
                            CustomTextLabel(
                              'Giá chứng khoán ${stockTypeList[selectedStockTypeIndex ?? 0].toString()}: ',
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: AppColors.ff222222,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: CustomTextLabel(
                                  priceStock,
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: AppColors.ff222222,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            CustomTextLabel(
                              "Tỉ lệ cầm cố: ",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: AppColors.ff222222,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: CustomTextLabel(
                                  '${interest}%',
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: AppColors.ff222222,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            CustomTextLabel(
                              "Mức vay tối đa: ",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: AppColors.ff222222,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: CustomTextLabel(
                                  InterestPeriod,
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: AppColors.ff222222,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            CustomTextLabel(
                              "Tổng lãi suất:",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: AppColors.ff222222,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: CustomTextLabel(
                                  '${numberFormat.format(double.parse(profit.round().toString()).round())} ${stockName}',
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: AppColors.ff222222,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            CustomTextLabel(
                              "Tổng vay & lãi suất:",
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: AppColors.ff222222,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: CustomTextLabel(
                                  "${numberFormat.format(double.parse((profit + money).toString()).round())} ${stockName}",
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: AppColors.ff222222,
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  : Column();
            },
          );
        }
        return SizedBox.shrink();
      },
    );
  }
}
