import 'dart:convert';
import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class OpenOrderStockScreen extends StatelessWidget {
  final ProductModel? productModel;

  const OpenOrderStockScreen({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<BuyProductCubit>(
            create: (_) => BuyProductCubit(),
          ),
          BlocProvider<DetailProductCubit>(
            create: (_) => DetailProductCubit()
              ..getDetailProduct(productModel,
                  UserInfoBuilderWidget.getUserInfo(context)?.id ?? 0),
          )
        ],
        child: OpenOrderStockBody(
          productModel: productModel,
        ));
  }
}

class OpenOrderStockBody extends StatelessWidget {
  final ProductModel? productModel;

  List<InterestRate> listPeriod = [];
  int indexSelect = 0;
  GlobalKey<TextFieldState> keyMoney = GlobalKey();
  var setStateProfit;
  var setStateStockTypeList;

  final _stockCode = GlobalKey<CustomDropDownState>();
  List<BankInfoModel> listData = [];
  List<ProductNotBlockModel>? proListNotBlock =
      []; // danh sách sản phẩm đang đầu tư có thể cầm cố
  List<String> stockTypeList = []; //danh sách chứng khoán có thể vay
  int? selectedIndex = null;
  int? selectedCusProIdIndex = null; //mã gói vay của khách hàng
  int? selectedStockTypeIndex = null;

  OpenOrderStockBody({
    Key? key,
    this.productModel,
  }) : super(key: key) {
    listPeriod = this
            .productModel
            ?.interestRateProductModel
            ?.items
            ?.where((e) => e.stockList != null)
            .toList() ??
        [];
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<BuyProductCubit>(),
      loadingWidget: CustomLoading<BuyProductCubit>(),
      title: productModel?.name,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // renderPeriod(),
              productModel?.isIndefinite != true
                  ? renderPeriod()
                  : CustomTextLabel(
                      productModel?.description,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: AppColors.base_color,
                    ),
              SizedBox(height: 20),
              renderStockTypeList(),
              SizedBox(height: 10),
              CustomTextInput(
                getTextFieldValue: (String value) {
                  if (keyMoney.currentState?.isValid ?? false) {
                    setStateProfit?.call(() {});
                  }
                },
                enableBorder: true,
                key: keyMoney,
                textController: TextEditingController(),
                hideUnderline: true,
                hintText: "",
                formatCurrency: true,
                maxLength: 18,
                keyboardType: TextInputType.number,
                validator: (String value) {
                  if (value.trim().isEmpty) {
                    return "Vui lòng nhập số chứng khoán đầu tư";
                  }
                  try {
                    int.parse(value.replaceAll(",", ""));
                  } catch (e) {
                    return "Số chứng khoán nhập không đúng định dạng";
                  }
                  return "";
                },
                title: "Số chứng khoán đầu tư",
              ),
              SizedBox(height: 20),
              renderInfoProfit(),
              SizedBox(height: 20),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, Routes.detailInvestScreen);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue, width: 1),
                          borderRadius: BorderRadius.circular(20)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Row(
                        children: [
                          Text(
                            "Chi tiết",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                            ),
                          ),
                          SizedBox(width: 3),
                          Icon(
                            Icons.arrow_forward,
                            size: 18,
                            color: Colors.blue,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              CommonWidget.baseLineWidget(),
              BaseButton(
                onTap: () {
                  bool valid = true;
                  if (!(keyMoney.currentState?.isValid ?? false)) {
                    valid = false;
                  }
                  if (!(_stockCode.currentState?.isValid ?? false)) {
                    valid = false;
                  }

                  if (valid) {
                    BlocProvider.of<BuyProductCubit>(context).buyProduct({
                      "paymentType": "",
                      "description": "Đầu tư chứng khoán",
                      "cusId": UserInfoBuilderWidget.getUserInfo(context)?.id,
                      "irId": listPeriod[indexSelect].irid,
                      "originvalue": Common.moneyToInt(
                          keyMoney.currentState?.value.toString()),
                      "stockCode":
                          stockTypeList[selectedStockTypeIndex ?? 0].toString()
                    });
                  }
                },
                width: double.infinity,
                margin: EdgeInsets.symmetric(vertical: 10),
                title: "Xác nhận",
              ),
              BlocListener<BuyProductCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<bool> && state.data == true) {
                    BlocProvider.of<ListHistoryProductCubit>(context)
                        .getListHistoryProduct({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id
                    });
                    CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            showIconClose: false,
                            barrierDismissible: false,
                            content: "Chúc mừng bạn đặt lệnh thành công!",
                            titleOK: "Đóng",
                            onTapOK: () {})
                        .then((value) {
                      Navigator.pop(context);
                    });
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  renderPeriod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextLabel(
          "Đầu tư chứng khoán - Kỳ hạn và tỷ suất lợi nhuận",
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: AppColors.base_color,
        ),
        SizedBox(height: 5),
        Stack(
          children: [
            StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                return SingleChildScrollView(
                  padding: EdgeInsets.only(right: 30),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: listPeriod
                        .mapIndexed((e, index) => renderItemPeriod(
                            value: e,
                            index: index,
                            onTap: () {
                              setState(() {
                                indexSelect = index;
                              });
                              setStateProfit?.call(() {});
                              setStateStockTypeList?.call(() {});
                              selectedStockTypeIndex = null;
                            }))
                        .toList(),
                  ),
                );
              },
            ),
            Positioned(
              top: 0,
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: AppColors.border,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.keyboard_arrow_right_outlined,
                  size: 20,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget renderStockTypeList() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        this.setStateStockTypeList = setState;

        try {
          InterestRate model = listPeriod[indexSelect];
          stockTypeList =
              json.decode(model.stockList ?? "[]").cast<String>().toList();
          var removeStockCash = stockTypeList.remove("CASH");
        } catch (e) {}
        return Column(
          children: [
            CustomDropDown(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: AppColors.ffBDBDBD, width: 1),
                didSelected: (int index) {
                  try {
                    selectedStockTypeIndex = index;
                  } catch (e) {
                    print("===== build =====${e}");
                  }
                  setStateProfit?.call(() {});
                },
                key: _stockCode,
                isRequired: true,
                hintText: "Chọn loại chứng đầu tư",
                listValues: stockTypeList),
          ],
        );
      },
    );
  }

  Widget renderItemPeriod({onTap, required int index, InterestRate? value}) {
    String title = "";

    int period = value?.period ?? 0;
    if (period <= 30) {
      title = "${value?.period} Ngày";
    } else {
      title = "${Common.doubleWithoutDecimalToInt(period / 30)} Tháng";
    }
    bool isSelect = indexSelect == index;
    return BaseButton(
      borderColor: isSelect ? AppColors.ff63C856 : AppColors.ffDBDBDB,
      margin: EdgeInsets.only(right: 10, top: 5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      backgroundColor: isSelect ? AppColors.ffF3F3F3 : AppColors.white,
      onTap: onTap,
      child: Container(
        height: 80,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: AppColors.black,
            ),
            SizedBox(height: 5),
            CustomTextLabel(
              "${value?.interestRate}%/ năm",
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: AppColors.black,
            ),
          ],
        ),
      ),
    );
  }

  renderInfoProfit() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        this.setStateProfit = setState;
        int money = 0;
        double profit = 0.0;

        var stockName = selectedStockTypeIndex != null
            ? stockTypeList[selectedStockTypeIndex ?? 0].toString()
            : "";

        if (selectedStockTypeIndex == null) {
          money = 0;
          profit = 0;
        } else {
          try {
            money = Common.strToInt(
                Common.moneyToInt(keyMoney.currentState?.value.toString()),
                defaultValue: 0);
            InterestRate model = listPeriod[indexSelect];
            // double ratePerDay = ((model.interestRate ?? 0) / 365);
            // profit = (money * (((model.period ?? 0)) * ratePerDay)) / 100;
            profit = (money *
                        (model.period ?? 0) *
                        ((model.interestRate ?? 0) / 100)) /
                    365 +
                (money * (model.period ?? 0) * ((model.addIR ?? 0)/100)) / 365;
          } catch (e) {}
        }
        final numberFormat = NumberFormat("#,###");

        return selectedStockTypeIndex != null
            ? Column(
                children: [
                  Row(
                    children: [
                      CustomTextLabel(
                        "Tổng lợi nhuận:",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: CustomTextLabel(
                            '${numberFormat.format(double.parse(profit.round().toString()).round())} ${stockName}',
                            formatCurrency: false,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: AppColors.ff222222,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      CustomTextLabel(
                        "Gốc & lợi nhuận:",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: CustomTextLabel(
                            "${numberFormat.format(double.parse((profit + money).toString()).round())} ${stockName}",
                            formatCurrency: false,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: AppColors.ff222222,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )
            : Column();
      },
    );
  }
}
