import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailInvestScreen extends StatelessWidget {
  const DetailInvestScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<CloseAccountCubit>(
        create: (_) => CloseAccountCubit(),
      )
    ], child: DetailInvestBody());
  }
}

class DetailInvestBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        messageNotify: CustomSnackBar<CloseAccountCubit>(),
        loadingWidget: CustomLoading<CloseAccountCubit>(),
        title: "Chi tiết cách tính lãi",
        body: Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: AppColors.ffC8FFC1, shape: BoxShape.circle),
                        child: Text(
                          "1",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomTextLabel(
                          "Định nghĩa",
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff00A95E,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "A :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: CustomTextLabel(
                            "Gốc (Số tiền bạn muốn đầu tư)",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: AppColors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "B :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: CustomTextLabel(
                            "Lãi suất",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: AppColors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "C :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: CustomTextLabel(
                            "Kỳ hạn đầu tư",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: AppColors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "D :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                            // width: 300,
                            padding: EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomTextLabel(
                                  "Lãi suất cộng thêm",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: AppColors.black,
                                ),
                                // CustomTextLabel(
                                //   "(dành cho gói cố đỊnh + thả nổi)",
                                //   fontWeight: FontWeight.w400,
                                //   fontSize: 16,
                                //   color: AppColors.black,
                                // ),
                              ],
                            ))
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "E :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomTextLabel(
                                  "Pool",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: AppColors.black,
                                ),
                                CustomTextLabel(
                                  "(Tổng đã cho vay / tổng đầu tư)",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: AppColors.black,
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, right: 10),
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "P :",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomTextLabel(
                                  "Lợi nhuận",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color: AppColors.black,
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: AppColors.ffC8FFC1, shape: BoxShape.circle),
                        child: Text(
                          "2",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomTextLabel(
                          "Gói cố định",
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff00A95E,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                    decoration: BoxDecoration(
                        color: AppColors.ffF3F3F3,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: CustomTextLabel(
                            "P = ( A * B * C ) / 365",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: AppColors.ffC8FFC1, shape: BoxShape.circle),
                        child: Text(
                          "3",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomTextLabel(
                          "Gói cố định + thả nổi",
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff00A95E,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                    decoration: BoxDecoration(
                        color: AppColors.ffF3F3F3,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: CustomTextLabel(
                            "P = ( A * B * C ) / 365 + ( A * D * C * E ) / 365",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: AppColors.ffC8FFC1, shape: BoxShape.circle),
                        child: Text(
                          "4",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomTextLabel(
                          "Gói thả nổi",
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff00A95E,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                    decoration: BoxDecoration(
                        color: AppColors.ffF3F3F3,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: CustomTextLabel(
                            "P = ( A * B * C * E ) / 365",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            )));
  }
}
