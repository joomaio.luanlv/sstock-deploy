import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/api_constant.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum PaymentType { CK, TM }

class OpenOrderScreen extends StatelessWidget {
  final ProductModel? productModel;

  const OpenOrderScreen({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<BuyProductCubit>(
            create: (_) => BuyProductCubit(),
          ),
          BlocProvider<ListBankInfoCubit>(
            create: (_) => ListBankInfoCubit()..getListBankInfo(),
          ),
          BlocProvider<ProfitCubit>(
            create: (_) => ProfitCubit(),
          ),
        ],
        child: OpenOrderBody(
          productModel: productModel,
        ));
  }
}

class OpenOrderBody extends StatelessWidget {
  final ProductModel? productModel;

  List<InterestRate> listPeriod = [];
  int indexSelect = 0;
  GlobalKey<TextFieldState> keyMoney = GlobalKey();
  var setStateProfit;

  GlobalKey<TextFieldState> keyStk = GlobalKey();
  GlobalKey<TextFieldState> keyTenTK = GlobalKey();
  PaymentType? paymentType = PaymentType.CK;
  final _formDropdownBank = GlobalKey<CustomDropDownState>();
  List<BankInfoModel> listData = [];
  int? selectedIndex = null;

  OpenOrderBody({
    Key? key,
    this.productModel,
  }) : super(key: key) {
    listPeriod = this.productModel?.interestRateProductModel?.items ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<BuyProductCubit>(),
      loadingWidget: CustomLoading<BuyProductCubit>(),
      title: productModel?.name,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              productModel?.isIndefinite != true
                  ? renderPeriod()
                  : CustomTextLabel(
                      productModel?.description,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: AppColors.base_color,
                    ),
              SizedBox(height: 10),
              CustomTextInput(
                getTextFieldValue: (String value) {
                  if (keyMoney.currentState?.isValid ?? false) {
                    setStateProfit?.call(() {});
                    //print(value);
                    BlocProvider.of<ProfitCubit>(context).getProfit({
                      "irId": listPeriod[indexSelect].irid,
                      "originvalue": Common.moneyToInt(
                          keyMoney.currentState?.value.toString()),
                    });
                  }
                },
                margin: EdgeInsets.symmetric(vertical: 20),
                enableBorder: true,
                key: keyMoney,
                textController: TextEditingController(),
                hideUnderline: true,
                hintText: "",
                formatCurrency: true,
                maxLength: 18,
                keyboardType: TextInputType.number,
                validator: (String value) {
                  if (value.trim().isEmpty) {
                    return "Vui lòng nhập số tiền đầu tư";
                  }
                  try {
                    int.parse(value.replaceAll(",", ""));
                  } catch (e) {
                    return "Số tiền nhập không đúng định dạng";
                  }
                  return "";
                },
                title: "Số tiền đầu tư",
              ),
              SizedBox(height: 10),
              renderInfoProfit(),
              // SizedBox(height: 20),
              // InkWell(
              //   onTap: () {
              //     Navigator.pushNamed(context, Routes.detailInvestScreen);
              //   },
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       Container(
              //         decoration: BoxDecoration(
              //             border: Border.all(color: Colors.blue, width: 1),
              //             borderRadius: BorderRadius.circular(20)),
              //         padding:
              //             EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //         child: Row(
              //           children: [
              //             Text(
              //               "Chi tiết",
              //               style: TextStyle(
              //                 fontSize: 16,
              //                 color: Colors.blue,
              //               ),
              //             ),
              //             SizedBox(width: 3),
              //             Icon(
              //               Icons.arrow_forward,
              //               size: 18,
              //               color: Colors.blue,
              //             )
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              CommonWidget.baseLineWidget(
                  margin: EdgeInsets.symmetric(vertical: 20)),
              renderTypePayment(),
              SizedBox(height: 50),
              CommonWidget.baseLineWidget(),
              BaseButton(
                onTap: () {
                  bool valid = true;
                  if (!(keyMoney.currentState?.isValid ?? false)) {
                    valid = false;
                  }

                  if (valid) {
                    BlocProvider.of<BuyProductCubit>(context).getContract({
                      "cusId": UserInfoBuilderWidget.getUserInfo(context)?.id,
                      "irId": listPeriod[indexSelect].irid,
                      "originvalue": Common.moneyToInt(
                          keyMoney.currentState?.value.toString()),
                    });
                  }
                },
                width: double.infinity,
                margin: EdgeInsets.symmetric(vertical: 10),
                title: "Ký hợp đồng và chuyển tiền",
              ),
              BlocListener<BuyProductCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<Contract>) {
                    BlocProvider.of<ListHistoryProductCubit>(context)
                        .getListHistoryProduct({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id
                    });

                    Navigator.pushNamed(context, Routes.pdfHDViewWidget,
                        arguments: PDFHDViewWidgetArg(
                            initialUrl: ApiConstant.getImageHost(
                                state.data.contractLink),
                            title: "Hợp đồng",
                            paymentType: this.paymentType == PaymentType.CK
                                ? "CK"
                                : "TM",
                            irId: listPeriod[indexSelect].irid,
                            code: state.data.code,
                            contractLink: state.data.contractLink,
                            originvalue: Common.moneyToInt(
                                keyMoney.currentState?.value.toString())));

                    // CustomDialog.showDialogConfirm(context,
                    //         showCancel: false,
                    //         showIconClose: false,
                    //         barrierDismissible: false,
                    //         content: "Ký hợp đồng và chuyển tiền thành công!",
                    //         titleOK: "Đóng",
                    //         onTapOK: () {})
                    //     .then((value) {
                    //   Navigator.pop(context);
                    // });
                  } else {
                    print("------------------------------------");
                    print(state is LoadedState<Contract>);
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  renderPeriod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextLabel(
          "Đầu tư tiền mặt - Kỳ hạn và tỷ suất Lợi nhuận",
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: AppColors.base_color,
        ),
        SizedBox(height: 5),
        Stack(
          children: [
            StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                return SingleChildScrollView(
                  padding: EdgeInsets.only(right: 30),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: listPeriod
                        .mapIndexed((e, index) => renderItemPeriod(
                            value: e,
                            index: index,
                            onTap: () {
                              setState(() {
                                indexSelect = index;
                              });
                              setStateProfit?.call(() {});
                            }))
                        .toList(),
                  ),
                );
              },
            ),
            Positioned(
              top: 0,
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: AppColors.border,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.keyboard_arrow_right_outlined,
                  size: 20,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget renderItemPeriod({onTap, required int index, InterestRate? value}) {
    String title = "";

    title = "${value?.period} ${value?.periodType}";
    bool isSelect = indexSelect == index;
    return BaseButton(
      borderColor: isSelect ? AppColors.ff63C856 : AppColors.ffDBDBDB,
      margin: EdgeInsets.only(right: 10, top: 5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      backgroundColor: isSelect ? AppColors.ffF3F3F3 : AppColors.white,
      onTap: onTap,
      child: Container(
        height: 80,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: AppColors.black,
            ),
            SizedBox(height: 5),
            CustomTextLabel(
              "${value?.interestRate}%/ năm",
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: AppColors.black,
            ),
            ((value?.addIR ?? 0) > 0)
                ? Column(
                    children: [
                      SizedBox(height: 5),
                      CustomTextLabel(
                        "+ ${value?.addIR}%",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: AppColors.base_color,
                      ),
                    ],
                  )
                : SizedBox(
                    height: 0,
                  )
          ],
        ),
      ),
    );
  }

  renderInfoProfit() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        // this.setStateProfit = setState;
        // int money = 0;
        // double profit = 0.0;
        // try {
        //   money = Common.strToInt(
        //       Common.moneyToInt(keyMoney.currentState?.value.toString()),
        //       defaultValue: 0);
        //   InterestRate model = listPeriod[indexSelect];
        //   profit = (money *
        //           (model.period ?? 0) *
        //           ((model.interestRate ?? 0) / 100)) +
        //       (money * (model.period ?? 0) * ((model.addIR ?? 0) / 100));
        // } catch (e) {}

        return renderProfit();
        // Column(
        //   children: [
        //     Row(
        //       children: [
        //         CustomTextLabel(
        //           "Lợi nhuận tối thiểu (*)",
        //           fontWeight: FontWeight.w400,
        //           fontSize: 16,
        //           color: AppColors.ff222222,
        //         ),
        //         SizedBox(width: 10),
        //         Expanded(
        //           child: Align(
        //             alignment: Alignment.centerRight,
        //             child: CustomTextLabel(
        //               profit,
        //               formatCurrency: true,
        //               fontWeight: FontWeight.w600,
        //               fontSize: 16,
        //               color: AppColors.ff222222,
        //             ),
        //           ),
        //         )
        //       ],
        //     ),
        //     SizedBox(height: 10),
        //     Row(
        //       children: [
        //         CustomTextLabel(
        //           "Gốc & lợi nhuận (*)",
        //           fontWeight: FontWeight.w400,
        //           fontSize: 16,
        //           color: AppColors.ff222222,
        //         ),
        //         SizedBox(width: 10),
        //         Expanded(
        //           child: Align(
        //             alignment: Alignment.centerRight,
        //             child: CustomTextLabel(
        //               "${profit + money}",
        //               formatCurrency: true,
        //               fontWeight: FontWeight.w600,
        //               fontSize: 16,
        //               color: AppColors.ff222222,
        //             ),
        //           ),
        //         )
        //       ],
        //     ),
        //   ],
        // );
      },
    );
  }

  renderProfit() {
    return BlocBuilder<ProfitCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<dynamic>) {
          var profit = state.data["Profit"];
          var total = state.data["Total"];
          return Column(
            children: [
              Row(
                children: [
                  CustomTextLabel(
                    "Lợi nhuận tối thiểu (*)",
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        profit,
                        formatCurrency: true,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  CustomTextLabel(
                    "Gốc & lợi nhuận (*)",
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        total,
                        formatCurrency: true,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                    ),
                  )
                ],
              ),
            ],
          );
        } else {
          return Column(
            children: [
              Row(
                children: [
                  CustomTextLabel(
                    "Lợi nhuận tối thiểu (*)",
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        0,
                        formatCurrency: true,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  CustomTextLabel(
                    "Gốc & lợi nhuận (*)",
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        0,
                        formatCurrency: true,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                    ),
                  )
                ],
              ),
            ],
          );
        }
      },
    );
  }

  renderTypePayment() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextLabel(
              "Phương thức thanh toán",
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: AppColors.base_color,
            ),
            SizedBox(height: 5),
            CustomTextLabel(
              "Vui lòng chuyển khoản số tiền đầu tư của Quý khách vào tài khoản công ty theo thông tin bên dưới",
              fontWeight: FontWeight.w400,
              fontSize: 14,
              fontStyle: FontStyle.italic,
              color: AppColors.ffBDBDBD,
            ),
            // Row(
            //   children: <Widget>[
            //     Expanded(
            //       child: Row(
            //         children: [
            //           Radio<PaymentType>(
            //             value: PaymentType.TM,
            //             groupValue: paymentType,
            //             onChanged: (PaymentType? value) {
            //               setState(() {
            //                 paymentType = value;
            //               });
            //             },
            //           ),
            //           const Text('Tiền mặt'),
            //         ],
            //       ),
            //     ),
            //     SizedBox(width: 10),
            //     Expanded(
            //       child: Row(
            //         children: [
            //           Radio<PaymentType>(
            //             value: PaymentType.CK,
            //             groupValue: paymentType,
            //             onChanged: (PaymentType? value) {
            //               setState(() {
            //                 paymentType = value;
            //               });
            //             },
            //           ),
            //           const Text('Chuyển khoản'),
            //         ],
            //       ),
            //     ),
            //   ],
            // ),
            renderInfoBankSend(context),
            // CommonWidget.baseLineWidget(
            //     margin: EdgeInsets.symmetric(vertical: 10)),

            // Container(
            //   width: 1000,
            //   child: Image.asset(
            //     AppImages.sstock_qr,
            //     width: 400,
            //     errorBuilder: (context, error, stackTrace) {
            //       return Text('Failed to load image');
            //     },
            //   ),
            // ),

            CommonWidget.baseLineWidget(
                margin: EdgeInsets.symmetric(vertical: 10)),
            renderInfoBankRecive(context)
          ],
        );
      },
    );
  }

  renderInfoBank(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<UserInfoCubit, BaseState>(
          builder: (_, state) {
            UserModel? userModel = UserInfoBuilderWidget.getUserInfo(context);
            if (state is LoadedState<UserModel>) {
              userModel = state.data;
            }
            return Container(
              child: Column(
                children: [
                  CommonWidget.renderActionBankWidget(
                      BankName: userModel?.bankName ?? "",
                      AccountNumber: userModel?.accountName ?? "",
                      AccountName: userModel?.accountNumber ?? "",
                      isDefault: true,
                      icon: AppImages.ic_account_private,
                      onTap: () {
                        Navigator.pushNamed(
                            context, Routes.listBankOfUserScreen);
                      }),
                ],
              ),
            );
          },
        ),
        // BlocBuilder<ListBankInfoCubit, BaseState>(
        //   builder: (_, state) {
        //     if (state is LoadedState<List<BankInfoModel>>) {
        //       listData = state.data;
        //     }
        //     return CustomDropDown(
        //         borderRadius: BorderRadius.circular(12),
        //         border: Border.all(color: AppColors.ffBDBDBD, width: 1),
        //         key: _formDropdownBank,
        //         selectedIndex: selectedIndex,
        //         renderItem: (int index) {
        //           var value = listData[index];
        //           return Row(
        //             children: [
        //               BaseNetworkImage(
        //                 url: value.logo,
        //                 width: 60,
        //                 height: 60,
        //               ),
        //               SizedBox(width: 5),
        //               Expanded(
        //                 child: CustomTextLabel(
        //                   value.name,
        //                   color: AppColors.black,
        //                   maxLines: 2,
        //                 ),
        //               )
        //             ],
        //           );
        //         },
        //         isRequired: true,
        //         didSelected: (int index) {
        //           try {
        //             selectedIndex = index;
        //           } catch (e) {
        //             print("===build =====${e}");
        //           }
        //         },
        //         hintText: "Chọn ngân hàng",
        //         listValues: listData.map((e) => e.name ?? "").toList());
        //   },
        // ),
        // SizedBox(height: 5),
        // CustomTextInput(
        //   margin: EdgeInsets.symmetric(vertical: 5),
        //   enableBorder: true,
        //   key: keyStk,
        //   textController: TextEditingController(),
        //   hideUnderline: true,
        //   hintText: "",
        //   validator: (String value) {
        //     if (value.trim().isEmpty) {
        //       return "Vui lòng nhập số tài khoản";
        //     }
        //     return "";
        //   },
        //   title: "Số tài khoản",
        // ),
        // SizedBox(height: 5),
        // CustomTextInput(
        //   margin: EdgeInsets.symmetric(vertical: 5),
        //   enableBorder: true,
        //   key: keyTenTK,
        //   textController: TextEditingController(),
        //   hideUnderline: true,
        //   hintText: "",
        //   validator: (String value) {
        //     if (value.trim().isEmpty) {
        //       return "Vui lòng nhập tên chủ tài khoản";
        //     }
        //     return "";
        //   },
        //   title: "Tên chủ tài khoản",
        // ),
      ],
    );
  }

  renderInfoBankSend(BuildContext context) {
    return Column(
      children: [
        _ItemInfo(
          label: "Tên ngân hàng:",
          value: "Techcombank",
          showLine: false,
        ),
        _ItemInfo(
          label: "Số tài khoản:",
          value: "19037404993013 ",
          showLine: false,
          copyable: true,
        ),
        _ItemInfo(
          label: "Tên chủ tài khoản:",
          value: "CÔNG TY TNHH ĐẦU TƯ TRĂNG NON",
          showLine: false,
        ),
      ],
    );
  }

  Widget renderInfoBankRecive(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        CustomTextLabel(
          "Tài khoản Ngân hàng của Quý khách",
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: AppColors.base_color,
        ),
        SizedBox(height: 5),
        CustomTextLabel(
          "Tài khoản ngân hàng của Quý khách để nhận tiền Gốc và Lợi nhuận khi kết thúc đầu tư",
          fontWeight: FontWeight.w400,
          fontSize: 14,
          fontStyle: FontStyle.italic,
          color: AppColors.ffBDBDBD,
        ),
        SizedBox(height: 15),
        renderInfoBank(context),
      ],
    );
  }
}

class _ItemInfo extends StatelessWidget {
  final String? label, value;
  final bool showLine;
  final bool copyable;
  final bool formatCurrency;

  const _ItemInfo(
      {Key? key,
      this.label,
      this.value,
      this.showLine = false,
      this.copyable = false,
      this.formatCurrency = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // color: Colors.green,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                // height: 25,
                alignment: Alignment.centerLeft,
                child: CustomTextLabel(
                  label,
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                  color: AppColors.black,
                ),
              ),
              Expanded(
                child: Container(
                  // height: 28,
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomTextLabel(
                          value,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff23262B,
                          formatCurrency: formatCurrency,
                          overflow: TextOverflow.visible,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Visibility(
                        child: SizedBox(
                          width: 5,
                        ),
                        visible: copyable,
                      ),
                      Visibility(
                        child: InkWell(
                          onTap: () {
                            Clipboard.setData(ClipboardData(
                              text: value ?? "",
                            ));
                            Fluttertoast.showToast(
                                msg: "Sao chép thành công",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: AppColors.ffF0084F4,
                                textColor: Colors.white,
                                fontSize: 14.0);
                          },
                          child: Image.asset(
                            AppImages.walletIcCopy,
                            width: 33,
                            height: 33,
                            fit: BoxFit.contain,
                          ),
                        ),
                        visible: copyable,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Visibility(
          child: Container(
            height: 1,
            color: AppColors.ffEBEEF2,
            margin: EdgeInsets.symmetric(horizontal: 10),
          ),
          visible: showLine,
        )
      ],
    );
  }
}
