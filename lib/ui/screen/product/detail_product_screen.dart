import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/constants.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/api_constant.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/images.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/screen/screen.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/shared_preference.dart';

class DetailProductScreen extends StatelessWidget {
  final ProductModel? productModel;

  const DetailProductScreen({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<DetailProductCubit>(
            create: (_) => DetailProductCubit()
              ..getDetailProduct(productModel,
                  UserInfoBuilderWidget.getUserInfo(context)?.id ?? 0),
          ),
          BlocProvider<ExpectedProfitCubit>(
            create: (_) => ExpectedProfitCubit(),
          ),
        ],
        child: _DetailProductBody(
          productModel: productModel,
        ));
  }
}

class _DetailProductBody extends StatelessWidget {
  final ProductModel? productModel;

  StateSetter? setStateIndex;
  int indexStack = 0;

  _DetailProductBody({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: productModel?.name,
      body: BlocBuilder<DetailProductCubit, BaseState>(
        builder: (_, state) {
          if (state is LoadedState<ProductModel?>) {
            return Container(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Expanded(
                    child: renderBody(),
                  ),
                  CommonWidget.baseLineWidget(),
                  SizedBox(height: 10),
                  BaseButton(
                    onTap: () async {
                      UserModel user = await SharedPreferenceUtil.getUserInfo();
                      if (user.fullName == null ||
                          user.accountName == null ||
                          user.accountNumber == null ||
                          user.address == null ||
                          user.cccd == null) {
                        CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            showIconClose: false,
                            barrierDismissible: false,
                            content:
                                "Vui lòng điền thông tin của bạn để nhận hợp đồng online!",
                            titleOK: "Cập nhật thông tin", onTapOK: () {
                          Navigator.pushNamed(
                              context, Routes.userInfoScreen);
                        });
                      } else {
                        var state =
                            BlocProvider.of<DetailProductCubit>(context).state;
                        var productModel = this.productModel;
                        if (state is LoadedState<ProductModel?>) {
                          productModel = state.data;
                          if (productModel?.isBorrow == true) {
                            Navigator.pushNamed(
                                context, Routes.borrowMoneyScreen,
                                arguments: productModel);
                          } else {
                            Navigator.pushNamed(context, Routes.openOrderScreen,
                                arguments: productModel);
                          }
                        }
                      }
                    },
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    title: productModel?.isBorrow == true
                        ? "Đặt lệnh vay tiền mặt"
                        : "Đặt lệnh",
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  renderBody() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        this.setStateIndex = setState;
        return Column(
          children: [
            Row(
              children: [
                // SizedBox(width: 10),
                // renderLabel(index: 0, title: "Tổng quan"),
                SizedBox(width: 10),
                renderLabel(index: 0, title: "Chính sách"),
              ],
            ),
            SizedBox(height: 5),
            Expanded(
                child: IndexedStack(
              index: indexStack,
              children: [
                // productModel?.isIndefinite == true
                //     ? OverviewNotFixScreen(proID: productModel?.id)
                //     : OverviewScreen(),
                ListPolicyScreen(productModel),
              ],
            ))
          ],
        );
      },
    );
  }

  renderLabel({required int index, required String title}) {
    bool isSelect = index == indexStack;
    return BaseButton(
      onTap: () {
        setStateIndex?.call(() {
          this.indexStack = index;
        });
      },
      borderRadius: 10,
      backgroundColor: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Container(
        child: IntrinsicWidth(
            child: Column(
          children: [
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: isSelect ? AppColors.base_color : AppColors.ff585858,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
              height: 2,
              color: isSelect ? AppColors.base_color : Colors.transparent,
            )
          ],
        )),
      ),
    );
  }
}

class OverviewNotFixScreen extends StatelessWidget {
  final int? proID;
  const OverviewNotFixScreen({Key? key, required this.proID}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var profit =
        BlocProvider.of<ExpectedProfitCubit>(context).getExpectedProfit({
      "period": 7,
      "proid": proID ?? 7,
      "originValue": 500000,
      "stockCode": "",
    });
    return BaseScreen(
      hideAppBar: true,
      messageNotify: CustomSnackBar<ExpectedProfitCubit>(),
      loadingWidget: CustomLoading<ExpectedProfitCubit>(), //ExpectedProfitCubit

      body: BlocBuilder<DetailProductCubit, BaseState>(
        builder: (_, state) {
          if (state is LoadedState<ProductModel?>) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextLabel(
                          state.data?.name,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff585858,
                        ),
                        SizedBox(height: 6),
                        Image.asset(
                          AppImages.ic_list_bank,
                          height: 32,
                          fit: BoxFit.fitWidth,
                        ),
                        SizedBox(height: 10),
                        CustomTextLabel(
                          state.data?.description,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: AppColors.ff585858,
                        ),
                      ],
                    ),
                  ),
                  CommonWidget.baseLineWidget(
                    margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                  ),
                  renderStockTypeList()
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  renderStockTypeList() {
    RangeValues _currentRangeValues = const RangeValues(0, 7);
    var originValue = 500000;
    var selectedIndex = 1;

    return BlocBuilder<ExpectedProfitCubit, BaseState>(builder: (_, state) {
      return StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState) {
          return Column(
            children: [
              CustomTextLabel(
                "TIỀN LỜI DỰ KIẾN".toUpperCase(),
                fontWeight: FontWeight.w600,
                fontSize: 17,
                color: AppColors.ff222222,
              ),
              SizedBox(height: 15),
              CustomTextLabel(
                (state is LoadedState<dynamic>) ? '${state.data}' : "0",
                fontWeight: FontWeight.w600,
                fontSize: 25,
                color: AppColors.base_color,
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 25, right: 25, top: 20, bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    CustomTextLabel(
                      "Với số tiền chọn thử",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: AppColors.ff222222,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 1;
                          originValue = 500000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 500000,
                            "stockCode": "",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 1
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "500.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 1
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 2;
                          originValue = 1000000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 1000000,
                            "stockCode": "",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 2
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "1.000.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 2
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 3;
                          originValue = 10000000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 10000000,
                            "stockCode": "",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 3
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "10.000.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 3
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 25, right: 25, top: 20, bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    CustomTextLabel(
                      "Với số chứng khoán VCB chọn thử",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: AppColors.ff222222,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 4;
                          originValue = 1000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 1000,
                            "stockCode": "VCB",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 4
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "1000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 4
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 5;
                          originValue = 10000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 10000,
                            "stockCode": "VCB",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 5
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "10.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 5
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 6;
                          originValue = 1000000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 100000,
                            "stockCode": "VCB",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 6
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "100.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 6
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = 7;
                          originValue = 1000000;
                          BlocProvider.of<ExpectedProfitCubit>(context)
                              .getExpectedProfit({
                            "period": (_currentRangeValues.end -
                                    _currentRangeValues.start)
                                .round(),
                            "proid": proID ?? 7,
                            "originValue": 1000000,
                            "stockCode": "VCB",
                          });
                        });
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: AppColors.ff00A95E, width: 1),
                          color: selectedIndex == 7
                              ? Colors.redAccent
                              : Colors.white,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: CustomTextLabel(
                          "1000.000",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: selectedIndex == 7
                              ? AppColors.white
                              : AppColors.ff222222,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 25, right: 25, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomTextLabel(
                        "Số ngày đầu tư",
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: AppColors.ff222222,
                      ),
                      CustomTextLabel(
                        "${(_currentRangeValues.end - _currentRangeValues.start).round()} ngày",
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: AppColors.base_color,
                      ),
                    ],
                  )),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: RangeSlider(
                  values: _currentRangeValues,
                  max: 30,
                  divisions: 30,
                  labels: RangeLabels(
                    _currentRangeValues.start.round().toString(),
                    _currentRangeValues.end.round().toString(),
                  ),
                  onChanged: (RangeValues values) {
                    setState(() {
                      _currentRangeValues = values;
                      BlocProvider.of<ExpectedProfitCubit>(context)
                          .getExpectedProfit({
                        "period": (_currentRangeValues.end -
                                _currentRangeValues.start)
                            .round(),
                        "proid": proID ?? 7,
                        "originValue": originValue,
                        "stockCode": "",
                      });
                    });
                  },
                ),
              )
            ],
          );
        },
      );
    });
    // ;
  }
}

class OverviewScreen extends StatefulWidget {
  const OverviewScreen({Key? key}) : super(key: key);

  @override
  State<OverviewScreen> createState() => _OverviewScreenState();
}

class _OverviewScreenState extends State<OverviewScreen> {
  @override
  Widget build(BuildContext context) {
    return _PointsLineChart();
  }
}

class _PointsLineChart extends StatelessWidget {
  final bool? animate;

  _PointsLineChart({this.animate = true});

  var axis = charts.NumericAxisSpec(
      renderSpec: charts.GridlineRendererSpec(
    labelStyle: charts.TextStyleSpec(
        fontSize: 15,
        fontFamily: "Verdana",
        color: charts.MaterialPalette.black),
  ));

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      hideAppBar: true,
      messageNotify: CustomSnackBar<DetailProductCubit>(),
      loadingWidget: CustomLoading<DetailProductCubit>(),
      body: BlocBuilder<DetailProductCubit, BaseState>(
        builder: (_, state) {
          if (state is LoadedState<ProductModel?>) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextLabel(
                          state.data?.name,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff585858,
                        ),
                        SizedBox(height: 6),
                        Image.asset(
                          AppImages.ic_list_bank,
                          height: 32,
                          fit: BoxFit.fitWidth,
                        ),
                        SizedBox(height: 10),
                        CustomTextLabel(
                          state.data?.description,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: AppColors.ff585858,
                        ),
                      ],
                    ),
                  ),
                  CommonWidget.baseLineWidget(
                    margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                  ),
                  CustomTextLabel(
                    state.data?.isBorrow == true
                        ? "Biều đồ lãi suất vay".toUpperCase()
                        : "Biểu đồ hiệu quả đầu tư".toUpperCase(),
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.ff222222,
                  ),
                  renderChart(state.data),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  renderChart(ProductModel? productModel) {
    var seriesList = _createSampleData(productModel?.interestRateProductModel);
    return Column(
      children: [
        renderInfoLine(productModel),
        Container(
          height: 400,
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: new charts.LineChart(seriesList,
              primaryMeasureAxis: axis,
              domainAxis: axis,
              behaviors: [
              ],
              animate: animate,
              defaultRenderer: new charts.LineRendererConfig(
                  includeLine: true, roundEndCaps: true, includePoints: true)),
        ),
      ],
    );
  }

  List<charts.Series<LinearSales, num>> _createSampleData(
      InterestRateProductModel? interestRateProductModel) {
    List<LinearSales> dataBank = [];
    interestRateProductModel?.banks?.forEach((element) {
      dataBank.add(LinearSales(element.period, element.bankIr));
    });
    charts.Series<LinearSales, num> chartBanks =
        new charts.Series<LinearSales, num>(
      id: 'bank',
      colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
      domainFn: (LinearSales sales, _) {
        return sales.period ?? 0;
      },
      measureFn: (LinearSales sales, _) => sales.value,
      displayName: "bank",
      data: dataBank,
    );
    List<LinearSales> dataProduct = [];
    interestRateProductModel?.items?.forEach((element) {
      dataProduct.add(LinearSales(element.period, element.interestRate));
    });
    charts.Series<LinearSales, num> chartProduct =
        new charts.Series<LinearSales, num>(
      id: 'product',
      colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
      domainFn: (LinearSales sales, _) {
        return sales.period ?? 0;
      },
      measureFn: (LinearSales sales, _) => sales.value,
      displayName: interestRateProductModel?.key,
      data: dataProduct,
    );

    return [
      chartBanks,
      chartProduct,
    ];
  }

  renderInfoLine(ProductModel? productModel) {
    return Container(
      margin: EdgeInsets.only(right: 15, top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          renderItem(title: productModel?.name, color: Colors.green),
          SizedBox(width: 20),
          renderItem(title: "VNIndex", color: Colors.red),
        ],
      ),
    );
  }

  Widget renderItem({String? title, Color? color}) {
    return Row(
      children: [
        Container(
          height: 1.5,
          color: color,
          width: 40,
        ),
        SizedBox(width: 4),
        CustomTextLabel(
          title,
          fontWeight: FontWeight.w400,
          fontSize: 12,
          color: AppColors.ff585858,
        ),
      ],
    );
  }
}

class ListPolicyScreen extends StatelessWidget {
  final ProductModel? productModel;

  const ListPolicyScreen(this.productModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PolicyProductCubit>(
        create: (_) => PolicyProductCubit()..getListPolicy(productModel),
        child: BaseScreen(
          hideAppBar: true,
          messageNotify: CustomSnackBar<PolicyProductCubit>(),
          loadingWidget: CustomLoading<PolicyProductCubit>(),
          body: renderListHistory(),
        ));
    ;
  }

  renderListHistory() {
    return BlocBuilder<PolicyProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<PolicyModel>>) {
          return ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return renderItemPolicy(context, state.data[index]);
            },
            itemCount: state.data.length,
            separatorBuilder: (BuildContext context, int index) {
              return CommonWidget.baseLineWidget(
                  margin: EdgeInsets.symmetric(vertical: 8));
            },
          );
        }
        return Container();
      },
    );
  }

  renderInfo(
      {required String title,
      value,
      FontWeight? fontWeightValue,
      double? fontSizeValue,
      Color? colorValue,
      AlignmentGeometry? alignment,
      bool formatCurrency = false}) {
    return Expanded(
        child: Align(
      alignment: alignment ?? Alignment.center,
      child: Column(
        children: [
          CustomTextLabel(
            title,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: AppColors.ff585858,
          ),
          SizedBox(height: 3),
          CustomTextLabel(
            value,
            fontWeight: fontWeightValue ?? FontWeight.w400,
            fontSize: fontSizeValue ?? 16,
            formatCurrency: formatCurrency,
            color: colorValue ?? AppColors.ff222222,
          ),
        ],
      ),
    ));
  }

  renderItemPolicy(BuildContext context, PolicyModel? data) {
    return InkWell(
      onTap: () {
        try {
          Navigator.pushNamed(context, Routes.pdfViewWidget,
              arguments: PDFViewWidgetArg(
                  initialUrl: ApiConstant.getImageHost(data?.linkDownload),
                  title: "Chính sách"));
        } catch (e) {
          print("===renderItemPolicy =====${e}");
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          children: [
            Image.asset(
              AppImages.ic_pdf,
              width: 40,
            ),
            SizedBox(width: 10),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextLabel(
                  data?.name,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  color: AppColors.ff585858,
                ),
                SizedBox(height: 5),
                CustomTextLabel(
                  Common.fromDate(
                      data?.createDateFormated, FormatDate.HH_mm_dd_mm_yy),
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: AppColors.ff585858,
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
