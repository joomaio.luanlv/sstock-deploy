import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../routes.dart';

class EditBankInfoScreen extends StatelessWidget {
  final BankOfCustomerModel? bankInfoModel;
  const EditBankInfoScreen({Key? key, this.bankInfoModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<CreateOrEditOfCusBankInfoCubit>(
          create: (_) => CreateOrEditOfCusBankInfoCubit()),
      BlocProvider<ListBankInfoCubit>(
        create: (_) => ListBankInfoCubit()..getListBankInfo(),
      ),
    ], child: EditBankInfoBody(bankInfoModel: bankInfoModel));
  }
}

class EditBankInfoBody extends StatelessWidget {
  final BankOfCustomerModel? bankInfoModel;
  EditBankInfoBody({Key? key, this.bankInfoModel}) : super(key: key);

  GlobalKey<TextFieldState> keyStk = GlobalKey();
  GlobalKey<TextFieldState> keyTenTK = GlobalKey();
  GlobalKey<TextFieldState> keyIsDefault = GlobalKey();

  final _formDropdownBank = GlobalKey<CustomDropDownState>();
  List<BankInfoModel> listData = [];
  int? selectedIndex = null;

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<CreateOrEditOfCusBankInfoCubit>(),
      loadingWidget: CustomLoading<CreateOrEditOfCusBankInfoCubit>(),
      title: "Sửa ngân hàng",
      rightWidgets: [
        BaseButton(
          child: CustomTextLabel(
            (bankInfoModel != null && bankInfoModel?.isDefault == true)
                ? "Mặc định"
                : "Đặt mặc định", // sau chuyển thành nút xoá
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: bankInfoModel?.isDefault == true
                ? AppColors.base_color
                : Color.fromARGB(255, 255, 6, 6),
          ),
          backgroundColor: Colors.transparent,
          onTap: () {
            if (bankInfoModel?.isDefault != true) {
              String bankName = "";
              int? bankId = null;
              bool valid = true;
              if (!(keyStk.currentState?.isValid ?? false)) {
                valid = false;
              }
              if (!(keyTenTK.currentState?.isValid ?? false)) {
                valid = false;
              }
              if (!(_formDropdownBank.currentState?.isValid ?? false)) {
                valid = false;
              }
              if (valid == true) {
                try {
                  bankName = listData[selectedIndex!].name ?? "";
                  bankId = listData[selectedIndex!].id;

                  if (bankId != null) {
                    BlocProvider.of<CreateOrEditOfCusBankInfoCubit>(context)
                        .CreateOrEditOfCusBankInfo({
                      "bankOfCusId": bankInfoModel?.id ?? 0,
                      "bankOnline_Id": bankId,
                      "bankName": bankName,
                      "accountNumber": keyStk.currentState?.value,
                      "accountName": keyTenTK.currentState?.value,
                      "isDefault": true
                    });
                  }

                  BlocProvider.of<UserInfoCubit>(context).getUserInfo();
                } catch (e) {}
              }

              // Navigator.pushNamed(context, Routes.editUserInfoScreen);
            }
          },
        )
      ],
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              renderTypePayment(),
            ],
          ),
        ),
      ),
    );
  }

  renderTypePayment() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [renderInfoBankRecive(context)],
        );
      },
    );
  }

  Widget renderInfoBankRecive(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        CustomTextLabel(
          "Thông tin tài khoản Ngân hàng của Quý khách",
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: AppColors.base_color,
        ),
        SizedBox(height: 5),
        CustomTextLabel(
          "Tài khoản ngân hàng của Quý khách để nhận tiền Gốc và Lợi nhuận khi kết thúc đầu tư",
          fontWeight: FontWeight.w400,
          fontSize: 14,
          fontStyle: FontStyle.italic,
          color: AppColors.ffBDBDBD,
        ),
        SizedBox(height: 15),
        renderInfoBank(),
        // CommonWidget.baseLineWidget(),
        BaseButton(
          onTap: () {
            String bankName = "";
            int? bankId = null;
            bool valid = true;
            if (!(keyStk.currentState?.isValid ?? false)) {
              valid = false;
            }
            if (!(keyTenTK.currentState?.isValid ?? false)) {
              valid = false;
            }
            if (!(_formDropdownBank.currentState?.isValid ?? false)) {
              valid = false;
            }
            if (valid == true) {
              try {
                bankName = listData[selectedIndex!].name ?? "";
                bankId = listData[selectedIndex!].id;

                if (bankId != null) {
                  BlocProvider.of<CreateOrEditOfCusBankInfoCubit>(context)
                      .CreateOrEditOfCusBankInfo({
                    "bankOfCusId": bankInfoModel?.id ?? 0,
                    "bankOnline_Id": bankId,
                    "bankName": bankName,
                    "accountNumber": keyStk.currentState?.value,
                    "accountName": keyTenTK.currentState?.value,
                  });
                }
              } catch (e) {}
            }
          },
          width: double.infinity,
          margin: EdgeInsets.symmetric(vertical: 10),
          title: "Lưu",
        ),
        BlocListener<CreateOrEditOfCusBankInfoCubit, BaseState>(
          listener: (_, state) {
            if (state is LoadedState<bool> && state.data == true) {
              //update bank of cus info here
              BlocProvider.of<UserInfoCubit>(context).getUserInfo();
              CustomDialog.showDialogConfirm(context,
                      showCancel: false,
                      showIconClose: false,
                      barrierDismissible: false,
                      content: "Cập nhật thông tin ngân hàng thành công!",
                      titleOK: "Đóng",
                      onTapOK: () {})
                  .then((value) {
                Navigator.pop(context);
              });
            }
          },
          child: Container(),
        )
      ],
    );
  }

  renderInfoBank() {
    return Column(
      children: [
        BlocBuilder<ListBankInfoCubit, BaseState>(
          builder: (_, state) {
            if (state is LoadedState<List<BankInfoModel>>) {
              listData = state.data;
              selectedIndex = bankInfoModel?.bankOnline_Id ?? null;
            }
            return CustomDropDown(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: AppColors.ffBDBDBD, width: 1),
                key: _formDropdownBank,
                selectedIndex: selectedIndex,
                renderItem: (int index) {
                  var value = listData[index];
                  return Row(
                    children: [
                      BaseNetworkImage(
                        url: value.logo,
                        width: 60,
                        height: 60,
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: CustomTextLabel(
                          value.name,
                          color: AppColors.black,
                          maxLines: 2,
                        ),
                      )
                    ],
                  );
                },
                isRequired: true,
                didSelected: (int index) {
                  try {
                    selectedIndex = index;
                  } catch (e) {
                    print("===build =====${e}");
                  }
                },
                hintText: "Chọn ngân hàng",
                listValues: listData.map((e) => e.name ?? "").toList());
          },
        ),
        SizedBox(height: 5),
        CustomTextInput(
          margin: EdgeInsets.symmetric(vertical: 5),
          enableBorder: true,
          key: keyStk,
          initData: bankInfoModel?.accountNumber,
          textController: TextEditingController(),
          hideUnderline: true,
          hintText: "",
          validator: (String value) {
            if (value.trim().isEmpty) {
              return "Vui lòng nhập số tài khoản";
            }
            return "";
          },
          title: "Số tài khoản",
        ),
        SizedBox(height: 5),
        CustomTextInput(
          margin: EdgeInsets.symmetric(vertical: 5),
          enableBorder: true,
          key: keyTenTK,
          initData: bankInfoModel?.accountName,
          textController: TextEditingController(),
          hideUnderline: true,
          hintText: "",
          validator: (String value) {
            if (value.trim().isEmpty) {
              return "Vui lòng nhập tên chủ tài khoản";
            }
            return "";
          },
          title: "Tên chủ tài khoản",
        ),
        SizedBox(height: 15),
      ],
    );
  }
}
