import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListBankOfUserScreen extends StatelessWidget {
  const ListBankOfUserScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<CloseAccountCubit>(
        create: (_) => CloseAccountCubit(),
      )
    ], child: ListBankOfUserBody());
  }
}

class ListBankOfUserBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<CloseAccountCubit>(),
      loadingWidget: CustomLoading<CloseAccountCubit>(),
      title: "Ngân hàng",
      rightWidgets: [
        BaseButton(
          child: CustomTextLabel(
            "Thêm mới",
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: AppColors.ff404A69,
          ),
          backgroundColor: Colors.transparent,
          onTap: () {
            Navigator.pushNamed(context, Routes.editBankInfoScreen);
          },
        )
      ],
      body: SingleChildScrollView(
        child: BlocBuilder<UserInfoCubit, BaseState>(
          builder: (_, state) {
            UserModel? userModel = UserInfoBuilderWidget.getUserInfo(context);
            if (state is LoadedState<UserModel>) {
              userModel = state.data;
            }
            if (userModel?.bankOfCustomers?.isNotEmpty ?? false) {
              return ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return CommonWidget.renderActionBankWidget(
                      BankName:
                          userModel?.bankOfCustomers?[index].bankName ?? "",
                      AccountNumber:
                          userModel?.bankOfCustomers?[index].accountName ?? "",
                      AccountName:
                          userModel?.bankOfCustomers?[index].accountNumber ??
                              "",
                      isDefault: userModel?.bankOfCustomers?[index].isDefault,
                      icon: AppImages.ic_account_private,
                      onTap: () {
                        BankOfCustomerModel? bankInfo =
                            userModel?.bankOfCustomers?[index];
                        Navigator.pushNamed(context, Routes.editBankInfoScreen,
                            arguments: bankInfo);
                      });
                },
                itemCount: userModel?.bankOfCustomers?.length ?? 0,
                separatorBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 15,
                  );
                },
              );
            } else {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Column(
                  children: [
                    CommonWidget.renderActionBankWidget(
                        BankName: userModel?.bankName ?? "",
                        AccountNumber: userModel?.accountName ?? "",
                        AccountName: userModel?.accountNumber ?? "",
                        isDefault: true,
                        icon: AppImages.ic_account_private,
                        onTap: () {
                          BankOfCustomerModel? bankInfo = new BankOfCustomerModel();
                          Navigator.pushNamed(
                              context, Routes.editBankInfoScreen,
                              arguments: bankInfo);
                        }),
                    CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
