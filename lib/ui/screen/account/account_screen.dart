import 'dart:developer';

import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      hideAppBar: true,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 70),
              CustomTextLabel(
                "Tài khoản",
                fontWeight: FontWeight.w600,
                fontSize: 28,
                color: AppColors.base_color,
              ),
              CommonWidget.baseLineWidget(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 20),
              ),
              renderInfoUser(),
              CommonWidget.baseLineWidget(
                height: 6,
                color: AppColors.ffF3F3F3,
                margin: EdgeInsets.only(top: 20),
              ),
              renderListAction(context),
              SizedBox(height: 20),
              BaseButton(
                onTap: () {
                  CustomDialog.showDialogConfirm(context,
                      content: "Bạn có muốn đăng xuất khỏi ứng dụng ?",
                      titleOK: "Đồng ý", onTapOK: () {
                    logout(context);
                  });
                },
                width: double.infinity,
                titleColor: AppColors.ff404A69,
                backgroundColor: AppColors.ffDBF2D7,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                title: "Đăng xuất",
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }

  void logout(BuildContext context) {
    SharedPreferenceUtil.clearData();
    Navigator.pushReplacementNamed(context, Routes.loginScreen);
  }

  renderInfoUser() {
    return UserInfoBuilderWidget(
      builder: (BuildContext context, UserModel? userModel) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: AppColors.ffF3F3F3, shape: BoxShape.circle),
                child: Image.asset(
                  AppImages.ic_user,
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextLabel(
                    userModel?.fullName,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(height: 5),
                  CustomTextLabel(
                    userModel?.mobilePhone,
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.ff585858,
                  ),
                ],
              )),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    color: AppColors.ffF0FFEE,
                    borderRadius: BorderRadius.circular(8)),
                child: CustomTextLabel(
                  "Đã xác thực",
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: AppColors.ff63C856,
                ),
              ),
              SizedBox(width: 10),
              Container(
                child: Image.asset(
                  AppImages.ic_arrow_right,
                  width: 10,
                  color: AppColors.ff585858,
                ),
              )
            ],
          ),
        );
      },
    );
  }

  renderListAction(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CommonWidget.renderActionWidget(
              title: "Thông tin cá nhân",
              icon: AppImages.ic_account_setting,
              onTap: () {
                Navigator.pushNamed(context, Routes.userInfoScreen);
              }),
          CommonWidget.baseLineWidget(),
          CommonWidget.renderActionWidget(
              title: "Bảo mật",
              icon: AppImages.ic_account_private,
              onTap: () {
                Navigator.pushNamed(context, Routes.securityInfoScreen);
              }),
          CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Liên kết tài khoản ngân hàng",
          //     icon: AppImages.ic_account_bank,
          //     onTap: () {}),
          // CommonWidget.baseLineWidget(),
          CommonWidget.renderActionWidget(
              title: "Mã giới thiệu",
              icon: AppImages.ic_account_invite,
              onTap: () {
                Navigator.pushNamed(context, Routes.referentScreen);
              }),
          CommonWidget.baseLineWidget(
            height: 6,
            color: AppColors.ffF3F3F3,
          ),
          // CommonWidget.renderActionWidget(
          //     title: "Dự kiến lãi và gốc đến hạn",
          //     icon: AppImages.ic_setting_pin,
          //     onTap: () {
          //       Navigator.pushNamed(context, Routes.packageDueScreen);
          //     }),
          // CommonWidget.baseLineWidget(),
          CommonWidget.baseLineWidget(
            height: 6,
            color: AppColors.ffF3F3F3,
          ),
          CommonWidget.renderActionWidget(
              title: "Câu hỏi thường gặp",
              icon: AppImages.ic_account_qa,
              onTap: () {
                Navigator.pushNamed(context, Routes.questionScreen);
              }),
          // CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Trung tâm trợ giúp",
          //     icon: AppImages.ic_account_help,
          //     onTap: () {}),
          // CommonWidget.baseLineWidget(),
          // CommonWidget.renderActionWidget(
          //     title: "Góp ý",
          //     icon: AppImages.ic_account_feedback,
          //     onTap: () {}),
          // CommonWidget.baseLineWidget(
          //   height: 6,
          //   color: AppColors.ffF3F3F3,
          // ),
          // CommonWidget.renderActionWidget(
          //     title: "Đánh giá ứng dụng",
          //     icon: AppImages.ic_account_rate,
          //     onTap: () {}),
        ],
      ),
    );
  }
}
