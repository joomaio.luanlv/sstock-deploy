import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/user_info_cubit.dart';
import 'package:app_sstock/data/model/user_model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditUserInfoScreen extends StatelessWidget {
  const EditUserInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UpdateUserInfoCubit>(
        create: (_) => UpdateUserInfoCubit(), child: EditUserInfoBody());
  }
}

class EditUserInfoBody extends StatelessWidget {
  EditUserInfoBody({Key? key}) : super(key: key);
  GlobalKey<TextFieldState> keyPhone = GlobalKey();
  GlobalKey<TextFieldState> keyFullName = GlobalKey();
  GlobalKey<TextFieldState> keyEmail = GlobalKey();
  GlobalKey<TextFieldState> keyMST = GlobalKey();
  GlobalKey<TextFieldState> keyAddress = GlobalKey();
  GlobalKey<TextFieldState> keyCccd = GlobalKey();
  GlobalKey<TextFieldState> keyProvideDate = GlobalKey();
  GlobalKey<TextFieldState> keyProvidePlace = GlobalKey();

  @override
  Widget build(BuildContext context) {
    UserModel? userModel = UserInfoBuilderWidget.getUserInfo(context);
    return BaseScreen(
      messageNotify: CustomSnackBar<UpdateUserInfoCubit>(),
      loadingWidget: CustomLoading<UpdateUserInfoCubit>(),
      title: "Chỉnh sửa thông tin cá nhân",
      rightWidgets: [
        BaseButton(
          child: CustomTextLabel(
            "Lưu",
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: AppColors.ff404A69,
          ),
          backgroundColor: Colors.transparent,
          onTap: () {
            if (keyFullName.currentState?.isValid ?? false) {
              BlocProvider.of<UpdateUserInfoCubit>(context).updateUserInfo({
                "fullName": keyFullName.currentState?.value,
                "email": keyEmail.currentState?.value,
                "mobilePhone": keyPhone.currentState?.value,
                "address": keyAddress.currentState?.value,
                "cccd": keyCccd.currentState?.value,
                "provideDate": keyProvideDate.currentState?.value,
                "providePlace": keyProvidePlace.currentState?.value,
              });
            }
          },
        )
      ],
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: [
              BlocListener<UpdateUserInfoCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState) {
                    BlocProvider.of<UserInfoCubit>(context).getUserInfo();
                    CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            showIconClose: false,
                            barrierDismissible: false,
                            content: "Chỉnh sửa thông tin thành công !",
                            titleOK: "Đóng",
                            onTapOK: () {})
                        .then((value) {
                      Navigator.pop(context);
                    });
                  }
                },
                child: Container(),
              ),
              renderItemInfo(
                title: "Tên tài khoản",
                value: userModel?.userName,
                enable: false,
              ),
              renderItemInfo(
                title: "Họ và tên",
                value: userModel?.fullName,
                enable: true,
                validator: (String value) {
                  if (value.trim().isEmpty) {
                    return "Vui lòng nhập họ tên";
                  }
                  return "";
                },
                key: keyFullName,
              ),
              renderItemInfo(
                title: "Email",
                value: userModel?.email,
                enable: true,
                key: keyEmail,
              ),
              renderItemInfo(
                title: "Số điện thoại",
                value: userModel?.mobilePhone,
                key: keyPhone,
                enable: true,
              ),
              renderItemInfo(
                title: "Địa chỉ ",
                value: userModel?.address,
                key: keyAddress,
              ),
              renderItemInfo(
                title: "CCCD",
                value: userModel?.cccd,
                key: keyCccd,
              ),
              renderItemInfo(
                title: "Ngày cấp",
                value: userModel?.provideDateFormated,
                isDateField: true,
                key: keyProvideDate,
              ),
              renderItemInfo(
                title: "Nơi cấp",
                value: userModel?.providePlace,
                key: keyProvidePlace,
              ),
            ],
          ),
        ),
      ),
    );
  }

  renderItemInfo(
      {required String title,
      value,
      bool enable = true,
      bool isDateField = false,
      key,
      CustomTextFieldValidator? validator}) {
    return CustomTextInput(
      margin: EdgeInsets.symmetric(vertical: 5),
      enabled: enable,
      key: key,
      isDateTimeTF: isDateField,
      initData: value,
      textController: TextEditingController(),
      hideUnderline: true,
      hintText: "",
      validator: validator,
      title: title,
    );
  }
}
