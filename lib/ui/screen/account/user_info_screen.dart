import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserInfoScreen extends StatelessWidget {
  const UserInfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<CloseAccountCubit>(
        create: (_) => CloseAccountCubit(),
      )
    ], child: UserInfoBody());
  }
}

class UserInfoBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<CloseAccountCubit>(),
      loadingWidget: CustomLoading<CloseAccountCubit>(),
      title: "Thông tin cá nhân",
      rightWidgets: [
        BaseButton(
          child: CustomTextLabel(
            "Sửa",
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: AppColors.ff404A69,
          ),
          backgroundColor: Colors.transparent,
          onTap: () {
            Navigator.pushNamed(context, Routes.editUserInfoScreen);
          },
        )
      ],
      body: SingleChildScrollView(
        child: BlocBuilder<UserInfoCubit, BaseState>(
          builder: (_, state) {
            UserModel? userModel = UserInfoBuilderWidget.getUserInfo(context);
            if (state is LoadedState<UserModel>) {
              userModel = state.data;
            }
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  CommonWidget.renderItemInfoWidget(
                    title: "Tên tài khoản",
                    value: userModel?.userName,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Họ tên",
                    value: userModel?.fullName,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Email",
                    value: userModel?.email,
                  ),
                  // TODO @nambuidanh
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Số điện thoại",
                    value: userModel?.mobilePhone,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Địa chỉ",
                    value: userModel?.address,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "CCCD",
                    value: userModel?.cccd,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Ngày cấp",
                    value: userModel?.provideDateFormated,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Nơi cấp",
                    value: userModel?.providePlace,
                  ),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  CommonWidget.renderItemInfoWidget(
                    title: "Ngân hàng",
                    value:"",
                  ),
                  CommonWidget.renderActionBankWidget(
                      BankName: userModel?.bankName ?? "",
                      AccountNumber: userModel?.accountName ?? "",
                      AccountName: userModel?.accountNumber ?? "",
                      isDefault: true,
                      icon: AppImages.ic_account_private,
                      onTap: () {
                        Navigator.pushNamed(context, Routes.listBankOfUserScreen);
                      }),
                  CommonWidget.baseLineWidget(color: AppColors.ffF3F3F3),
                  SizedBox(
                    height: 20,
                  ),
                  BaseButton(
                    onTap: () {
                      CustomDialog.showDialogConfirm(context,
                          content:
                              "Tài khoản của bạn sẽ bị khóa vĩnh viễn. Bạn có chắc chắn muốn đóng tài khoản này?",
                          titleOK: "Đồng ý", onTapOK: () {
                        BlocProvider.of<CloseAccountCubit>(context)
                            .closeAccount(context);
                      });
                    },
                    width: double.infinity,
                    titleColor: AppColors.ff404A69,
                    backgroundColor: AppColors.ffDBF2D7,
                    title: "Đóng tài khoản",
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
