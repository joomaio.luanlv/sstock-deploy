import 'dart:developer';

import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

String? selectedValue = "-2";
var now = DateTime.now();
DateFormat formatter = DateFormat('dd/MM/yyyy');
var endDate = formatter.format(new DateTime(now.year, now.month, now.day));
var firstDate = formatter.format(new DateTime(now.year, now.month, 1));
TextEditingController txtStart = TextEditingController();
TextEditingController txtEnd = TextEditingController();

class MainInvestScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<ListHistoryProductCubit>(
        create: (_) => ListHistoryProductCubit()
          ..getListHistoryProduct(
              {"uid": UserInfoBuilderWidget.getUserInfo(context)?.id}),
      ),
      BlocProvider<ListAssetCubit>(
        create: (_) => ListAssetCubit()..getListAsset(),
      ),
      BlocProvider<ListInvestingCubit>(
        create: (_) => ListInvestingCubit()..getInvesting(),
      ),
      BlocProvider<ListStatusCubit>(
        create: (_) => ListStatusCubit()..getListStatus(),
      ),
      BlocProvider<InvestPerfomanceCubit>(
        create: (_) => InvestPerfomanceCubit()
          ..getInvestPerfomance({
            "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
            "dayProfit": 7
          }),
      ),
    ], child: MainInvestBody());
  }
}

class MainInvestBody extends StatelessWidget {
  StateSetter? setStateIndex;
  StateSetter? setStateSelectedValue;
  int indexStack = 0;
  final RefreshController controller = RefreshController();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "TÀI SẢN",
      onBackPress: () {
        BlocProvider.of<ListHistoryProductCubit>(context)
            .getListHistoryProduct({
          "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
          "status": 2
        });
      },
      loadingWidget: CustomLoading<ListHistoryProductCubit>(),
      body: SmartRefresher(
        header: WaterDropMaterialHeader(),
        enablePullUp: false,
        enablePullDown: true,
        onRefresh: () {
          controller.refreshCompleted();
          BlocProvider.of<ListHistoryProductCubit>(context)
              .getListHistoryProduct({
            "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
            "startDate": txtStart.text,
            "endDate": txtEnd.text,
            "status": selectedValue
          });

          BlocProvider<InvestPerfomanceCubit>(
            create: (_) => InvestPerfomanceCubit()
              ..getInvestPerfomance({
                "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                "dayProfit": 7
              }),
          );

          BlocProvider.of<ListStatusCubit>(context).getListStatus();
        },
        controller: controller,
        child: SingleChildScrollView(
          child: Column(
            children: [
              renderHeader(),
              SizedBox(height: 10),
              renderBody(),
            ],
          ),
        ),
      ),
    );
  }

  renderHeader() {
    return BlocBuilder<ListAssetCubit, BaseState>(builder: (_, state) {
      if (state is LoadedState<List<AssetModel>?> && state.data!.length > 0) {
        var list = state.data ?? [];
        return Container(
          decoration: BoxDecoration(
            color: AppColors.ffDBF2D7,
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              for (var item in list)
                Row(
                  children: [
                    CustomTextLabel(
                      item.stockCode,
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: AppColors.ff222222,
                    ),
                    SizedBox(height: 30),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: CustomTextLabel(
                          item.totalFormat,
                          formatCurrency: false,
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.ff222222,
                        ),
                      ),
                    ),
                  ],
                ),
            ],
          ),
        );
      }
      return Container(
          decoration: BoxDecoration(
            color: AppColors.ffDBF2D7,
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CustomTextLabel(
                    "Tiền mặt:",
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(height: 30),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        0,
                        formatCurrency: true,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: AppColors.ff222222,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ));
    });
  }

  renderBody() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        this.setStateIndex = setState;
        return Column(
          children: [
            // Row(
            //   children: [
            //     SizedBox(width: 10),
            //     renderLabel(index: 0, title: "Đang đầu tư"),
            //     SizedBox(width: 10),
            //     renderLabel(index: 1, title: "Lịch sử"),
            //   ],
            // ),
            SizedBox(height: 5),
            IndexedStack(
              index: indexStack,
              children: [
                BlocProvider<InvestPerfomanceCubit>(
                  create: (_) => InvestPerfomanceCubit()
                    ..getInvestPerfomance({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                      "dayProfit": 7
                    }),
                  child: InvestScreen(),
                ),
                HistoryInvestScreen()
              ],
            )
          ],
        );
      },
    );
  }

  renderLabel({required int index, required String title}) {
    bool isSelect = index == indexStack;
    return BaseButton(
      onTap: () {
        setStateIndex?.call(() {
          this.indexStack = index;
        });
      },
      borderRadius: 10,
      backgroundColor: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Container(
        child: IntrinsicWidth(
            child: Column(
          children: [
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: isSelect ? AppColors.base_color : AppColors.ff585858,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
              height: 2,
              color: isSelect ? AppColors.base_color : Colors.transparent,
            )
          ],
        )),
      ),
    );
  }
}

class InvestScreen extends StatefulWidget {
  const InvestScreen({Key? key}) : super(key: key);

  @override
  State<InvestScreen> createState() => _InvestScreenState();
}

class _InvestScreenState extends State<InvestScreen> {
  @override
  Widget build(BuildContext context) {
    return _PointsLineChart();
  }
}

class _PointsLineChart extends StatelessWidget {
  final bool? animate;
  StateSetter? setStateIndex;
  int index = 0;

  _PointsLineChart({this.animate = true});

  var axis = charts.NumericAxisSpec(
      renderSpec: charts.GridlineRendererSpec(
    labelStyle: charts.TextStyleSpec(
        fontSize: 15,
        fontFamily: "Verdana",
        color: charts.MaterialPalette.black),
  ));

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            Column(
              children: [
                renderListProductInvest(),
                SizedBox(height: 20),
                CustomTextLabel(
                  "Biểu đồ tăng trưởng tài sản".toUpperCase(),
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                  color: AppColors.ff222222,
                ),
                SizedBox(height: 20),
                renderChart(),
                SizedBox(height: 50),
                // renderInfoLine(),
                // SizedBox(height: 50),
              ],
            ),
          ],
        ),
        // Positioned.fill(child: CustomLoading<InvestPerfomanceCubit>()),
        CustomSnackBar<InvestPerfomanceCubit>()
      ],
    );
  }

  renderListProductInvest() {
    return BlocBuilder<ListInvestingCubit, BaseState>(builder: (_, state) {
      if (state is LoadedState<List<InvestingModel>?> &&
          state.data!.length > 0) {
        var listData = state.data;
        return Column(
            children: listData!.map((e) {
          return renderItemProductInvest(e);
        }).toList());
      }
      return Column();
    });
  }

  Widget renderItemProductInvest(InvestingModel investingModel) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: AppColors.ffF3F3F3, shape: BoxShape.circle),
                child: Container(
                  decoration: BoxDecoration(
                      color: getColorByKeyName(investingModel.colorCode),
                      shape: BoxShape.circle),
                  width: 10,
                  height: 10,
                ),
              ),
              Expanded(
                  child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                child: CustomTextLabel(
                  investingModel.productName,
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: getColorByKeyName(investingModel.colorCode),
                ),
              )),
              CustomTextLabel(
                investingModel.total,
                formatCurrency: true,
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: AppColors.ff222222,
              ),
            ],
          ),
        ),
        CommonWidget.baseLineWidget()
      ],
    );
  }

  List<TimeSeriesSales> getListDataChart(List<DataChartTime>? items) {
    List<TimeSeriesSales> dataProduct = [];
    items?.forEach((element) {
      dataProduct.add(
          TimeSeriesSales(element.time ?? DateTime.now(), element.value ?? 0));
    });
    return dataProduct;
  }

  List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData(
      List<ChartInfoModel>? list) {
    List<charts.Series<TimeSeriesSales, DateTime>> listChart = [];

    list?.forEach((element) {
      charts.Series<TimeSeriesSales, DateTime> chartProduct =
          new charts.Series<TimeSeriesSales, DateTime>(
        id: element.key ?? "",
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(
            getColorByKeyName(element.colorCode)),
        domainFn: (TimeSeriesSales sales, _) {
          return sales.time;
        },
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        displayName: element.key,
        data: getListDataChart(element.items),
      );
      listChart.add(chartProduct);
    });
    return listChart;
    // final myDesktopData = [
    //   new TimeSeriesSales(new DateTime(2024, 8, 19), 5),
    //   new TimeSeriesSales(new DateTime(2024, 9, 26), 25),
    // ];

    // final myTabletData = [
    //   new TimeSeriesSales(new DateTime(2024, 8, 19), 10),
    //   new TimeSeriesSales(new DateTime(2024, 9, 26), 50),
    // ];
    // return [
    //   new charts.Series<TimeSeriesSales, DateTime>(
    //     id: 'Desktop',
    //     colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
    //     domainFn: (TimeSeriesSales sales, _) => sales.time,
    //     measureFn: (TimeSeriesSales sales, _) => sales.sales,
    //     data: myDesktopData,
    //   ),
    //   new charts.Series<TimeSeriesSales, DateTime>(
    //     id: 'Tablet',
    //     colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
    //     domainFn: (TimeSeriesSales sales, _) => sales.time,
    //     measureFn: (TimeSeriesSales sales, _) => sales.sales,
    //     data: myTabletData,
    //   ),
    // ];
  }

  renderChart() {
    return BlocBuilder<InvestPerfomanceCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<ChartInfoModel>>) {
          var seriesList = _createSampleData(state.data);
          return Container(
              width: 500,
              child: Column(
                children: [
                  Container(
                      width: 500,
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      height: 400,
                      child: new charts.TimeSeriesChart(seriesList,
                          primaryMeasureAxis: axis,
                          animate: animate,
                          dateTimeFactory: const charts.LocalDateTimeFactory(),
                          defaultRenderer: new charts.LineRendererConfig(
                              includeLine: true,
                              includeArea: true,
                              stacked: false,
                              roundEndCaps: true,
                              includePoints: true))),
                  renderInfoLine(state.data),
                ],
              ));
        }
        return Container();
      },
    );
  }

  renderButtonTime(String profit) {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        this.setStateIndex = setState;
        return Row(
          children: [
            BaseButton(
              onTap: () {
                BlocProvider.of<InvestPerfomanceCubit>(context)
                    .getInvestPerfomance({
                  "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                  "dayProfit": 7
                });

                setStateIndex?.call(() {
                  this.index = 0;
                });
              },
              borderRadius: 10,
              backgroundColor: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: IntrinsicWidth(
                    child: Column(
                  children: [
                    CustomTextLabel(
                      "7D",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: index == 0
                          ? AppColors.base_color
                          : AppColors.ff585858,
                    ),
                    index == 0
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 5),
                            height: 2,
                            color: AppColors.base_color,
                          )
                        : SizedBox(),
                    index == 0
                        ? CustomTextLabel(
                            profit,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          )
                        : SizedBox()
                  ],
                )),
              ),
            ),
            BaseButton(
              onTap: () {
                BlocProvider.of<InvestPerfomanceCubit>(context)
                    .getInvestPerfomance({
                  "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                  "dayProfit": 30
                });

                setStateIndex?.call(() {
                  this.index = 1;
                });
              },
              borderRadius: 10,
              backgroundColor: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: IntrinsicWidth(
                    child: Column(
                  children: [
                    CustomTextLabel(
                      "1M",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: index == 1
                          ? AppColors.base_color
                          : AppColors.ff585858,
                    ),
                    index == 1
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 5),
                            height: 2,
                            color: AppColors.base_color,
                          )
                        : SizedBox(),
                    index == 1
                        ? CustomTextLabel(
                            profit,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          )
                        : SizedBox()
                  ],
                )),
              ),
            ),
            BaseButton(
              onTap: () {
                BlocProvider.of<InvestPerfomanceCubit>(context)
                    .getInvestPerfomance({
                  "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                  "dayProfit": 90
                });

                setStateIndex?.call(() {
                  this.index = 2;
                });
              },
              borderRadius: 10,
              backgroundColor: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: IntrinsicWidth(
                    child: Column(
                  children: [
                    CustomTextLabel(
                      "3M",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: index == 2
                          ? AppColors.base_color
                          : AppColors.ff585858,
                    ),
                    index == 2
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 5),
                            height: 2,
                            color: AppColors.base_color,
                          )
                        : SizedBox(),
                    index == 2
                        ? CustomTextLabel(
                            profit,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          )
                        : SizedBox()
                  ],
                )),
              ),
            ),
            BaseButton(
              onTap: () {
                BlocProvider.of<InvestPerfomanceCubit>(context)
                    .getInvestPerfomance({
                  "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                  "dayProfit": 180
                });

                setStateIndex?.call(() {
                  this.index = 3;
                });
              },
              borderRadius: 10,
              backgroundColor: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: IntrinsicWidth(
                    child: Column(
                  children: [
                    CustomTextLabel(
                      "6M",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: index == 3
                          ? AppColors.base_color
                          : AppColors.ff585858,
                    ),
                    index == 3
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 5),
                            height: 2,
                            color: AppColors.base_color,
                          )
                        : SizedBox(),
                    index == 3
                        ? CustomTextLabel(
                            profit,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          )
                        : SizedBox()
                  ],
                )),
              ),
            ),
            BaseButton(
              onTap: () {
                BlocProvider.of<InvestPerfomanceCubit>(context)
                    .getInvestPerfomance({
                  "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                  "dayProfit": 365
                });

                setStateIndex?.call(() {
                  this.index = 4;
                });
              },
              borderRadius: 10,
              backgroundColor: Colors.transparent,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Container(
                child: IntrinsicWidth(
                    child: Column(
                  children: [
                    CustomTextLabel(
                      "1Y",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: index == 4
                          ? AppColors.base_color
                          : AppColors.ff585858,
                    ),
                    index == 4
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 5),
                            height: 2,
                            color: AppColors.base_color,
                          )
                        : SizedBox(),
                    index == 4
                        ? CustomTextLabel(
                            profit,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          )
                        : SizedBox()
                  ],
                )),
              ),
            ),
          ],
        );
      },
    );
  }

  renderInfoLine(List<ChartInfoModel>? data) {
    return Container(
      margin: EdgeInsets.only(right: 15, top: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: data?.map((elemnt) {
                return renderItem(
                    title: elemnt.key,
                    color: getColorByKeyName(elemnt.colorCode));
              }).toList() ??
              [],
        ),
      ),
    );
  }

  Widget renderItem({String? title, Color? color}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
      child: Row(
        children: [
          Container(
            height: 2,
            color: color,
            width: 30,
          ),
          SizedBox(width: 4),
          CustomTextLabel(
            title,
            fontWeight: FontWeight.w400,
            fontSize: 13,
            color: AppColors.ff585858,
          ),
        ],
      ),
    );
  }

  Color getColorByKeyName(String? color) {
    return AppColors.getColorFromHex(color);
  }
}

/// Sample linear data type.
class LinearSales {
  final int? period;
  final double? value;

  LinearSales(this.period, this.value);
}

class HistoryInvestScreen extends StatelessWidget {
  GlobalKey<TextFieldState> keyEndDate = GlobalKey();
  GlobalKey<TextFieldState> keyStartDate = GlobalKey();

  var setStateEndDate;
  var now = DateTime.now();
  DateFormat formatter = DateFormat('dd/MM/yyyy');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
              color: AppColors.ffDBF2D7,
            ),
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              children: [
                Column(
                  children: [
                    Container(
                      width: 150,
                      child: CustomTextLabel(
                        "Từ ngày:",
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: AppColors.ff63C856,
                      ),
                    ),
                    CustomTextInput(
                      key: keyStartDate,
                      colorBgTextField: AppColors.ffDBF2D7,
                      enableBorder: false,
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      isDateTimeTF: true,
                      textController: txtStart,
                      hideUnderline: true,
                      hintText: "",
                      maxLength: 18,
                      width: 150,
                      heightTextInput: 25,
                      keyboardType: TextInputType.text,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Vui lòng chọn ngày bắt đầu";
                        }
                        try {
                          int.parse(value.replaceAll(",", ""));
                        } catch (e) {
                          return "Ngày nhập không đúng định dạng";
                        }
                        return "";
                      },
                      initData: firstDate,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: 150,
                      child: CustomTextLabel(
                        "Đến ngày:",
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: AppColors.ff63C856,
                      ),
                    ),
                    new CustomTextInput(
                      key: keyEndDate,
                      colorBgTextField: AppColors.ffDBF2D7,
                      enableBorder: false,
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      isDateTimeTF: true,
                      textController: txtEnd,
                      hideUnderline: true,
                      hintText: "",
                      maxLength: 18,
                      width: 150,
                      heightTextInput: 25,
                      keyboardType: TextInputType.text,
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return "Vui lòng chọn ngày kết thúc";
                        }
                        try {
                          int.parse(value.replaceAll(",", ""));
                        } catch (e) {
                          return "Ngày nhập không đúng định dạng";
                        }
                        return "";
                      },
                      initData: endDate,
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                        child: IconButton(
                      iconSize: 30,
                      icon: Icon(Icons.search_rounded, color: Colors.grey),
                      onPressed: () {
                        BlocProvider.of<ListHistoryProductCubit>(context)
                            .getListHistoryProduct({
                          "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                          "startDate": txtStart.text,
                          "endDate": txtEnd.text,
                          "status": selectedValue
                        });
                      },
                    ))
                  ],
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
            )),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
          child: Row(
            children: [
              Container(
                child: CustomTextLabel(
                  "Trạng thái:",
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                  color: AppColors.ff63C856,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              renderStattus(),
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        renderListHistory(),
      ],
    );
  }

  renderStattus() {
    return StatefulBuilder(builder:
        (BuildContext context, void Function(void Function()) setState) {
      return BlocBuilder<ListStatusCubit, BaseState>(
        builder: (_, state) {
          if (state is LoadedState<List<StatusModel>>) {
            var list = [new StatusModel(status: -2, statusText: "Tất cả")];
            list = state.data != null
                ? state.data
                : [new StatusModel(status: -2, statusText: "Tất cả")];
            return Container(
              width: 180,
              height: 30,
              child: DropdownButtonHideUnderline(
                child: DropdownButton2(
                  itemPadding: EdgeInsets.only(left: 5),
                  buttonPadding: EdgeInsets.only(left: 5),
                  buttonDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: AppColors.ffDBF2D7,
                  ),
                  hint: Text(
                    'Trạng thái',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  items: list
                      .map((item) => DropdownMenuItem<String>(
                            value: item.status.toString(),
                            child: Text(
                              item.statusText ?? "",
                              style: const TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ))
                      .toList(),
                  value: selectedValue,
                  onChanged: (value) {
                    setState(() {
                      selectedValue = value as String;
                      setStateEndDate?.call(() {});
                    });
                    log(value as String);
                    BlocProvider.of<ListHistoryProductCubit>(context)
                        .getListHistoryProduct({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
                      "status": selectedValue.toString(),
                      "startDate": txtStart.text,
                      "endDate": txtEnd.text
                    });
                  },
                  buttonHeight: 40,
                  buttonWidth: 140,
                  itemHeight: 40,
                ),
              ),
            );
          }
          return SizedBox.shrink();
        },
      );
    });
  }

  Color getColorByKeyName(String? color) {
    return AppColors.getColorFromHex(color);
  }

  Widget renderLabel(HistoryModel? historyModel) {
    var data = historyModel?.getValueState();
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              color: getColorByKeyName(historyModel?.statusColor),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          child: CustomTextLabel(
            historyModel?.statusText,
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  Widget renderItemHistory(BuildContext context, HistoryModel? historyModel) {
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.detailHistoryScreen,
              arguments: historyModel);
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.ff00A95E, width: 1),
              color: AppColors.white,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: CustomTextLabel(
                      "${historyModel?.startDate} - ${historyModel?.endDate}",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: AppColors.ff404A69,
                    ),
                  ),
                  renderLabel(historyModel)
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 2,
                    child: CustomTextLabel(
                      historyModel?.productName,
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: AppColors.ff585858,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        "${historyModel?.originValue}",
                        formatCurrency: false,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: AppColors.ff585858,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              IntrinsicHeight(
                child: Row(
                  children: [
                    renderInfo(
                        title: "Lãi suất",
                        value: "${historyModel?.iRate ?? ""}%",
                        alignment: Alignment.centerLeft),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                      width: 1,
                      color: AppColors.ffF3F3F3,
                    ),
                    renderInfo(
                        title:
                            historyModel?.actionType == 0 ? "Lợi nhuận" : "Lãi",
                        value: historyModel?.profit,
                        formatCurrency: false),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                      width: 1,
                      color: AppColors.ffF3F3F3,
                    ),
                    renderInfo(
                        title: historyModel?.actionType == 0
                            ? "Tổng nhận"
                            : "Tổng nộp",
                        value: "${(historyModel?.total)}",
                        alignment: Alignment.centerRight,
                        fontSizeValue: 16,
                        fontWeightValue: FontWeight.w600,
                        colorValue: AppColors.base_color,
                        formatCurrency: false),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  renderListHistory() {
    return BlocBuilder<ListHistoryProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<HistoryModel>>) {
          var list = state.data;
          if (list.length > 0) {
            return ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return renderItemHistory(context, list[index]);
              },
              itemCount: list.length,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 15,
                );
              },
            );
          } else {
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: CustomTextLabel(
                  "Không tìm thấy giao dịch nào phát sinh.",
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.base_color,
                  textAlign: TextAlign.center,
                ));
          }
        }

        return SizedBox.shrink();
      },
    );
  }

  renderInfo(
      {required String title,
      value,
      FontWeight? fontWeightValue,
      double? fontSizeValue,
      Color? colorValue,
      AlignmentGeometry? alignment,
      bool formatCurrency = false}) {
    return Expanded(
        child: Align(
      alignment: alignment ?? Alignment.center,
      child: Column(
        children: [
          CustomTextLabel(
            title,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            textAlign: TextAlign.center,
            color: AppColors.ff585858,
          ),
          SizedBox(height: 3),
          CustomTextLabel(
            value,
            fontWeight: fontWeightValue ?? FontWeight.w400,
            fontSize: fontSizeValue ?? 16,
            textAlign: TextAlign.center,
            formatCurrency: formatCurrency,
            color: colorValue ?? AppColors.ff222222,
          ),
        ],
      ),
    ));
  }
}
