import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/blocs/product/cancel_request_cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/api_constant.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DetailHistoryScreen extends StatelessWidget {
  final HistoryModel? historyModel;

  const DetailHistoryScreen({Key? key, required this.historyModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<CancelRequestCubit>(
            create: (_) => CancelRequestCubit(),
          ),
          BlocProvider<WithdrawProductCubit>(
            create: (_) => WithdrawProductCubit(),
          ),
          BlocProvider<SoldStockCubit>(
            create: (_) => SoldStockCubit(),
          ),
          BlocProvider<ListHistoryProductCubit>(
            create: (_) => ListHistoryProductCubit(),
          ),
          BlocProvider<DailyProfitCubit>(
            create: (_) => DailyProfitCubit()
              ..getListDailyProfit(
                  {"cidpro": historyModel?.id, "startTime": "01/01/1900"}),
          ),
        ],
        child: DetailHistoryBody(
          historyModel: historyModel,
        ));
  }
}

class DetailHistoryBody extends StatelessWidget {
  final HistoryModel? historyModel;
  final bool? animate;

  DetailHistoryBody({Key? key, this.historyModel, this.animate})
      : super(key: key) {}

  var axis = charts.NumericAxisSpec(
      renderSpec: charts.GridlineRendererSpec(
    labelStyle: charts.TextStyleSpec(
        fontSize: 15,
        fontFamily: "Verdana",
        color: charts.MaterialPalette.black),
  ));

  @override
  Widget build(BuildContext context) {
    var data = historyModel?.getValueState();
    return BaseScreen(
      messageNotify: CustomSnackBar<CancelRequestCubit>(),
      loadingWidget: CustomLoading<CancelRequestCubit>(),
      onBackPress: () {
        //log("message"); // do something
      },
      title: historyModel?.actionType == 0
          ? "Thông tin gói đầu tư"
          : "Thông tin gói vay",
      body: SingleChildScrollView(
          child: Container(
        // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: CustomTextLabel(
                          "${historyModel?.startDate} - ${historyModel?.endDate}",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: AppColors.ff585858,
                        ),
                      ),
                      Container(
                        child: renderLabel(historyModel),
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: CustomTextLabel(
                          historyModel?.productName,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: AppColors.ff585858,
                        ),
                      ),
                      Container(
                        child: CustomTextLabel(
                          historyModel?.originValue,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: AppColors.ff585858,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: CustomTextLabel(
                          "Lãi suất: ",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: AppColors.ff585858,
                        ),
                      ),
                      Container(
                        child: CustomTextLabel(
                          "${historyModel?.iRate ?? ""}%",
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: AppColors.ff585858,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: CustomTextLabel(
                          "Lợi nhuận: ",
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: AppColors.ff585858,
                        ),
                      ),
                      Container(
                        child: CustomTextLabel(
                          "+ ${historyModel?.profit}",
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                          color: AppColors.ff585858,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            renderChart(),
            SizedBox(height: 20),
            CommonWidget.baseLineWidget(
                color: AppColors.ffF3F3F3, margin: EdgeInsets.only(bottom: 20)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  historyModel?.isCancel == true
                      ? BaseButton(
                          onTap: () {
                            Navigator.pushNamed(context, Routes.mainScreen);
                          },
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(vertical: 5),
                          title: "Đầu tư mới",
                        )
                      : Container(),
                  BaseButton(
                      onTap: () {
                        Navigator.pushNamed(
                            context, Routes.dailyProfitScreen,
                            arguments: historyModel?.id);
                      },
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      title: "Xem chi tiết lãi",
                    ),
                  BaseButton(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.pdfViewWidget,
                          arguments: PDFViewWidgetArg(
                              initialUrl: ApiConstant.getImageHost(
                                  historyModel?.contractLink),
                              title: "Hợp đồng"));
                    },
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(vertical: 5),
                    title: "Chi tiết hợp đồng",
                  ),
                  // Container(
                  //   child: historyModel?.withdraw == true
                  //       ? Column(
                  //           children: [
                  //             BaseButton(
                  //               onTap: () {
                  //                 CustomDialog.showDialogConfirm(context,
                  //                     showCancel: true,
                  //                     showIconClose: false,
                  //                     barrierDismissible: false,
                  //                     content:
                  //                         "Rút sớm sẽ không được nhận 100% lợi nhuận.Bạn có chắc muốn rút?",
                  //                     titleOK: "Đồng ý", onTapOK: () {
                  //                   BlocProvider.of<CancelRequestCubit>(context)
                  //                       .withdraw({
                  //                     "cid": UserInfoBuilderWidget.getUserInfo(
                  //                             context)
                  //                         ?.id,
                  //                     "cproid": historyModel?.id,
                  //                     "description": "Yêu cầu rút",
                  //                   });
                  //                 });
                  //               },
                  //               backgroundColor: Colors.red,
                  //               width: double.infinity,
                  //               margin: EdgeInsets.symmetric(vertical: 5),
                  //               title: "Đặt lệnh rút",
                  //             ),
                  //             BlocListener<CancelRequestCubit, BaseState>(
                  //               listener: (_, state) {
                  //                 if (state is LoadedState<int> &&
                  //                     state.data == 3) {
                  //                   CustomDialog.showDialogConfirm(context,
                  //                           showCancel: false,
                  //                           showIconClose: false,
                  //                           barrierDismissible: false,
                  //                           content:
                  //                               "Gửi yêu cầu rút thành công",
                  //                           titleOK: "Đóng",
                  //                           onTapOK: () {})
                  //                       .then((value) {
                  //                     Navigator.pushNamed(
                  //                       context,
                  //                       Routes.mainScreen,
                  //                     );
                  //                   });
                  //                 }
                  //               },
                  //               child: Container(),
                  //             )
                  //           ],
                  //         )
                  //       : SizedBox(width: 0),
                  // ),
                  // Container(
                  //   child: historyModel?.sold == true
                  //       ? Column(
                  //           children: [
                  //             BaseButton(
                  //               onTap: () {
                  //                 CustomDialog.showDialogConfirm(context,
                  //                     showCancel: true,
                  //                     showIconClose: false,
                  //                     barrierDismissible: false,
                  //                     content:
                  //                         "Bạn có chắc muốn gửi yêu cầu bán?",
                  //                     titleOK: "Đồng ý", onTapOK: () {
                  //                   BlocProvider.of<CancelRequestCubit>(context)
                  //                       .soldStock({
                  //                     "cid": UserInfoBuilderWidget.getUserInfo(
                  //                             context)
                  //                         ?.id,
                  //                     "cidpro": historyModel?.id,
                  //                   });
                  //                 });
                  //               },
                  //               backgroundColor: Colors.red,
                  //               width: double.infinity,
                  //               margin: EdgeInsets.symmetric(vertical: 5),
                  //               title: "Đặt lệnh bán chứng khoán",
                  //             ),
                  //             BlocListener<CancelRequestCubit, BaseState>(
                  //               listener: (_, state) {
                  //                 if (state is LoadedState<int> &&
                  //                     state.data == 2) {
                  //                   CustomDialog.showDialogConfirm(context,
                  //                           showCancel: false,
                  //                           showIconClose: false,
                  //                           barrierDismissible: false,
                  //                           content:
                  //                               "Gửi yêu cầu bán thành công",
                  //                           titleOK: "Đóng",
                  //                           onTapOK: () {})
                  //                       .then((value) {
                  //                     Navigator.pushNamed(
                  //                       context,
                  //                       Routes.mainScreen,
                  //                     );
                  //                   });
                  //                 }
                  //               },
                  //               child: Container(),
                  //             )
                  //           ],
                  //         )
                  //       : SizedBox(width: 0),
                  // ),
                  // historyModel?.statusFull == 0
                  //     ? Column(
                  //         children: [
                  //           BaseButton(
                  //             onTap: () {
                  //               CustomDialog.showDialogConfirm(context,
                  //                   showCancel: true,
                  //                   showIconClose: false,
                  //                   barrierDismissible: false,
                  //                   content: "Bạn có chắc muốn hủy yêu cầu?",
                  //                   titleOK: "Đồng ý", onTapOK: () {
                  //                 BlocProvider.of<CancelRequestCubit>(context)
                  //                     .cancelRequest({
                  //                   "cproid": historyModel?.id,
                  //                 });
                  //               });
                  //             },
                  //             backgroundColor: Colors.red,
                  //             width: double.infinity,
                  //             margin: EdgeInsets.symmetric(vertical: 5),
                  //             title: "Hủy yêu cầu",
                  //           ),
                  //           BlocListener<CancelRequestCubit, BaseState>(
                  //             listener: (_, state) {
                  //               if (state is LoadedState<int> &&
                  //                   state.data == 1) {
                  //                 CustomDialog.showDialogConfirm(context,
                  //                         showCancel: false,
                  //                         showIconClose: false,
                  //                         barrierDismissible: false,
                  //                         content: "Hủy yêu cầu thành công",
                  //                         titleOK: "Đóng",
                  //                         onTapOK: () {})
                  //                     .then((value) {
                  //                   Navigator.pushNamed(
                  //                     context,
                  //                     Routes.mainScreen,
                  //                   );
                  //                 });
                  //               }
                  //             },
                  //             child: Container(),
                  //           )
                  //         ],
                  //       )
                  //     : SizedBox(width: 0),
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }

  renderInfoLine(String productName) {
    return Container(
      margin: EdgeInsets.only(right: 15, top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          renderItem(title: "VNIndex", color: Colors.red),
          SizedBox(width: 20),
          renderItem(title: productName, color: Colors.blue),
        ],
      ),
    );
  }

  Widget renderItem({String? title, Color? color}) {
    return Row(
      children: [
        Container(
          height: 1.5,
          color: color,
          width: 40,
        ),
        SizedBox(width: 4),
        CustomTextLabel(
          title,
          fontWeight: FontWeight.w400,
          fontSize: 12,
          color: AppColors.ff585858,
        ),
      ],
    );
  }

  Widget renderChart() {
    return BlocBuilder<DailyProfitCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<DailyProfitModel>>) {
          if (state.data.length > 0) {
            var seriesList = _createSampleData(state.data);
            return Column(
              children: [
                CommonWidget.baseLineWidget(
                    color: AppColors.ffF3F3F3,
                    margin: EdgeInsets.only(bottom: 20)),
                SizedBox(height: 20),
                CustomTextLabel(
                  "Biểu đồ chi tiết hiệu quả đầu tư".toUpperCase(),
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                  color: AppColors.ff222222,
                ),
                SizedBox(height: 10),
                renderInfoLine(historyModel?.productName ?? ""),
                SizedBox(height: 10),
                Container(
                  height: 400,
                  margin: EdgeInsets.only(left: 15),
                  child: new charts.TimeSeriesChart(seriesList,
                      primaryMeasureAxis: axis,
                      animate: animate,
                      dateTimeFactory: const charts.LocalDateTimeFactory(),
                      defaultRenderer: new charts.LineRendererConfig(
                          includeLine: true,
                          includeArea: true,
                          stacked: true,
                          roundEndCaps: true,
                          includePoints: true)),
                ),
              ],
            );
          }
        }
        return Container();
      },
    );
  }

  static List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData(
      List<DailyProfitModel> list) {
    List<TimeSeriesSales> vnIndexData = list
        .where((element) => element.vnIndex == true)
        .map((model) => TimeSeriesSales(
            model.date ?? DateTime.now(), model.interestValue ?? 0))
        .toList();
    List<TimeSeriesSales> productData = list
        .where((element) => element.vnIndex != true)
        .map((model) => TimeSeriesSales(
            model.date ?? DateTime.now(), model.interestValue ?? 0))
        .toList();

    return [
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'VNIndexData',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: vnIndexData,
      ),
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Desktop',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: productData,
      ),
    ];
  }

  Color getColorByKeyName(String? color) {
    return AppColors.getColorFromHex(color);
  }

  Widget renderLabel(HistoryModel? historyModel) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              color: getColorByKeyName(historyModel?.statusColor),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          child: CustomTextLabel(
            historyModel?.statusText,
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class LinearSalesDetail {
  final DateTime day;
  final double? value;

  LinearSalesDetail(this.day, this.value);
}