import 'dart:developer';

import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

//Mã gói
int? cidpro;
var today = DateTime.now();
DateFormat formatterDaily = DateFormat('dd/MM/yyyy');
TextEditingController txtStartDaily = TextEditingController();
TextEditingController txtEndDaily = TextEditingController();

class DailyProfitScreen extends StatelessWidget {
  final int? packId;

  const DailyProfitScreen({Key? key, required this.packId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    cidpro = packId;
    return BlocProvider<DailyProfitCubit>(
        create: (_) =>
            DailyProfitCubit()..getListDailyProfit({"cidpro": packId}),
        child: DailyProfitBody());
  }
}

class DailyProfitBody extends StatelessWidget {
  GlobalKey<TextFieldState> keyEndDate = GlobalKey();
  GlobalKey<TextFieldState> keyStartDate = GlobalKey();
  final RefreshController controller = RefreshController();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Lịch sử tính lãi",
      loadingWidget: CustomLoading<DailyProfitCubit>(),
      hideAppBar: false,
      body: SmartRefresher(
        header: WaterDropMaterialHeader(),
        enablePullUp: false,
        enablePullDown: true,
        onRefresh: () {
          BlocProvider.of<DailyProfitCubit>(context).getListDailyProfit({
            "cidpro": cidpro,
            "startTime": txtStartDaily.text,
            "endTime": txtEndDaily.text
          });
          controller.refreshCompleted();
        },
        controller: controller,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              renderBody(),
            ],
          ),
        ),
      ),
    );
  }

  renderListProfit() {
    return BlocBuilder<DailyProfitCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<DailyProfitModel>>) {
          if (state.data.length > 0) {
            var data =
                state.data.where((element) => element.vnIndex != true).toList();
            if (data.isNotEmpty) data.removeAt(0);
            return ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return renderProfitItem(context, data[index]);
              },
              itemCount: data.length,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 15,
                );
              },
            );
          } else {
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: CustomTextLabel(
                  "Không tìm thấy ngày nào được tính lãi trong khoảng thời gian này.",
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.base_color,
                  textAlign: TextAlign.center,
                ));
          }
        }
        return SizedBox.shrink();
      },
    );
  }

  renderProfitItem(BuildContext context, DailyProfitModel? dailyProfitModel) {
    return Container(
      // height: 65,
      // padding: EdgeInsets.all(5),
      // decoration: BoxDecoration(
      //     color: AppColors.white,
      //     boxShadow: [
      //       BoxShadow(
      //         color: Colors.black26,
      //         blurRadius: 5.0,
      //         offset: const Offset(2, 2.0),
      //       ),
      //     ],
      //     borderRadius: BorderRadius.circular(10)),
      // child: Container(
      //   padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      //   child: Row(
      //     children: [
      //       Expanded(
      //         child: Column(
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: [
      //             Row(
      //               children: [
      //                 Row(
      //                   children: [
      //                     Icon(
      //                       Icons.access_time_outlined,
      //                       color: AppColors.ff999999,
      //                       size: 14,
      //                     ),
      //                     SizedBox(width: 5),
      //                     CustomTextLabel(dailyProfitModel?.interestDay,
      //                         maxLines: 2,
      //                         fontSize: 14,
      //                         fontWeight: FontWeight.w400,
      //                         color: AppColors.ff999999)
      //                   ],
      //                 ),
      //               ],
      //             ),
      //             SizedBox(height: 8),
      //             Container(
      //               height: 20,
      //               child: Row(
      //                 children: [
      //                   Container(
      //                     width: 70,
      //                     child: Expanded(
      //                         child: CustomTextLabel("Lãi ngày: ",
      //                             maxLines: 2,
      //                             fontSize: 16,
      //                             fontWeight: FontWeight.w400,
      //                             color: AppColors.ff999999)),
      //                   ),
      //                   Container(
      //                     child: Expanded(
      //                         child: CustomTextLabel(
      //                             dailyProfitModel?.interestNum,
      //                             maxLines: 2,
      //                             fontSize: 18,
      //                             formatCurrency: false,
      //                             fontWeight: FontWeight.w400,
      //                             color: AppColors.ff222222)),
      //                   ),
      //                 ],
      //               ),
      //             )
      //           ],
      //         ),
      //       ),
      //       SizedBox(width: 5),
      //     ],
      //   ),
      // ),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0,
              offset: const Offset(2, 2.0),
            ),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.access_time_outlined,
                                color: AppColors.ff999999,
                                size: 14,
                              ),
                              SizedBox(width: 5),
                              CustomTextLabel(dailyProfitModel?.interestDay,
                                  maxLines: 2,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff999999),
                              SizedBox(width: 3),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomTextLabel("Lãi:",
                              textAlign: TextAlign.left,
                              maxLines: 2,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: AppColors.ff999999),
                          SizedBox(height: 5),
                          Container(
                              child: CustomTextLabel(
                                  dailyProfitModel?.interestNum,
                                  maxLines: 2,
                                  fontSize: 18,
                                  formatCurrency: false,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.ff222222)),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }

  renderBody() {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        var endDate = formatterDaily
            .format(new DateTime(today.year, today.month, today.day));
        var firstDate =
            formatterDaily.format(new DateTime(today.year, today.month, 1));

        return Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: [
                  // Row(
                  //   children: [
                  //     CustomTextLabel(
                  //       "Chi tiết lãi gói đầu tư theo ngày.",
                  //       fontWeight: FontWeight.w400,
                  //       fontSize: 14,
                  //       fontStyle: FontStyle.italic,
                  //       color: AppColors.ffBDBDBD,
                  //       textAlign: TextAlign.left,
                  //     ),
                  //   ],
                  // ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Column(
                        children: [
                          Container(
                            width: 150,
                            child: CustomTextLabel(
                              "Từ ngày:",
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: AppColors.ff63C856,
                            ),
                          ),
                          new CustomTextInput(
                            key: keyStartDate,
                            enableBorder: false,
                            padding: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 0),
                            isDateTimeTF: true,
                            textController: txtStartDaily,
                            hideUnderline: true,
                            hintText: "",
                            maxLength: 18,
                            width: 150,
                            keyboardType: TextInputType.text,
                            validator: (String value) {
                              if (value.trim().isEmpty) {
                                return "Vui lòng chọn ngày bắt đầu";
                              }
                              try {
                                int.parse(value.replaceAll(",", ""));
                              } catch (e) {
                                return "Ngày nhập không đúng định dạng";
                              }
                              return "";
                            },
                            initData: firstDate,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            width: 150,
                            child: CustomTextLabel(
                              "Đến ngày:",
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: AppColors.ff63C856,
                            ),
                          ),
                          new CustomTextInput(
                            key: keyEndDate,
                            enableBorder: false,
                            padding: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 0),
                            isDateTimeTF: true,
                            textController: txtEndDaily,
                            hideUnderline: true,
                            hintText: "",
                            maxLength: 18,
                            width: 150,
                            keyboardType: TextInputType.text,
                            validator: (String value) {
                              if (value.trim().isEmpty) {
                                return "Vui lòng chọn ngày kết thúc";
                              }
                              try {
                                int.parse(value.replaceAll(",", ""));
                              } catch (e) {
                                return "Ngày nhập không đúng định dạng";
                              }
                              return "";
                            },
                            initData: endDate,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                              child: IconButton(
                            iconSize: 30,
                            icon:
                                Icon(Icons.search_rounded, color: Colors.grey),
                            onPressed: () {
                              BlocProvider.of<DailyProfitCubit>(context)
                                  .getListDailyProfit({
                                "cidpro": cidpro,
                                "startTime": txtStartDaily.text,
                                "endTime": txtEndDaily.text
                              });
                            },
                          ))
                        ],
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                  ),
                  CommonWidget.baseLineWidget(
                      margin: EdgeInsets.only(top: 10, bottom: 0)),
                  SizedBox(height: 20),
                  renderListProfit()
                ],
              ),
            ),
            SizedBox(height: 10),
          ],
        );
      },
    );
  }
}
