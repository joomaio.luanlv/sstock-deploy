import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/constants.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:app_sstock/utils/device_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListNotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _ListNotificationBody();
  }
}

class _ListNotificationBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ListNotificationBodyState();
  }
}

class _ListNotificationBodyState extends State<_ListNotificationBody> with SingleTickerProviderStateMixin {
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ListNotificationCubit>(context).getList(refresh: _refreshController, paramRequest: {});
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Thông báo",
      messageNotify: CustomSnackBar<ListNotificationCubit>(),
      loadingWidget: CustomLoading<ListNotificationCubit>(),
      body: Container(child: BlocBuilder<ListNotificationCubit, BaseState>(
        builder: (_, state) {
          if (state is LoadedState<List<NotificationModel>>) {
            if (state.data.isEmpty) {
              return Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: CustomTextLabel("Danh sách thông báo trống !"),
              );
            }
            return CustomSmartRefresher(
              refreshController: _refreshController,
              enablePullUp: false,
              enablePullDown: true,
              onRefresh: () {
                // paramFilter = null;
                BlocProvider.of<ListNotificationCubit>(context).getList(isRefresh: true);
              },
              onLoadMore: () {
                // BlocProvider.of<ListNotificationCubit>(context).getList(isLoadMore: true);
              },
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              itemBuilder: (_, index) {
                return renderItem(index, state.data[index]);
              },
              colorSeparator: Colors.transparent,
              heightSeparator: 10,
              listData: state.data,
            );
          }
          return Container();
        },
      )),
    );
  }

  Widget renderItem(int index, NotificationModel data) {
    return InkWell(
      onTap: () {
        BlocProvider.of<ListNotificationCubit>(context).doReadNotification(index, data);
        DeviceUtil.handleNavigation(context, data, fromListNotification: true, getArguments: (int tye) {
          // try {
          //   if (DeviceUtil.LIST_TYPE_NOTI_REPAIR.contains(data.event)) {
          //     int? id = int.parse(jsonDecode(data.data)["id"]?.toString() ?? "");
          //     return RequestRepairModel(id: id);
          //   }
          //   return null;
          // } catch (e) {}
          return null;
        });
      },
      child: Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: AppColors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 5.0,
                offset: const Offset(2, 2.0),
              ),
            ],
            // border: Border.all(width: 1, color: AppColors.border),
            borderRadius: BorderRadius.circular(10)),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            // Image.asset(
                            //   AppImages.ic_account_bank,
                            //   width: 35,
                            //   height: 35,
                            // ),
                            // SizedBox(width: 5),
                            Expanded(
                                child: CustomTextLabel(data.title,
                                    maxLines: 2, fontSize: 15, fontWeight: FontWeight.w500, color: AppColors.ff222222)),
                            SizedBox(width: 10),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Expanded(
                              child: CustomTextLabel(data.message,
                                  maxLines: 4, fontSize: 13, fontWeight: FontWeight.w400, color: AppColors.ff222222),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.access_time_outlined,
                                  color: AppColors.ff999999,
                                  size: 14,
                                ),
                                SizedBox(width: 5),
                                CustomTextLabel(Common.fromDate(data.createDateFormated, FormatDate.HH_mm_dd_mm_yy),
                                    maxLines: 2, fontSize: 14, fontWeight: FontWeight.w400, color: AppColors.ff999999),
                              ],
                            ),
                            // Expanded(
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.end,
                            //     children: [
                            //       CustomTextLabel("Xem chi tiết",
                            //           fontSize: 14, fontWeight: FontWeight.w400, color: AppColors.ffB2171F),
                            //       Icon(
                            //         Icons.arrow_forward_ios_outlined,
                            //         color: AppColors.ffB2171F,
                            //         size: 14,
                            //       ),
                            //     ],
                            //   ),
                            // )
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 5),
                ],
              ),
            ),
            if (data.isViewed == false)
              Positioned(
                top: 0,
                right: 0,
                child: Container(
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(color: AppColors.ffB2171F, shape: BoxShape.circle),
                  width: 8,
                  height: 8,
                ),
              )
          ],
        ),
      ),
    );
  }
}
