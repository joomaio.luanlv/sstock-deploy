import 'dart:developer';

import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ReferentScreen extends StatefulWidget {
  const ReferentScreen({Key? key}) : super(key: key);
  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<ReferentScreen> {
  String url = "";

  @override
  void initState() {
    super.initState();
  }

  void getDynammicLink(String ref) async {
    url = await buildDynamicLinks(ref);
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Mã giới thiệu",
      // onBackPress: () {
      //   BlocProvider.of<ListHistoryProductCubit>(context)
      //       .getListHistoryProduct({
      //     "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
      //     "status": 2
      //   });
      // },
      body: SingleChildScrollView(
        child: BlocBuilder<UserInfoCubit, BaseState>(
          builder: (_, state) {
            UserModel? userModel = UserInfoBuilderWidget.getUserInfo(context);
            getDynammicLink(userModel?.referralCode ?? "");
            if (state is LoadedState<UserModel>) {
              userModel = state.data;
            }
            return Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          'Mời bạn bè dùng SStock để nhận thưởng và các ưu đãi đặc biệt',
                          style: TextStyle(fontSize: 20, color: AppColors.base_color),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          'Mời tải ứng dụng và đăng ký tài khoản qua link sau:',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                        color: AppColors.ffF3F3F3,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: CustomTextLabel(
                            url.length > 0 ? url : "https://appsstock.page.link/3Pja",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                        Container(
                            child: InkWell(
                          onTap: () {
                            Clipboard.setData(
                                ClipboardData(text: url));
                            CustomDialog.showDialogConfirm(context,
                                showCancel: false,
                                showIconClose: false,
                                barrierDismissible: false,
                                content: "Copy thành công link tải phần mềm",
                                titleOK: "Đóng",
                                onTapOK: () {});
                          },
                          child: Icon(
                            Icons.content_copy,
                            color: AppColors.base_color,
                            size: 30,
                          ),
                        ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          'Hoặc chỉ cần tải app và nhập mã giới thiệu dưới đây:',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                        color: AppColors.ffF3F3F3,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: CustomTextLabel(
                            userModel?.referralCode,
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                        Container(
                            child: InkWell(
                          onTap: () {
                            Clipboard.setData(
                                ClipboardData(text: userModel?.referralCode ?? ""));
                            CustomDialog.showDialogConfirm(context,
                                showCancel: false,
                                showIconClose: false,
                                barrierDismissible: false,
                                content: "Copy thành công mã giới thiệu",
                                titleOK: "Đóng",
                                onTapOK: () {});
                          },
                          child: Icon(
                            Icons.content_copy,
                            color: AppColors.base_color,
                            size: 30,
                          ),
                        ))
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

buildDynamicLinks(String ref) async {
  String url =
      "https://apps.apple.com/us/app/sstock-chia-s%E1%BA%BB-ch%E1%BB%A9ng-kho%C3%A1n/id1643830294";
  final DynamicLinkParameters parameters = DynamicLinkParameters(
    uriPrefix: "https://appsstock.page.link",
    link: Uri.parse('$url?ref=$ref'),
    androidParameters: AndroidParameters(
      packageName: "com.app.sstock",
      minimumVersion: 30,
    ),
    iosParameters: IOSParameters(
      bundleId: "com.app.sstock",
      minimumVersion: '30',
    ),
    socialMetaTagParameters: SocialMetaTagParameters(
        description: '',
        imageUrl:
            Uri.parse("https://flutter.dev/images/flutter-logo-sharing.png")),
  );
  final ShortDynamicLink dynamicUrl =
      await FirebaseDynamicLinks.instance.buildShortLink(parameters);

  String? desc = dynamicUrl.shortUrl.toString();
  return desc;
}
