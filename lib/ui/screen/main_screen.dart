import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/screen/account/account_screen.dart';
import 'package:app_sstock/ui/screen/home_screen.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/device_util.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  static const int homeIndex = 0;
  static const int accountIndex = 1;
  static const int zaloIndex = 2;
  static const int messengerIndex = 3;

  int currentTab = 0;

  late List<Widget> screens;

  // final PageStorageBucket bucket = PageStorageBucket();

  late TabController _tabController;
  List<String> imageStrings = [
    AppImages.ic_tabbar_home,
    AppImages.ic_tabbar_user,
    AppImages.ic_zalo,
    AppImages.ic_messenger,
  ];
  List<String> titles = [
    "Đầu tư",
    "Tài khoản",
    "Zalo",
    "Messenger",
  ];

  _barItem(BuildContext context, int index) {
    return GestureDetector(
        child: Container(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  imageStrings[index],
                  color: index == currentTab
                      ? AppColors.base_color
                      : AppColors.ff585858,
                  width: index < 2 ? 24 : 20,
                  height: index < 2 ? 24 : 20,
                ),
              ),
              SizedBox(height: 10),
              CustomTextLabel(
                titles[index],
                textAlign: TextAlign.center,
                maxLines: 2,
                fontWeight: FontWeight.w500,
                fontSize: 12,
                color: index == currentTab
                    ? AppColors.base_color
                    : AppColors.ff585858,
              )
            ],
          ),
        ),
        onTap: () async {
          if (index <= 1) {
            setState(() {
              currentTab = index;
              _tabController.animateTo(currentTab);
            });
          } else if (index == 2) {
            openApp("https://zalo.me/0383371352");
          } else {
            openApp("https://m.me/CuThongThai.VNInvestor/");
          }
        });
  }

  Future<void> openApp(String url) async {
    try {
      Uri uri = Uri.parse(url);

      if (await canLaunchUrl(uri)) {
        await launchUrl(uri, mode: LaunchMode.externalApplication);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Ứng dụng chưa được cài đặt hoặc không tìm thấy ID người dùng'),
          ),
        );
      }
    } catch (e) {
    } finally {}
  }

  @override
  void initState() {
    initDynamicLinks();
    screens = [
      KeepAlivePage(child: HomeScreen()),
      KeepAlivePage(
        child: AccountScreen(),
      ),
    ];
    _tabController = TabController(vsync: this, length: screens.length);
    _tabController.addListener(() {
      setState(() {
        currentTab = _tabController.index;
      });
    });
    super.initState();
    BlocProvider.of<ListHistoryProductCubit>(context).getListHistoryProduct(
        {"uid": UserInfoBuilderWidget.getUserInfo(context)?.id});
    BlocProvider.of<TotalUnreadNotifyCubit>(context).getTotalUnread();
    DeviceUtil.listenerNotification(context);
    DeviceUtil.fcmSubscribeListTopic(context);
    NotificationRepository.regFirebase();
  }

  //Retreve dynamic link firebase.
  void initDynamicLinks() async {
    final PendingDynamicLinkData? data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri? deepLink = data?.link;
    if (deepLink != null) {
      Navigator.pushNamed(context, Routes.registerScreen);
    }
    FirebaseDynamicLinks.instance.onLink
        .listen((PendingDynamicLinkData? dynamicLink) {
      final Uri? deepLink = dynamicLink?.link;
      if (deepLink != null) {
        final queryParams = deepLink.queryParameters;
        String? ref = "";
        if (queryParams.length > 0) {
          ref = queryParams['ref'];
        }
        Navigator.pushNamed(context, Routes.registerScreen, arguments: ref);
      }
    }).onError((error) {
      //log(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
        children: screens,
      ),
      bottomNavigationBar: BottomAppBar(
        color: AppColors.white,
        shape: CircularNotchedRectangle(),
        notchMargin: 0,
        child: Container(
          height: 60,
          child: Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: _barItem(context, homeIndex)),
                Expanded(
                  child: _barItem(context, accountIndex),
                ),
                Expanded(
                  child: _barItem(context, zaloIndex),
                ),
                Expanded(
                  child: _barItem(context, messengerIndex),
                ),
              ]),
        ),
      ),
    );
  }
}
