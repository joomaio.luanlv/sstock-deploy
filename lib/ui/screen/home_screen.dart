import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/api_constant.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/images.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:badges/badges.dart' as badges;
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:scale_size/scale_size.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<ListHistoryProductCubit>(
        create: (_) => ListHistoryProductCubit()
          ..getListHistoryProduct({
            "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
            // "status": 2
          }),
      ),
      BlocProvider<ListProductCubit>(
        create: (_) => ListProductCubit()..getListProduct(),
      ),
      BlocProvider<ListAssetCubit>(
        create: (_) => ListAssetCubit()..getListAsset(),
      ),
    ], child: HomeBody());
  }
}

class HomeBody extends StatelessWidget {
  final RefreshController controller = RefreshController();

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      messageNotify: CustomSnackBar<ListHistoryProductCubit>(),
      loadingWidget:
          CustomLoading<ListHistoryProductCubit>(), //ListProductCubit
      hideAppBar: true,
      body: SmartRefresher(
        header: WaterDropMaterialHeader(),
        enablePullUp: false,
        enablePullDown: true,
        onRefresh: () {
          BlocProvider.of<ListHistoryProductCubit>(context)
              .getListHistoryProduct({
            "uid": UserInfoBuilderWidget.getUserInfo(context)?.id,
            // "status": 3
          });
          BlocProvider.of<ListAssetCubit>(context).getListAsset();
          controller.refreshCompleted();
          reloadDataHome(context);
        },
        controller: controller,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              renderInfoUser(),
              renderHeader(context),
              SizedBox(height: 15),
              renderListInvesting(context),
              SizedBox(height: 25),
              CommonWidget.baseLineWidget(
                  margin: EdgeInsets.symmetric(vertical: 0)),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: CustomTextLabel(
                      "Sản phẩm đầu tư",
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: AppColors.ff00A95E,
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    width: 60,
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.ff00A95E, width: 1),
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(20)),
                  )
                ],
              ),
              renderListProduct(),
              SizedBox(height: 25),
              // CommonWidget.baseLineWidget(
              //     margin: EdgeInsets.symmetric(vertical: 0)),
              // SizedBox(height: 20),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Container(
              //       padding: EdgeInsets.only(left: 15, right: 15),
              //       child: CustomTextLabel(
              //         "Truy cập nhanh",
              //         fontWeight: FontWeight.w700,
              //         fontSize: 20,
              //         color: AppColors.ff00A95E,
              //         textAlign: TextAlign.left,
              //       ),
              //     )
              //   ],
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     Container(
              //       margin: EdgeInsets.only(top: 5),
              //       width: 60,
              //       decoration: BoxDecoration(
              //           border: Border.all(color: AppColors.ff00A95E, width: 1),
              //           color: AppColors.white,
              //           borderRadius: BorderRadius.circular(20)),
              //     )
              //   ],
              // ),
              // renderListButton(context),
              // SizedBox(height: 20),
              CommonWidget.baseLineWidget(
                  margin: EdgeInsets.symmetric(vertical: 0)),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: CustomTextLabel(
                      "Lịch sử giao dịch",
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: AppColors.ff00A95E,
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    width: 60,
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.ff00A95E, width: 1),
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(20)),
                  )
                ],
              ),
              SizedBox(height: 25),
              renderListHistory(),
              SizedBox(height: 25),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderItemHistory(BuildContext context, HistoryModel? historyModel) {
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, Routes.detailHistoryScreen,
              arguments: historyModel);
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.ff00A95E, width: 1),
              color: AppColors.white,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: CustomTextLabel(
                      "${historyModel?.startDate} - ${historyModel?.endDate}",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: AppColors.ff404A69,
                    ),
                  ),
                  renderLabel(historyModel)
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 2,
                    child: CustomTextLabel(
                      historyModel?.productName,
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: AppColors.ff585858,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: CustomTextLabel(
                        "${historyModel?.originValue}",
                        formatCurrency: false,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: AppColors.ff585858,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              IntrinsicHeight(
                child: Row(
                  children: [
                    renderInfo(
                        title: "Lãi suất",
                        value: "${historyModel?.iRate ?? ""}%",
                        alignment: Alignment.centerLeft),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                      width: 1,
                      color: AppColors.ffF3F3F3,
                    ),
                    renderInfo(
                        title:
                            historyModel?.actionType == 0 ? "Lợi nhuận" : "Lãi",
                        value: historyModel?.profit,
                        formatCurrency: false),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                      width: 1,
                      color: AppColors.ffF3F3F3,
                    ),
                    renderInfo(
                        title: historyModel?.actionType == 0
                            ? "Tổng nhận"
                            : "Tổng nộp",
                        value: "${(historyModel?.total)}",
                        alignment: Alignment.centerRight,
                        fontSizeValue: 16,
                        fontWeightValue: FontWeight.w600,
                        colorValue: AppColors.base_color,
                        formatCurrency: false),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  renderListHistory() {
    return BlocBuilder<ListHistoryProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<HistoryModel>>) {
          var list = state.data;
          if (list.length > 0) {
            return ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return renderItemHistory(context, list[index]);
              },
              itemCount: list.length,
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 15,
                );
              },
            );
          } else {
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: CustomTextLabel(
                  "Không tìm thấy giao dịch nào phát sinh.",
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.base_color,
                  textAlign: TextAlign.center,
                ));
          }
        }

        return SizedBox.shrink();
      },
    );
  }

  Widget renderLabel(HistoryModel? historyModel) {
    // var data = historyModel?.getValueState();
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              color: getColorByKeyName(historyModel?.statusColor),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          child: CustomTextLabel(
            historyModel?.statusText,
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  renderInfo(
      {required String title,
      value,
      FontWeight? fontWeightValue,
      double? fontSizeValue,
      Color? colorValue,
      AlignmentGeometry? alignment,
      bool formatCurrency = false}) {
    return Expanded(
        child: Align(
      alignment: alignment ?? Alignment.center,
      child: Column(
        children: [
          CustomTextLabel(
            title,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            textAlign: TextAlign.center,
            color: AppColors.ff585858,
          ),
          SizedBox(height: 3),
          CustomTextLabel(
            value,
            fontWeight: fontWeightValue ?? FontWeight.w400,
            fontSize: fontSizeValue ?? 16,
            textAlign: TextAlign.center,
            formatCurrency: formatCurrency,
            color: colorValue ?? AppColors.ff222222,
          ),
        ],
      ),
    ));
  }

  renderListButton(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routes.packageDueScreen);
            },
            child: Container(
              width: 100,
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.ff00A95E, width: 1),
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  Container(
                      child: IconButton(
                    padding:
                        EdgeInsets.only(left: 2, right: 2, top: 0, bottom: 2),
                    iconSize: 50,
                    icon: Icon(Icons.calculate, color: AppColors.ff00A95E),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.packageDueScreen);
                    },
                  )),
                  CustomTextLabel(
                    "Dự kiến lãi",
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: AppColors.black,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routes.referentScreen);
            },
            child: Container(
              width: 100,
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.ff00A95E, width: 1),
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  Container(
                      child: IconButton(
                    padding:
                        EdgeInsets.only(left: 2, right: 2, top: 0, bottom: 2),
                    iconSize: 50,
                    icon: Icon(Icons.code, color: AppColors.ff00A95E),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.referentScreen);
                    },
                  )),
                  CustomTextLabel(
                    "Mã giới thiệu",
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: AppColors.black,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routes.questionScreen);
            },
            child: Container(
              width: 100,
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.ff00A95E, width: 1),
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  Container(
                      child: IconButton(
                    padding:
                        EdgeInsets.only(left: 2, right: 2, top: 0, bottom: 2),
                    iconSize: 50,
                    icon: Icon(Icons.question_mark, color: AppColors.ff00A95E),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.questionScreen);
                    },
                  )),
                  CustomTextLabel(
                    "Hỏi đáp",
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: AppColors.black,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderListInvesting(BuildContext context) {
    return BlocBuilder<ListHistoryProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<HistoryModel>>) {
          var list = state.data.where((element) => element.status == 3).toList();
          return list.length > 0
              ? Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: CustomTextLabel(
                            "Hiệu quả đầu tư",
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: AppColors.ff00A95E,
                            textAlign: TextAlign.left,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          width: 60,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: AppColors.ff00A95E, width: 1),
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(20)),
                        )
                      ],
                    ),
                    SizedBox(height: 25),
                    ListView.separated(
                      physics: NeverScrollableScrollPhysics(),
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                      shrinkWrap: true,
                      itemBuilder: (context, int index) {
                        return renderListInvestingItem(context, list[index]);
                      },
                      itemCount: list.length,
                      separatorBuilder: (context, int index) {
                        return Container(
                          height: 15,
                        );
                      },
                    )
                  ],
                )
              : Container();
        }
        return SizedBox.shrink();
      },
    );
  }

  Color getColorByKeyName(String? color) {
    return AppColors.getColorFromHex(color);
  }

  renderListInvestingItem(BuildContext context, HistoryModel? historyModel) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, Routes.detailHistoryScreen,
            arguments: historyModel);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.ff00A95E, width: 1),
            color:
                getColorByKeyName(historyModel?.colorCode), //AppColors.white,
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            Expanded(
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextLabel(
                          historyModel?.productName,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.white,
                        ),
                        SizedBox(height: 5),
                        CustomTextLabel(
                          historyModel?.originValue,
                          fontWeight: FontWeight.w400,
                          maxLines: 3,
                          fontSize: 15,
                          color: AppColors.white,
                        ),
                      ],
                    ))),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: CustomTextLabel(
                "+ ${historyModel?.profit}",
                fontWeight: FontWeight.w600,
                maxLines: 3,
                fontSize: 20,
                color: AppColors.white, //AppColors.ff63C856,
              ),
            )
          ],
        ),
      ),
    );
  }

  renderInfoUser() {
    return UserInfoBuilderWidget(
      builder: (BuildContext context, UserModel? userModel) {
        return Container(
          margin: EdgeInsets.only(
              top: ScaleSize.statusBarHeight + 10, left: 15, right: 15),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: AppColors.ffF3F3F3, shape: BoxShape.circle),
                child: Image.asset(
                  AppImages.ic_user,
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextLabel(
                    "Xin chào, ${userModel?.fullName ?? ""}",
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: AppColors.ff222222,
                  ),
                  SizedBox(height: 5),
                  CustomTextLabel(
                    "Chào mừng bạn đến với SStock",
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: AppColors.ff585858,
                  ),
                ],
              )),
              SizedBox(width: 5),
              BlocBuilder<TotalUnreadNotifyCubit, BaseState>(
                builder: (_, state) {
                  int total = 0;
                  if (state is LoadedState<int>) {
                    total = state.data;
                  }
                  return Container(
                      margin: EdgeInsets.only(top: 5, right: 0),
                      child: InkWell(
                        child: (total > 0)
                            ? badges.Badge(
                                badgeContent: CustomTextLabel(
                                  total,
                                  color: AppColors.white,
                                  fontSize: 10,
                                ),
                                child: Icon(Icons.notifications_active),
                              )
                            : Icon(Icons.notifications_active),
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            Routes.listNotificationScreen,
                          );
                        },
                      ));
                },
              ),
            ],
          ),
        );
      },
    );
  }

  renderListProduct() {
    return BlocBuilder<ListProductCubit, BaseState>(
      builder: (_, state) {
        if (state is LoadedState<List<ProductModel>>) {
          var proList =
              groupBy(state.data, (ProductModel pro) => pro.productCat)
                  .entries
                  .toList();
          return Column(
            children: [
              ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return renderListProductGroup(proList[index].value);
                },
                itemCount: proList.length,
                separatorBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 15,
                  );
                },
              )
            ],
          );
        }
        return SizedBox.shrink();
      },
    );
  }

  renderListProductGroup(List<ProductModel> proList) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: CustomTextLabel(
                proList.first.productCat,
                fontWeight: FontWeight.w600,
                fontSize: 18,
                color: AppColors.ff585858,
                textAlign: TextAlign.left,
              ),
            )
          ],
        ),
        ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return renderItemProduct(context, proList[index]);
          },
          itemCount: proList.length,
          separatorBuilder: (BuildContext context, int index) {
            return Container(
              height: 20,
            );
          },
        )
      ],
    );
  }

  renderHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      // height: 140,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.ic_bg_header_home),
              fit: BoxFit.fill)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextLabel(
                  "Giá trị tài sản",
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: AppColors.white,
                ),
                SizedBox(height: 10),
                renderAsset(),
                SizedBox(height: 70),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                Routes.mainInvestScreen,
              );
            },
            child: Container(
              margin: EdgeInsets.only(top: 10, right: 10),
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: AppColors.ffC8FFC1, shape: BoxShape.circle),
              child: Icon(
                Icons.arrow_forward,
                size: 16,
                color: AppColors.black,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget renderAsset() {
    return BlocBuilder<ListAssetCubit, BaseState>(builder: (_, state) {
      if (state is LoadedState<List<AssetModel>?> && state.data!.length > 0) {
        var list = state.data ?? [];
        return Wrap(
          spacing: 8.0,
          runSpacing: 4.0,
          children: <Widget>[
            for (var item in list)
              Wrap(
                crossAxisAlignment: WrapCrossAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10, right: 20),
                    child: CustomTextLabel(
                      item.totalFormat,
                      formatCurrency: false,
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: AppColors.white,
                    ),
                  ),
                  // Container(
                  //     margin: EdgeInsets.only(bottom: 10, right: 20),
                  //     child: Wrap(
                  //       children: [
                  //         CustomTextLabel(
                  //           '${item.totalInterestFormat}',
                  //           formatCurrency: false,
                  //           fontWeight: FontWeight.w600,
                  //           fontSize: 14,
                  //           color: Color.fromARGB(255, 89, 31, 31),
                  //         ),
                  //         CustomTextLabel(
                  //           ' / ',
                  //           formatCurrency: false,
                  //           fontWeight: FontWeight.w600,
                  //           fontSize: 14,
                  //           color: AppColors.white,
                  //         ),
                  //         CustomTextLabel(
                  //           '${item.percent}%',
                  //           formatCurrency: false,
                  //           fontWeight: FontWeight.w600,
                  //           fontSize: 14,
                  //           color: Color.fromARGB(255, 89, 31, 31),
                  //         ),
                  //       ],
                  //     )),
                ],
              )
          ],
        );
      }
      return Column(
        children: [
          CustomTextLabel(
            0,
            formatCurrency: true,
            fontWeight: FontWeight.w600,
            fontSize: 18,
            color: AppColors.white,
          ),
          SizedBox(
            height: 20,
          ),
        ],
      );
    });
  }

  renderItemProduct(BuildContext context, ProductModel? productModel) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, Routes.detailProductScreen,
            arguments: productModel);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.ff00A95E, width: 1),
            color: AppColors.white,
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            BaseNetworkImage(
              url: ApiConstant.getImageHost(productModel?.logo),
              width: 60,
              height: 60,
            ),
            SizedBox(width: 10),
            Expanded(
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextLabel(
                          productModel?.name,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          color: AppColors.ff404A69,
                        ),
                        SizedBox(height: 5),
                        CustomTextLabel(
                          productModel?.description,
                          fontWeight: FontWeight.w400,
                          maxLines: 3,
                          fontSize: 14,
                          color: AppColors.ff63C856,
                        ),
                      ],
                    ))),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                AppImages.ic_arrow_right,
                width: 10,
              ),
            )
          ],
        ),
      ),
    );
  }

  void reloadDataHome(BuildContext context) {
    BlocProvider.of<UserInfoCubit>(context).getUserInfo();
    BlocProvider.of<ListProductCubit>(context).getListProduct();
    BlocProvider.of<TotalUnreadNotifyCubit>(context).getTotalUnread();
    BlocProvider.of<ListHistoryProductCubit>(context).getListHistoryProduct(
        {"uid": UserInfoBuilderWidget.getUserInfo(context)?.id, "status": 2});
  }
}
