import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';

class CustomDialog {
  static Future<dynamic> showDialogConfirm(
    BuildContext buildContext, {
    String? title = "Thông báo",
    String? content,
    Widget? contentWidget,
    String? titleOK,
    String? image,
    bool showCancel = true,
    bool showIconClose = true,
    bool barrierDismissible = true,
    Function? onTapOK,
    Function? onTapCancel,
  }) {
    return CustomDialog.showDialogCommon(buildContext,
        barrierDismissible: barrierDismissible,
        showIconClose: showIconClose,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (image != null)
              Image.asset(
                image,
                height: 100,
              ),
            SizedBox(height: 10),
            CustomTextLabel(
              title,
              fontWeight: FontWeight.w600,
              fontSize: 16,
              textAlign: TextAlign.center,
              color: AppColors.colorTitle,
            ),
            SizedBox(height: 10),
            contentWidget ??
                CustomTextLabel(
                  content,
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                  textAlign: TextAlign.center,
                  color: AppColors.colorTitle,
                ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: showCancel ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,
              children: [
                showCancel
                    ? BaseButton(
                        onTap: () {
                          Navigator.pop(buildContext);
                          onTapCancel?.call();
                        },
                        title: "Huỷ",
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                        backgroundColor: AppColors.ffEBEEF2,
                        titleColor: AppColors.ff999999,
                        // fontWeight: FontWeight.w600,
                        // fontSizeTitle: 15,
                      )
                    : SizedBox(),
                BaseButton(
                  onTap: () {
                    Navigator.pop(buildContext);
                    onTapOK?.call();
                  },
                  title: titleOK ?? "OK",
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  titleColor: AppColors.white,
                  // fontWeight: FontWeight.w600,
                  // fontSizeTitle: 15,
                ),
              ],
            )
          ],
        ));
  }

  static Future<dynamic> showDialogCommon(
    BuildContext context, {
    Widget? child,
    bool barrierDismissible = true,
    bool showIconClose = true,
  }) {
    return showDialog(
      barrierDismissible: barrierDismissible,
      builder: (BuildContext _context) {
        return Container(
          child: Center(
            child: Material(
              type: MaterialType.transparency,
              child: Wrap(
                alignment: WrapAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      FocusScope.of(_context).requestFocus(FocusNode());
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      // width: 1.sw,
                      child: Stack(
                        children: [
                          Container(margin: EdgeInsets.only(top: 20, bottom: 20), child: child),
                          if (showIconClose == true)
                            Positioned(
                              top: 10,
                              right: 0,
                              child: InkWell(
                                onTap: () {
                                  Navigator.pop(_context);
                                },
                                child: Container(
                                  child: Icon(
                                    Icons.clear_rounded,
                                    size: 20,
                                  ),
                                ),
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
      context: context,
    );
  }
}
