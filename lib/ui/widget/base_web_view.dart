import 'dart:convert';
import 'dart:io';

import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebViewArg {
  final String initialUrl;
  final String? title;

  CustomWebViewArg({required this.initialUrl, this.title});
}

class CustomWebView extends StatefulWidget {
  final CustomWebViewArg? customWebViewArg;

  const CustomWebView({Key? key, this.customWebViewArg}) : super(key: key);

  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {
  WebViewController? _webViewController;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
    isLoading = true;
  }

  bool isLoading = false;
  final _key = UniqueKey();

  loadAsset() async {
    String fileHtmlContents =
        '<html><meta name="viewport" content="width=device-width, shrink-to-fit=YES"><head><style>img { width: 100%;'
        'height:auto;max-height:100%;}</style></head><body>${widget.customWebViewArg?.title}</body><html>';
    _webViewController?.loadUrl(
        Uri.dataFromString(fileHtmlContents, mimeType: 'text/html', encoding: Encoding.getByName('utf-8')).toString());
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: widget.customWebViewArg?.title,
      body: Stack(
        children: <Widget>[
          WebView(
            key: _key,
            onWebViewCreated: (controller) {
              _webViewController = controller;
//            loadAsset();
            },
            initialUrl: widget.customWebViewArg?.initialUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
              setState(() {
                isLoading = false;
              });
            },
            onWebResourceError: (e) {
              print("===build =====${(e as WebResourceError).description}");
              setState(() {
                isLoading = false;
              });
            },
          ),
          isLoading
              ? Center(
                  child: BaseProgressIndicator(),
                )
              : Container(),
        ],
      ),
    );
  }
}
