import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/custom_text_label.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

typedef IndexedWidgetBuilder = Widget Function(int index);

class CustomDropDown extends StatefulWidget {
  final double? heightOfBox;
  final EdgeInsets margin;
  final String title;
  final String hintText;
  final List<String>? listValues;
  final int? selectedIndex;
  final Function? didSelected;
  final bool isRequired;
  final Color? titleColor;
  final Color? valueColor;
  final Color? bgColorDropdownSelect;
  final BoxBorder? border;
  final BorderRadius? borderRadius;
  final bool enabled;
  final IndexedWidgetBuilder? renderItem;

  const CustomDropDown(
      {this.heightOfBox,
      this.margin = EdgeInsets.zero,
      this.title = "",
      this.hintText = "",
      required this.listValues,
      this.selectedIndex,
      this.didSelected,
      this.isRequired = false,
      this.titleColor,
      this.valueColor,
      Key? key,
      this.bgColorDropdownSelect,
      this.enabled = true,
      this.border,
      this.borderRadius,
      this.renderItem})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CustomDropDownState();
  }
}

class CustomDropDownState extends State<CustomDropDown> {
  int? selectedIndex;
  String errorText = "";

  @override
  void initState() {
    super.initState();
    if (widget.selectedIndex != null && widget.selectedIndex! < (widget.listValues?.length ?? 0)) {
      selectedIndex = widget.selectedIndex;
    }
  }

  @override
  void didUpdateWidget(covariant CustomDropDown oldWidget) {
    super.didUpdateWidget(oldWidget);
    selectedIndex = widget.selectedIndex;
  }

  bool get isValid => _validate();

  bool _validate() {
    String error = "";
    if (widget.isRequired == true && selectedIndex == null) {
      error = "Vui lòng chọn dữ liệu";
    }
    setState(() {
      errorText = error;
    });
    return error.isEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.title.isNotEmpty) CustomTextLabel(widget.title, color: widget.titleColor ?? AppColors.colorTitle),
          Container(
            height: widget.heightOfBox ?? 46,
            padding: EdgeInsets.symmetric(horizontal: 10.sw),
            decoration: BoxDecoration(
              borderRadius: widget.borderRadius ?? BorderRadius.zero,
              border:
                  widget.border ?? Border(bottom: BorderSide(color: AppColors.base_color_border_textfield, width: 0.6)),
            ),
            child: DropdownButtonHideUnderline(
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: widget.bgColorDropdownSelect ?? AppColors.white,
                ),
                child: DropdownButton<int>(
                  items: createListDropdownMenuItem(),
                  value: selectedIndex,
                  onChanged: widget.enabled
                      ? (int? _index) {
                          setState(() {
                            selectedIndex = _index;
                            widget.didSelected?.call(_index);
                            errorText = "";
                          });
                        }
                      : null,
                  style: TextStyle(color: AppColors.white, fontSize: 14.sw, fontWeight: FontWeight.w400),
                  hint: CustomTextLabel(widget.hintText,
                      color: AppColors.ffBDBDBD, fontWeight: FontWeight.w400, fontSize: 14),
                  icon: Container(
                    width: 15,
                    // height: 10,
                    margin: EdgeInsets.only(right: 4),
                    child: Icon(Icons.arrow_drop_down
                        // width: 10,
                        // height: 10,
                        ),
                  ),
                  isExpanded: true,
                ),
              ),
            ),
          ),
          errorText.isNotEmpty
              ? Container(
                  margin: const EdgeInsets.only(top: 6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(
                        Icons.error,
                        size: 13,
                        color: Colors.red,
                      ),
                      const SizedBox(width: 2),
                      Expanded(
                        child: CustomTextLabel(
                          errorText,
                          color: Colors.red,
                          fontSize: 12.sw,
                        ),
                      ),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  List<DropdownMenuItem<int>> createListDropdownMenuItem() {
    List<DropdownMenuItem<int>> list = [];
    for (int i = 0; i < (widget.listValues?.length ?? 0); i++) {
      DropdownMenuItem<int> item = DropdownMenuItem<int>(
        child: widget.renderItem?.call(i) ??
            CustomTextLabel(
              widget.listValues![i],
              color: widget.valueColor ?? AppColors.black,
              maxLines: 1,
            ),
        value: i,
      );
      list.add(item);
    }
    return list;
  }
}
