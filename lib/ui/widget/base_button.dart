import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

class BaseButton extends StatelessWidget {
  final String? title;
  final Color? titleColor;
  final BoxDecoration? decoration;
  final GestureTapCallback? onTap;
  final Widget? child;
  final Color? backgroundColor;
  final double? borderRadius;
  final Color? borderColor;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final AlignmentGeometry? alignment;
  final double? width;
  final double? height;

  final bool enable;

  const BaseButton(
      {this.child,
      Key? key,
      this.decoration,
      this.onTap,
      this.backgroundColor = AppColors.base_color,
      this.borderRadius,
      this.borderColor = Colors.transparent,
      this.margin,
      this.padding,
      this.alignment,
      this.width,
      this.height,
      this.title,
      this.enable = true, this.titleColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius? borderRadiusInWell = BorderRadius.circular(0);
    if (borderRadius != null) {
      borderRadiusInWell = BorderRadius.circular(borderRadius!);
    }
    return Container(
      margin: margin ?? EdgeInsets.zero,
      alignment: alignment,
      decoration: decoration ??
          BoxDecoration(
              gradient: backgroundColor == null ? AppColors.base_color_gradient : null,
              color: enable ? (backgroundColor ?? Colors.white) : AppColors.ffD6D6D6,
              border: Border.all(color: borderColor!),
              borderRadius: BorderRadius.circular(borderRadius ?? 10.sw)),
      child: Material(
        child: InkWell(
          borderRadius: borderRadiusInWell,
          onTap: () {
            if (enable) {
              FocusScope.of(context).requestFocus(FocusNode());
              onTap?.call();
            }
          },
          child: Container(
              width: width,
              height: height,
              // alignment: Alignment.center,
              padding: padding ?? EdgeInsets.symmetric(vertical: 12.sw, horizontal: 10.sw),
              child: renderChild()),
        ),
        color: Colors.transparent,
      ),
    );
  }

  renderChild() {
    if (title != null) {
      return CustomTextLabel(
        title,
        textAlign: TextAlign.center,
        fontWeight: FontWeight.w600,
        fontSize: 16,
        color: titleColor ?? (enable ? AppColors.white : AppColors.ff585858),
      );
    }
    return child ?? Container();
  }
}
