import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';

class BaseCheckBox extends StatefulWidget {
  final dynamic title;
  final bool? isCheck;
  final Function? onChanged;
  final EdgeInsetsGeometry? margin;

  const BaseCheckBox({Key? key, this.title, this.isCheck, this.onChanged, this.margin}) : super(key: key);

  @override
  _BaseCheckBoxState createState() => _BaseCheckBoxState();
}

class _BaseCheckBoxState extends State<BaseCheckBox> {
  bool isCheck = false;

  @override
  void initState() {
    isCheck = widget.isCheck ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Row(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                isCheck = !isCheck;
                widget.onChanged?.call(isCheck);
              });
            },
            child: Icon(
              isCheck ? Icons.check_box_outlined : Icons.check_box_outline_blank_outlined,
              color: AppColors.colorTitle,
              size: 20,
            ),
          ),
          SizedBox(width: 8),
          Expanded(
              child: widget.title is Widget
                  ? (widget.title)
                  : CustomTextLabel(widget.title?.toString(), fontWeight: FontWeight.w400, fontSize: 16))
        ],
      ),
    );
  }
}
