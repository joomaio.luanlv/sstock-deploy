import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:app_sstock/ui/widget/base_progress_indicator.dart';
import 'package:scale_size/scale_size.dart';

class BaseNetworkImage extends StatelessWidget {
  final String? url;
  final double borderRadius;
  final double? width;
  final double? height;
  final String? errorAssetImage;
  final double? loadingSize;

  const BaseNetworkImage(
      {Key? key, this.url, this.borderRadius = 10, this.width, this.height, this.errorAssetImage, this.loadingSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget errorWidget = ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius),
      child: Container(
        width: this.width ?? double.infinity,
        height: this.height ?? double.infinity,
        child: errorAssetImage?.isNotEmpty ?? false
            ? Image.asset(
                errorAssetImage!,
                width: this.width,
                height: this.height,
                fit: BoxFit.cover,
              )
            : Icon(Icons.image),
      ),
    );
    return url == null
        ? errorWidget
        : ClipRRect(
            child: CachedNetworkImage(
              width: this.width ?? double.infinity,
              height: this.height ?? double.infinity,
              // fit: BoxFit.fill,
              imageUrl: url!,
              placeholder: (context, url) => Center(
                child:  BaseProgressIndicator(size: loadingSize ?? 10.sw),
              ),
              errorWidget: (context, url, error) => Icon(Icons.image),
            ),
            borderRadius: BorderRadius.circular(borderRadius),
          );

  }
}
