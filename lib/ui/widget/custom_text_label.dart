import 'package:app_sstock/localizations.dart';
import 'package:app_sstock/utils/common.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

import 'locale_widget.dart';

class CustomTextLabel extends StatelessWidget {
  final dynamic title;
  final double? fontSize;
  final FontWeight fontWeight;
  final Color color;
  final TextAlign textAlign;
  final int maxLines;
  final double? fontHeight;
  final bool isLocalizeTitle;
  final FontStyle? fontStyle;
  final bool formatCurrency;
  final TextOverflow? overflow;

  const CustomTextLabel(this.title,
      {Key? key,
      this.fontSize,
      this.fontWeight = FontWeight.normal,
      this.color = Colors.black,
      this.textAlign = TextAlign.start,
      this.maxLines = 50,
      this.fontHeight,
      this.isLocalizeTitle = true,
      this.fontStyle,
      this.formatCurrency = false,
      this.overflow})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LocaleWidget(
      builder: (Language? locale) {
        return Text(
          (formatCurrency
                  ? Common.formatPrice(title ?? "")
                  : locale?.getText(title?.toString() ?? "").trim() ?? title?.toString()?.trim()) ??
              "",
          textAlign: textAlign,
          overflow: overflow ?? TextOverflow.ellipsis,
          maxLines: maxLines,
          style: TextStyle(
              fontStyle: fontStyle ?? FontStyle.normal,
              height: fontHeight ?? 22.27 / 19,
              fontSize: fontSize ?? 14.sw,
              fontWeight: fontWeight,
              color: color),
        );
      },
    );
  }
}
