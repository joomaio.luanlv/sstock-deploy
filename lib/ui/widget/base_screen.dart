import 'dart:developer';

import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/common/common_widget.dart';
import 'package:app_sstock/ui/widget/custom_text_label.dart';
import 'package:flutter/material.dart';

class BaseScreen extends StatelessWidget {
  static double toolbarHeight = 50;

  // body của màn hình
  final Widget? body;

  // title của appbar có 2 kiểu String và Widget
  // title là kiểu Widget thì sẽ render widget
  // title là String
  final dynamic? title;

  // trường hợp có AppBar đặc biệt thì dùng customAppBar
  final Widget? customAppBar;

  // callBack của onBackPress với trường hợp  hiddenIconBack = false
  final Function? onBackPress;

  // custom widget bên phải của appBar
  final List<Widget>? rightWidgets;

  // loadingWidget để show loading toàn màn hình
  final Widget? loadingWidget;

  // show thông báo
  final Widget? messageNotify;
  final Widget? floatingButton;

  // nếu true => sẽ ẩn backIcon , mặc định là true
  final bool hiddenIconBack;

  final Color colorTitle;
  final Color backgroundColor;
  final bool hideAppBar;

  final DecorationImage? backgroundImage;

  const BaseScreen(
      {Key? key,
      this.body,
      this.title = "",
      this.customAppBar,
      this.onBackPress,
      this.rightWidgets,
      this.hiddenIconBack = false,
      this.colorTitle = AppColors.ff404A69,
      this.loadingWidget,
      this.hideAppBar = false,
      this.messageNotify,
      this.floatingButton,
      this.backgroundColor = AppColors.white,
      this.backgroundImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold(
        appBar: hideAppBar
            ? null
            : (customAppBar == null
                ? baseAppBar(
                    context,
                    hiddenIconBack: hiddenIconBack,
                    onBackPress: onBackPress,
                    rightWidgets: rightWidgets,
                    title: title,
                    colorTitle: colorTitle,
                  )
                : customAppBar),
        backgroundColor: backgroundColor,
        body: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            decoration: BoxDecoration(image: backgroundImage),
            child: Stack(
              children: [
                body ?? Container(),
                Positioned(
                  top: AppDimens.SIZE_0,
                  right: AppDimens.SIZE_0,
                  left: AppDimens.SIZE_0,
                  bottom: AppDimens.SIZE_0,
                  child: loadingWidget ?? Container(),
                ),
                messageNotify ?? Container()
              ],
            ),
          ),
        ),
        floatingActionButton: floatingButton ?? null);
    return scaffold;
  }

  static baseAppBar(BuildContext context,
      {dynamic title,
      Color colorTitle = AppColors.ff404A69,
      bool hiddenIconBack = false,
      Function? onBackPress,
      List<Widget>? rightWidgets}) {
    var widgetTitle;
    if (title is Widget) {
      widgetTitle = title;
    } else {
      widgetTitle = CustomTextLabel(
        title?.toString(),
        maxLines: 2,
        fontWeight: FontWeight.w600,
        fontSize: 16,
        textAlign: TextAlign.center,
        color: colorTitle,
      );
    }
    return AppBar(
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1.0),
        child: CommonWidget.baseLineWidget(),
      ),
      elevation: 0,
      toolbarHeight: toolbarHeight,
      backgroundColor: Colors.white,
      title: widgetTitle,
      leading: hiddenIconBack
          ? Container()
          : InkWell(
              onTap: () {
                Navigator.pop(context);
                onBackPress?.call();
              },
              child: Container(
                alignment: Alignment.center,
                child: Image.asset(
                  AppImages.IC_BACK_2,
                  height: 20,
                ),
              ),
            ),
      centerTitle: true,
      actions: rightWidgets ?? [],
    );
  }

  static baseAppBarOnlyBack(BuildContext context, {Function? onBackPress, List<Widget>? rightWidgets}) {
    return AppBar(
      elevation: 0,
      toolbarHeight: toolbarHeight,
      backgroundColor: Colors.white,
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
          onBackPress?.call();
        },
        child: Container(
          margin: EdgeInsets.only(top: 8, left: 8),
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: AppColors.white, border: Border.all(width: 1, color: AppColors.ffF3F3F3)),
          alignment: Alignment.center,
          child: Image.asset(
            AppImages.IC_BACK,
            height: 18,
          ),
        ),
      ),
      centerTitle: true,
      actions: rightWidgets ?? [],
    );
  }

  static baseTitle({
    required String title,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      child: CustomTextLabel(
        title,
        textAlign: TextAlign.center,
        fontWeight: FontWeight.w600,
        fontSize: 26,
        color: AppColors.base_color,
      ),
    );
  }
}
