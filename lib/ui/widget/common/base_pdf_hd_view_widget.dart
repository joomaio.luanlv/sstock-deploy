import 'dart:io';

import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/images.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class PDFHDViewWidgetArg {
  final String initialUrl;
  final String? title;

  final String? paymentType;
  final int? irId;
  final String? originvalue;
  final String? code;
  final String? contractLink;

  PDFHDViewWidgetArg(
      {required this.initialUrl,
      required this.paymentType,
      required this.irId,
      required this.originvalue,
      required this.code,
      required this.contractLink,
      this.title});
}

class PDFHDViewWidget extends StatelessWidget {
  final PDFHDViewWidgetArg? pdfViewWidgetArg;

  // PDFHDViewWidget({Key? key, this.pdfViewWidgetArg}) : super(key: key);

  // _PDFHDViewWidgetState createState() => _PDFHDViewWidgetState();
  const PDFHDViewWidget({Key? key, this.pdfViewWidgetArg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<BuyProductCubit>(
            create: (_) => BuyProductCubit(),
          ),
        ],
        child: _PDFHDViewWidgetState(
          pdfViewWidgetArg: pdfViewWidgetArg,
        ));
  }
}

class _PDFHDViewWidgetState extends StatelessWidget
    with WidgetsBindingObserver {
  final PDFHDViewWidgetArg? pdfViewWidgetArg;

  // _PDFHDViewWidgetState();
  _PDFHDViewWidgetState({
    Key? key,
    this.pdfViewWidgetArg,
  }) : super(key: key) {}

  @override
  Widget build(BuildContext context) {
    PDF pdf;
    if (Platform.isIOS == true) {
      pdf = PDF(
        enableSwipe: true,
        autoSpacing: true,
        pageFling: false,
      );
    } else {
      pdf = PDF(
        enableSwipe: true,
        autoSpacing: false,
        pageFling: false,
      );
    }
    return BaseScreen(
      title: pdfViewWidgetArg?.title,
      onBackPress: () {
        Navigator.pop(context);
      },
      messageNotify: CustomSnackBar<BuyProductCubit>(),
      loadingWidget: CustomLoading<BuyProductCubit>(),
      rightWidgets: [
        BaseButton(
          child: Image.asset(
            AppImages.ic_qrcode,
            height: 32,
            fit: BoxFit.fitWidth,
          ),
          backgroundColor: Colors.transparent,
          onTap: () {
            _showImageDialog(context);
          },
        )
      ],
      body: Stack(
        children: <Widget>[
          pdf.cachedFromUrl(
            pdfViewWidgetArg?.initialUrl ?? "",
            placeholder: (double progress) => Center(
                child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  BaseProgressIndicator(),
                  const SizedBox(width: 20.0),
                  CustomTextLabel(
                    'Đang tải ($progress%)',
                    fontSize: 15,
                    color: AppColors.base_color,
                  ),
                ],
              ),
            )),
            errorWidget: (dynamic error) => Center(
                child: CustomTextLabel(
              "Có lỗi xảy ra vui lòng thử lại sau !",
              color: AppColors.base_color,
            )),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BaseButton(
                onTap: () async {
                  BlocProvider.of<BuyProductCubit>(context).buyProduct({
                    "paymentType": pdfViewWidgetArg?.paymentType,
                    "description": "",
                    "cusId": UserInfoBuilderWidget.getUserInfo(context)?.id,
                    "irId": pdfViewWidgetArg?.irId,
                    "originvalue": pdfViewWidgetArg?.originvalue,
                    "code": pdfViewWidgetArg?.code,
                    "contractLink": pdfViewWidgetArg?.contractLink,
                  });
                },
                width: double.infinity,
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                title: "Chờ xác nhận",
              ),
              BlocListener<BuyProductCubit, BaseState>(
                listener: (_, state) {
                  if (state is LoadedState<bool> && state.data == true) {
                    BlocProvider.of<ListHistoryProductCubit>(context)
                        .getListHistoryProduct({
                      "uid": UserInfoBuilderWidget.getUserInfo(context)?.id
                    });

                    CustomDialog.showDialogConfirm(context,
                            showCancel: false,
                            showIconClose: false,
                            barrierDismissible: false,
                            content:
                                "Gửi yêu cầu thành công, vui lòng chờ xác nhận từ phía công ty!",
                            titleOK: "Đóng",
                            onTapOK: () {})
                        .then((value) {
                      Navigator.pop(context);
                    });
                  }
                },
                child: Container(),
              )
            ],
          ),
        ],
      ),
    );
  }

  void _showImageDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                AppImages.sstock_qr,
                fit: BoxFit.cover,
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Đóng'),
            ),
          ],
        );
      },
    );
  }
}
