import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/res/resources.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';

class CommonWidget {
  static Widget baseLineWidget(
      {Color? color, double? height, EdgeInsets? margin}) {
    return Container(
      margin: margin ?? EdgeInsets.zero,
      height: height ?? 1,
      color: color ?? AppColors.ffDBDBDB,
    );
  }

  static Widget baseQuoteWidget({required String quote}) {
    return Column(
      children: [
        Container(
            child: Image.asset(
          AppImages.ic_quo,
          width: 20,
        )),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
          child: CustomTextLabel(
            quote,
            textAlign: TextAlign.center,
            fontStyle: FontStyle.italic,
          ),
        ),
      ],
    );
  }

  static Widget renderItemInfoWidget({required String title, value}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 0, vertical: 12),
      child: Row(
        children: [
          Container(
              width: 120,
              child: Align(
                alignment: Alignment.centerLeft,
                child: CustomTextLabel(
                  title,
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  maxLines: 2,
                  color: AppColors.ff585858,
                ),
              )),
          SizedBox(width: 5),
          Expanded(
              child: Align(
            alignment: Alignment.centerLeft,
            child: CustomTextLabel(
              value,
              maxLines: 2,
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: AppColors.ff222222,
            ),
          )),
        ],
      ),
    );
  }

  static Widget renderActionBankWidget(
      {required String BankName,
      required String AccountNumber,
      required String AccountName,
      required bool? isDefault,
      required String icon,
      Null Function()? onTap,
      Widget? rightWidget}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            color: AppColors.ffF3F3F3, borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.only(bottom: 12),
        padding: EdgeInsets.only(left: 5, top: 10, bottom: 10, right: 10),
        child: Row(
          children: [
            Container(
              width: 48,
              height: 48,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: AppColors.ffF3F3F3, shape: BoxShape.circle),
              child: Image.asset(
                AppImages.ic_account_bank,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
                child: BankName.length > 0
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomTextLabel(
                            BankName,
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            maxLines: 2,
                            color: AppColors.ff585858,
                          ),
                          SizedBox(height: 5),
                          CustomTextLabel(
                            AccountName,
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: AppColors.ff222222,
                          ),
                          SizedBox(height: 5),
                          CustomTextLabel(
                            AccountNumber,
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: AppColors.ff585858,
                          ),
                        ],
                      )
                    : CustomTextLabel(
                        "Thêm ngân hàng",
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        maxLines: 2,
                        color: AppColors.ff585858,
                      )),
            SizedBox(width: 10),
            isDefault == true
                ? CustomTextLabel(
                    "Mặc định",
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    maxLines: 2,
                    color: AppColors.ff585858,
                  )
                : SizedBox(width: 0),
            SizedBox(width: isDefault == true ? 10 : 0),
            Container(
              child: Image.asset(
                AppImages.ic_arrow_right,
                width: 10,
                color: AppColors.ff585858,
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Widget renderActionWidget(
      {required String title,
      required String icon,
      Null Function()? onTap,
      Widget? rightWidget}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
              child: Image.asset(
                icon,
                width: 14,
              ),
            ),
            Expanded(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 6, vertical: 0),
              child: CustomTextLabel(
                title,
                fontWeight: FontWeight.w400,
                fontSize: 16,
                color: AppColors.ff585858,
              ),
            )),
            rightWidget ??
                Container(
                  child: Image.asset(
                    AppImages.ic_arrow_right,
                    width: 10,
                    color: AppColors.ff585858,
                  ),
                )
          ],
        ),
      ),
    );
  }
}
