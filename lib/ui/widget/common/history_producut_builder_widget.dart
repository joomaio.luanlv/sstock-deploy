import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef WidgetBuilder<S> = Widget Function(BuildContext context, List<HistoryModel>? list);

class HistoryProductBuilderWidget extends StatelessWidget {
  final WidgetBuilder builder;

  HistoryProductBuilderWidget({Key? key, required this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ListHistoryProductCubit, BaseState>(builder: (_, state) {
      List<HistoryModel>? list;
      if (state is LoadedState<List<HistoryModel>?>) {
        list = state.data;
      }
      return builder.call(context, list);
    });
  }
}
