import 'dart:io';

import 'package:app_sstock/res/colors.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class PDFViewWidgetArg {
  final String initialUrl;
  final String? title;

  PDFViewWidgetArg({required this.initialUrl, this.title});
}

class PDFViewWidget extends StatefulWidget {
  final PDFViewWidgetArg? pdfViewWidgetArg;

  PDFViewWidget({Key? key, this.pdfViewWidgetArg}) : super(key: key);

  _PDFViewWidgetState createState() => _PDFViewWidgetState();
}

class _PDFViewWidgetState extends State<PDFViewWidget> with WidgetsBindingObserver {
  _PDFViewWidgetState();

  @override
  Widget build(BuildContext context) {
    PDF pdf;
    if (Platform.isIOS == true) {
      pdf = PDF(
        enableSwipe: true,
        autoSpacing: true,
        pageFling: false,
      );
    } else {
      pdf = PDF(
        enableSwipe: true,
        autoSpacing: false,
        pageFling: false,
      );
    }
    return BaseScreen(
      title: widget.pdfViewWidgetArg?.title,
      onBackPress: () {
        Navigator.pop(context);
      },
      body: Stack(
        children: <Widget>[
          pdf.cachedFromUrl(
            widget.pdfViewWidgetArg?.initialUrl ?? "",
            placeholder: (double progress) => Center(
                child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  BaseProgressIndicator(),
                  const SizedBox(width: 20.0),
                  CustomTextLabel(
                    'Đang tải ($progress%)',
                    fontSize: 15,
                    color: AppColors.base_color,
                  ),
                ],
              ),
            )),
            errorWidget: (dynamic error) => Center(
                child: CustomTextLabel(
              "Có lỗi xảy ra vui lòng thử lại sau !",
              color: AppColors.base_color,
            )),
          )
        ],
      ),
    );
  }
}
