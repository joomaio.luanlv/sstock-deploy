import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef WidgetUserBuilder<S> = Widget Function(BuildContext context, UserModel? userModel);

class UserInfoBuilderWidget extends StatelessWidget {
  final WidgetUserBuilder builder;

  UserInfoBuilderWidget({Key? key,required this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserInfoCubit, BaseState>(builder: (_, state) {
      UserModel? user;
      if (state is LoadedState<UserModel>) {
        user = state.data;
      }
      return builder.call(context, user);
    });
  }

  static UserModel? getUserInfo(BuildContext context) {
    var state = BlocProvider.of<UserInfoCubit>(context).state;
    UserModel? user;
    if (state is LoadedState<UserModel>) {
      user = state.data;
    }
    return user;
  }
}
