import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef WidgetPackageBuilder<S> = Widget Function(BuildContext context, List<PackageDueModel>? list);

class PackageDueBuilderWidget extends StatelessWidget {
  final WidgetPackageBuilder builder;

  PackageDueBuilderWidget({Key? key, required this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PackageDueProductCubit, BaseState>(builder: (_, state) {
      List<PackageDueModel>? list;
      if (state is LoadedState<List<PackageDueModel>?>) {
        list = state.data;
      }
      return builder.call(context, list);
    });
  }
}