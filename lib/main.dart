import 'dart:io';
import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'ui/app.dart';
import 'utils/shared_preference.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  SharedPreferences preferences = await SharedPreferences.getInstance();
  String? language = preferences.getString(SPrefCache.PREF_KEY_LANGUAGE);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.white,
    statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
    statusBarBrightness: Brightness.light, // For iOS (dark icons)
  ));
  HttpOverrides.global = MyHttpOverrides();
  runApp(MultiBlocProvider(providers: [
    BlocProvider(
      create: (_) => LanguageCubit(),
    ),
    BlocProvider(
      create: (_) => UserInfoCubit(),
    ),
    BlocProvider(
      create: (_) => ListHistoryProductCubit(),
    ),
    BlocProvider(
      create: (_) => ListNotificationCubit(),
    ),
    BlocProvider(
      create: (_) => TotalUnreadNotifyCubit(),
    ),
  ], child: MyApp.language(language)));
}