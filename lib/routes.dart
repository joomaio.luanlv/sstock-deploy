import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/ui/screen/screen.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class Routes {
  Routes._();

  //screen name
  static const String splashScreen = "/splashScreen";
  static const String loginScreen = "/loginScreen";
  static const String mainScreen = "/mainScreen";
  static const String registerScreen = "/registerScreen";
  static const String inputPasswordRegisterScreen =
      "/inputPasswordRegisterScreen";
  static const String inputPasswordForgotScreen = "/inputPasswordForgotScreen";
  static const String inputOTPScreen = "/inputOTPScreen";
  static const String forgotPasswordScreen = "/forgotPasswordScreen";
  static const String mainInvestScreen = "/mainInvestScreen";
  static const String detailProductScreen = "/detailProductScreen";
  static const String detailInvestScreen = "/detailInvestScreen";
  static const String openOrderScreen = "/openOrderScreen";
  static const String userInfoScreen = "/userInfoScreen";
  static const String editUserInfoScreen = "/editUserInfoScreen";
  static const String editBankInfoScreen = "/editBankInfoScreen";
  static const String listBankOfUserScreen = "/listBankOfUserScreen";
  static const String securityInfoScreen = "/securityInfoScreen";
  static const String changePasswordScreen = "/changePasswordScreen";
  static const String customWebView = "/customWebView";
  static const String pdfViewWidget = "/PDFViewWidget";
  static const String pdfHDViewWidget = "/PDFHDViewWidget";
  static const String listNotificationScreen = "/listNotificationScreen";
  static const String referentScreen = "/referentScreen";
  static const String openOrderStockScreen = "/openOrderStockScreen";
  static const String borrowMoneyScreen = "/borrowMoneyScreen";
  static const String borrowStockScreen = "/borrowStockScreen";
  static const String packageDueScreen = "/PackageDueScreen";
  static const String listPackageUnfinishedScreen =
      "/ListPackageUnfinishedScreen";
  static const String detailHistoryScreen = "/detailHistoryScreen";
  static const String dailyProfitScreen = "/dailyProfitScreen";
  static const String questionScreen = "/questionScreen";
  static const String homeScreen = "/homeScreen";

  //init screen name
  static String initScreen() => splashScreen;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case mainScreen:
        return PageTransition(
            child: MainScreen(), type: PageTransitionType.fade);
      case referentScreen:
        return PageTransition(
            child: ReferentScreen(), type: PageTransitionType.fade);
      case splashScreen:
        return PageTransition(
            child: SplashScreen(), type: PageTransitionType.fade);
      case loginScreen:
        return PageTransition(
            child: LoginScreen(), type: PageTransitionType.fade);
      case questionScreen:
        return PageTransition(
            child: QuestionScreen(), type: PageTransitionType.rightToLeft);
      case registerScreen:
        return PageTransition(
            child: RegisterScreen(
              ref: settings.arguments.toString(),
            ),
            type: PageTransitionType.rightToLeft);
      case inputPasswordRegisterScreen:
        RegisterUserModel? registerUserModel;
        if (settings.arguments is RegisterUserModel) {
          registerUserModel = settings.arguments as RegisterUserModel;
        }
        return PageTransition(
            child: InputPasswordRegisterScreen(
                registerUserModel: registerUserModel),
            type: PageTransitionType.rightToLeft);
      case inputOTPScreen:
        InputOTPArg? inputOTPArg;
        if (settings.arguments is InputOTPArg) {
          inputOTPArg = settings.arguments as InputOTPArg;
        }
        return PageTransition(
            child: InputOTPScreen(inputOTPArg: inputOTPArg),
            type: PageTransitionType.rightToLeft);
      case packageDueScreen:
        return PageTransition(
            child: PackageDueScreen(), type: PageTransitionType.rightToLeft);
      case listPackageUnfinishedScreen:
        return PageTransition(
            child: ListPackageUnfinishedScreen(),
            type: PageTransitionType.rightToLeft);
      case forgotPasswordScreen:
        return PageTransition(
            child: ForgotPasswordScreen(email: settings.arguments.toString()),
            type: PageTransitionType.rightToLeft);
      case inputPasswordForgotScreen:
        RegisterUserModel? registerUserModel;
        if (settings.arguments is RegisterUserModel) {
          registerUserModel = settings.arguments as RegisterUserModel;
        }
        return PageTransition(
            child:
                InputPasswordForgotScreen(registerUserModel: registerUserModel),
            type: PageTransitionType.rightToLeft);
      case mainInvestScreen:
        return PageTransition(
            child: MainInvestScreen(), type: PageTransitionType.rightToLeft);
      case detailProductScreen:
        ProductModel? productModel;
        if (settings.arguments is ProductModel) {
          productModel = settings.arguments as ProductModel;
        }
        return PageTransition(
            child: DetailProductScreen(
              productModel: productModel,
            ),
            type: PageTransitionType.rightToLeft);
      case detailInvestScreen:
        return PageTransition(
            child: DetailInvestScreen(), type: PageTransitionType.rightToLeft);
      case detailHistoryScreen:
        HistoryModel? historyModel;
        if (settings.arguments is HistoryModel) {
          historyModel = settings.arguments as HistoryModel;
        }
        return PageTransition(
            child: DetailHistoryScreen(
              historyModel: historyModel,
            ),
            type: PageTransitionType.rightToLeft);
      case dailyProfitScreen:
        int? packId;
        if (settings.arguments is int) {
          packId = settings.arguments as int;
        }
        return PageTransition(
            child: DailyProfitScreen(
              packId: packId,
            ),
            type: PageTransitionType.rightToLeft);
      case openOrderScreen:
        ProductModel? productModel;
        if (settings.arguments is ProductModel) {
          productModel = settings.arguments as ProductModel;
        }
        return PageTransition(
            child: OpenOrderScreen(
              productModel: productModel,
            ),
            type: PageTransitionType.rightToLeft);
      case openOrderStockScreen:
        ProductModel? productModel;
        if (settings.arguments is ProductModel) {
          productModel = settings.arguments as ProductModel;
        }
        return PageTransition(
            child: OpenOrderStockScreen(
              productModel: productModel,
            ),
            type: PageTransitionType.rightToLeft);
      case userInfoScreen:
        return PageTransition(
            child: UserInfoScreen(), type: PageTransitionType.rightToLeft);
      case editUserInfoScreen:
        return PageTransition(
            child: EditUserInfoScreen(), type: PageTransitionType.rightToLeft);
      case editBankInfoScreen:
        BankOfCustomerModel? bankInfoModel;
        if (settings.arguments is BankOfCustomerModel) {
          bankInfoModel = settings.arguments as BankOfCustomerModel;
        }
        return PageTransition(
            child: EditBankInfoScreen(
              bankInfoModel: bankInfoModel,
            ),
            type: PageTransitionType.rightToLeft);
      case listBankOfUserScreen:
        return PageTransition(
            child: ListBankOfUserScreen(),
            type: PageTransitionType.rightToLeft);
      case borrowMoneyScreen:
        ProductModel? productModel;
        if (settings.arguments is ProductModel) {
          productModel = settings.arguments as ProductModel;
        }
        return PageTransition(
            child: BorrowMoneyScreen(
              productModel: productModel,
            ),
            type: PageTransitionType.rightToLeft);
      case borrowStockScreen:
        ProductModel? productModel;
        if (settings.arguments is ProductModel) {
          productModel = settings.arguments as ProductModel;
        }
        return PageTransition(
            child: BorrowStockScreen(
              productModel: productModel,
            ),
            type: PageTransitionType.rightToLeft);
      case securityInfoScreen:
        return PageTransition(
            child: SecurityInfoScreen(), type: PageTransitionType.rightToLeft);
      case changePasswordScreen:
        return PageTransition(
            child: ChangePasswordScreen(),
            type: PageTransitionType.rightToLeft);
      case customWebView:
        CustomWebViewArg? customWebViewArg;
        if (settings.arguments is CustomWebViewArg) {
          customWebViewArg = settings.arguments as CustomWebViewArg;
        }
        return PageTransition(
            child: CustomWebView(
              customWebViewArg: customWebViewArg,
            ),
            type: PageTransitionType.fade);
      case pdfViewWidget:
        PDFViewWidgetArg? arg;
        if (settings.arguments is PDFViewWidgetArg) {
          arg = settings.arguments as PDFViewWidgetArg;
        }
        return PageTransition(
            child: PDFViewWidget(
              pdfViewWidgetArg: arg,
            ),
            type: PageTransitionType.fade);
      case pdfHDViewWidget:
        PDFHDViewWidgetArg? arg;
        if (settings.arguments is PDFHDViewWidgetArg) {
          arg = settings.arguments as PDFHDViewWidgetArg;
        }
        return PageTransition(
            child: PDFHDViewWidget(
              pdfViewWidgetArg: arg,
            ),
            type: PageTransitionType.fade);
      case listNotificationScreen:
        return PageTransition(
            child: ListNotificationScreen(), type: PageTransitionType.fade);
      default:
        return MaterialPageRoute(builder: (context) => Container());
    }
  }
}
