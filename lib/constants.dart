import 'dart:ui';

class Constants {
  static const PASSWORD_MIN_LENGTH = 8;
  static const PASSWORD_MAX_LENGTH = 15;
  // locale
  static const String VI = "vi";
  static const String EN = "en";
  static const SUPPORT_LOCALE = [Locale(VI), Locale(EN)];
}

class FormatDate {
  static const String full = "dd/MM/yyyy HH:mm:ss";
  static const String HH_mm_dd_mm_yy = "HH:mm dd/MM/yyyy";
  static const String dayMonthYear = "dd/MM/yyyy";
  static const String formatByServer = "yyyyMMdd";
  static const String hhMMss = "HH:mm ss";
  static const String formatTimeServer = "yyyy-MM-ddTHH:mm:ss";
  static const String yyyyMMdd = "yyyyMMdd";
}