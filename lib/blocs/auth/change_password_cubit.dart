import 'package:app_sstock/data/model/user_model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../base_bloc/base_state.dart';

class ChangePasswordCubit extends Cubit<BaseState> {
  ChangePasswordCubit() : super(InitState());

  void changePassword(String oldPass, String newPass) async {
    try {
      emit(LoadingState());
      UserModel user = await SharedPreferenceUtil.getUserInfo();
      Map<String, dynamic> param = {"oldPassword": oldPass, "newPassword": newPass, "uid": user.id};
      ApiResponse response = await Network.instance.post(url: ApiConstant.doiMatKhau, body: param);
      if (response.isSuccess) {
        emit(LoadedState<bool>(true));
      } else {
        emit(ErrorState(response.message));
      }
    } catch (e) {
      emit(ErrorState("system_error"));
    }
  }
}

class ForgotPasswordCubit extends Cubit<BaseState> {
  ForgotPasswordCubit() : super(InitState());

  void sendMail(String Email) async {
    try {
      emit(LoadingState());
      Map<String, dynamic> param = {
        "Email": Email,
      };
      ApiResponse response = await Network().post(url: ApiConstant.QuenMatKhau, body: param);
      if (response.isSuccess) {
        if (response.data["status"] == 200) {
          emit(LoadedState<bool>(true));
        } else {
          emit(ErrorState(response.data["errMsg"] ?? response.message));
        }
      } else {
        emit(ErrorState(response.message));
      }
    } catch (e) {
      emit(ErrorState("system_error"));
    }
  }
}
