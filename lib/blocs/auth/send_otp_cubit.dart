import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SendOTPCubit extends Cubit<BaseState> {
  SendOTPCubit() : super(InitState());

  void sendOtp({
    String? mobilePhone,
    String? email,
  }) async {
    try {
      emit(LoadingState());
      Map<String, dynamic> map = {};
      map["email"] = email;
      map["mobilePhone"] = mobilePhone;
      ApiResponse response = await AuthRepository.sendOTP(map);
      if (response.isSuccess) {
        emit(LoadedState<int>(response.data["ExpriceInMinute"]));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
