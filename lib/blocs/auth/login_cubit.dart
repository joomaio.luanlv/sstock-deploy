import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/utils/device_util.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginCubit extends Cubit<BaseState> {
  LoginCubit() : super(InitState());

  void loginWithUserName(
    String? email,
    String? password,
  ) async {
    try {
      emit(LoadingState());
      Map<String, dynamic> map = {};
      map["username"] = email;
      map["password"] = password;
      map["firebaseToken"] = await DeviceUtil.getFcmToken();
      ApiResponse response = await AuthRepository.login(map);
      if (response.isSuccess) {
        UserModel userModel = UserModel.fromMap(response.data["Customer"]["Data"]);
        await SharedPreferenceUtil.saveToken(response.data["Token"]["Value"]);
        await SharedPreferenceUtil.saveUserInfo(userModel);
        emit(LoadedState<UserModel>(userModel));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}

class ActionOTPCubit extends Cubit<BaseState> {
  ActionOTPCubit() : super(InitState());

  void register(RegisterUserModel? registerUserModel) async {
    try {
      emit(LoadingState());
      ApiResponse response = await AuthRepository.register(registerUserModel?.toJson());
      if (response.isSuccess) {
        emit(LoadedState(true));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }

  void forgotPass(RegisterUserModel? registerUserModel) async {
    try {
      emit(LoadingState());
      ApiResponse response = await AuthRepository.resetPasword({
        "username": registerUserModel?.username,
        "otp": registerUserModel?.otp,
        "password": registerUserModel?.password
      });
      if (response.isSuccess) {
        emit(LoadedState(true));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
