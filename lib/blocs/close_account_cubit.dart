import 'dart:developer';

import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/custom_dialog.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CloseAccountCubit extends Cubit<BaseState> {
  CloseAccountCubit() : super(InitState());

  void closeAccount(BuildContext context) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.closeAccount();
      if (response.error == 0) {
        SharedPreferenceUtil.clearData();
        Navigator.pushReplacementNamed(context, Routes.loginScreen);
      } else {
        CustomDialog.showDialogConfirm(
          context,
          title: "Không thể đóng tài khoản",
          showCancel: true,
          content: "${response.message ?? "error.common"}",
          titleOK: "Xem chi tiết",
          onTapOK: () {
            Navigator.pushNamed(context, Routes.listPackageUnfinishedScreen);
          }
        );
        emit(LoadedState(true));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
