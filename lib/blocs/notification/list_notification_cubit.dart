import 'package:app_sstock/blocs/base_bloc/base_state.dart';
import 'package:app_sstock/blocs/base_list_cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/model/notification_model.dart';
import 'package:app_sstock/data/network/api_response.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/utils/navigator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListNotificationCubit extends BaseListCubit<BaseState> {
  ListNotificationCubit() : super(InitState());
  Map<String, dynamic> paramRequest = {};

  void getList(
      {RefreshController? refresh,
      Map<String, dynamic>? paramRequest,
      bool isRefresh = false,
      bool isLoadMore = false}) async {
    if (!isLoadMore) {
      super.setDefaultConfig(refresh);
    }
    if (!super.canLoadMore()) {
      return;
    }
    if (paramRequest != null) {
      this.paramRequest = paramRequest;
    }
    List<NotificationModel> dataListCurrent = super.getListDataCurrent<NotificationModel>();
    try {
      if (!isRefresh && !isLoadMore) {
        emit(LoadingState());
      }
      ApiResponse response = await (NotificationRepository.getListNofication());
      if (response.isSuccess) {
        updateTotalUnreade(response);
        List<NotificationModel> listData = NotificationModel.mapToList(response.data["Notifies"]["Data"]);
        if (!isLoadMore) {
          dataListCurrent = [];
        }
        dataListCurrent.addAll(listData);
        emit(LoadedState(dataListCurrent, timeEmit: DateTime.now()));
      } else {
        emit(LoadedState(dataListCurrent, msgError: response.message ?? "", timeEmit: DateTime.now()));
      }
    } catch (e) {
      print("===e =====${e}");
      emit(LoadedState(dataListCurrent, msgError: "error.common", timeEmit: DateTime.now()));
    } finally {
      super.finishLoad();
    }
  }

  void doReadNotification(int index, NotificationModel data) {
    if (state is LoadedState<List<NotificationModel>>) {
      var list = (state as LoadedState<List<NotificationModel>>).data;
      if (data.isViewed == false) {
        list[index].isViewed = true;
        emit(LoadedState(list, timeEmit: DateTime.now()));
        decrTotalUnread();
        watchNotification(data.id);
      }
    }
  }

  void watchNotification(id) {
    try {
      NotificationRepository.readListNofication(id);
    } catch (e) {}
  }

  void updateTotalUnreade(ApiResponse<dynamic> response) {
    try {
      int total = response.data["TotalUnSeen"] ?? 0;
      BlocProvider.of<TotalUnreadNotifyCubit>(NavigationService.instance.navigatorKey.currentContext!)
          .updateTotalUnread(total);
    } catch (e) {}
  }

  void decrTotalUnread() {
    try {
      BlocProvider.of<TotalUnreadNotifyCubit>(NavigationService.instance.navigatorKey.currentContext!)
          .decrTotalUnread();
    } catch (e) {}
  }
}

class TotalUnreadNotifyCubit extends Cubit<BaseState> {
  TotalUnreadNotifyCubit() : super(InitState());

  void getTotalUnread() async {
    ApiResponse response = await (NotificationRepository.getListNofication());
    if (response.isSuccess) {
      int total = response.data["TotalUnSeen"] ?? 0;
      emit(LoadedState<int>(total, timeEmit: DateTime.now()));
    }
  }

  void updateTotalUnread(int total) async {
    try {
      emit(LoadedState<int>(total, timeEmit: DateTime.now()));
    } catch (e) {}
  }

  void decrTotalUnread() async {
    try {
      if (state is LoadedState<int>) {
        int total = (state as LoadedState<int>).data;
        if (total > 0) {
          total -= 1;
          emit(LoadedState<int>(total, timeEmit: DateTime.now()));
        }
      }
    } catch (e) {}
  }
}
