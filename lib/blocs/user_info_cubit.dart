import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserInfoCubit extends Cubit<BaseState> {
  UserInfoCubit() : super(InitState());

  void getUserInfo() async {
    UserModel user = await SharedPreferenceUtil.getUserInfo();
    try {
      emit(LoadedState<UserModel>(user, timeEmit: DateTime.now()));
      ApiResponse response = await AuthRepository.getUserInfo(user.id);
      if (response.isSuccess) {
        user = UserModel.fromMap(response.data["CustomerInfo"]["Data"]);
        await SharedPreferenceUtil.saveUserInfo(user);
        emit(LoadedState(user, timeEmit: DateTime.now()));
      } else {
        emit(LoadedState(user, timeEmit: DateTime.now()));
      }
    } catch (e) {
      emit(LoadedState(user, timeEmit: DateTime.now()));
    }
  }
}

class UpdateUserInfoCubit extends Cubit<BaseState> {
  UpdateUserInfoCubit() : super(InitState());

  void updateUserInfo(Map<String, dynamic> params) async {
    try {
      emit(LoadingState());
      ApiResponse response = await AuthRepository.updateUserInfo(params);
      if (response.isSuccess) {
        emit(LoadedState(true));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}

class CreateOrEditOfCusBankInfoCubit extends Cubit<BaseState> {
  CreateOrEditOfCusBankInfoCubit() : super(InitState());

  void CreateOrEditOfCusBankInfo(Map<String, dynamic> params) async {
    try {
      emit(LoadingState());
      ApiResponse response = await AuthRepository.CreateOrEditOfCusBankInfo(params);
      if (response.isSuccess) {
        emit(LoadedState(true));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
