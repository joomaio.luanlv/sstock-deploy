import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListBankInfoCubit extends Cubit<BaseState> {
  ListBankInfoCubit() : super(InitState());

  void getListBankInfo() async {
    List<BankInfoModel> list = [];
    try {
      list = await SharedPreferenceUtil.getListBankInfo();
      emit(LoadedState(list, timeEmit: DateTime.now()));
      BaseOptions options =
          BaseOptions(connectTimeout: timeOutRequest, receiveTimeout: timeOutRequest, baseUrl: ApiConstant.vietqr);
      final Dio _dio = Dio(options);
      Response response = await _dio.get(ApiConstant.listBank);
      list = BankInfoModel.listFromMap(response.data["data"]);
      await SharedPreferenceUtil.saveListBankInfo(response.data["data"]);
      emit(LoadedState(list, timeEmit: DateTime.now()));
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
