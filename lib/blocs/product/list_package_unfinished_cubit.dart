import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListPackageUnfinishedCubit extends Cubit<BaseState> {
  ListPackageUnfinishedCubit() : super(InitState());

  void getListPackageUnfinished() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.listPackageUnfinished();
      if (response.isSuccess) {
        List<HistoryModel> list = HistoryModel.listFromMap(response.data["ListNotComplete"])
                .toList();
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}