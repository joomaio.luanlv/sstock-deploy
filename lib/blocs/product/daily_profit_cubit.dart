import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DailyProfitCubit extends Cubit<BaseState> {
  DailyProfitCubit() : super(InitState());

  void getListDailyProfit(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getDailyProfit(param);
      if (response.isSuccess) {
        List<DailyProfitModel> list = DailyProfitModel.listFromMap(response.data["DailyProfit"])
                .toList();
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}