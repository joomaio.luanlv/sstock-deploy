import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PolicyProductCubit extends Cubit<BaseState> {
  PolicyProductCubit() : super(InitState());

  void getListPolicy(ProductModel? productModel) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getPolicyProduct(productModel?.id);
      if (response.isSuccess) {
        List<PolicyModel> list = PolicyModel.listFromMap(response.data["Documents"]["Data"]);
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      //emit(ErrorState("error.common"));
    }
  }
}