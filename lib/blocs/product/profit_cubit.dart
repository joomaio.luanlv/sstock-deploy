import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfitCubit extends Cubit<BaseState> {
  ProfitCubit() : super(InitState());

  void getProfit(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getProfit(param);
      if (response.isSuccess) {
        emit(LoadedState(response.data, timeEmit: DateTime.now()));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}