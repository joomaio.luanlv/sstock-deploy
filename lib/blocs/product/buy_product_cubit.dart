import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BuyProductCubit extends Cubit<BaseState> {
  BuyProductCubit() : super(InitState());

  void buyProduct(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.buyProduct(param);
      if (response.isSuccess) {
        emit(LoadedState(true));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }

  void getContract(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getContract(param);
      if (response.isSuccess) {
        Contract contract = Contract.fromMap(response.data["contract"]);
        emit(LoadedState(contract, timeEmit: DateTime.now()));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}

class Contract {
  final String? contractLink;
  final String? code;

  Contract({
    this.contractLink,
    this.code,
  });

  factory Contract.fromMap(Map<String, dynamic> json) => Contract(
        contractLink: json["contractLink"],
        code: json["code"],
      );
}
