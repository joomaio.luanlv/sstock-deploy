import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListQuestionCubit extends Cubit<BaseState> {
  ListQuestionCubit() : super(InitState());

  void getListQuestion() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getListQuestion();
      var rs = response.isSuccess;
      if (response.isSuccess) {
        List<QuestionModel> listQS = QuestionModel.listFromMap(response.data["ListQuestion"]).toList();
        emit(LoadedState(listQS));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
