import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CancelRequestCubit extends Cubit<BaseState> {
  CancelRequestCubit() : super(InitState());

  //Hủy yêu cầu
  void cancelRequest(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.cancelRequest(param);
      if (response.isSuccess) {
        emit(LoadedState(1));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }

  //Bán chứng khoán
  void soldStock(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.soldStock(param);
      if (response.isSuccess) {
        emit(LoadedState(2));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }

  //Rút tài khoản
  void withdraw(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.withdraw(param);
      if (response.isSuccess) {
        emit(LoadedState(3));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
