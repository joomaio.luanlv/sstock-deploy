import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExpectedProfitCubit extends Cubit<BaseState> {
  ExpectedProfitCubit() : super(InitState());

  void getExpectedProfit(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getExpectedProfit(param);
      if (response.isSuccess) {
        emit(LoadedState(response.data['InterestPeriod']));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}