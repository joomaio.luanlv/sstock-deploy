import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PackageDueProductCubit extends Cubit<BaseState> {
  PackageDueProductCubit() : super(InitState());

  void getListPackageDueProduct(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.listPackageDue(param);
      if (response.isSuccess) {
        List<PackageDueModel> list = PackageDueModel.listFromMap(response.data["GuessInterest"])
                .toList();
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
