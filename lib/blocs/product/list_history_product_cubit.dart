import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:app_sstock/utils/shared_preference.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListHistoryProductCubit extends Cubit<BaseState> {
  ListHistoryProductCubit() : super(InitState());

  void getListHistoryProduct(Map<String, dynamic> param) async {
    List<HistoryModel>? list;
    try {
      list = await SharedPreferenceUtil.getListHistory();
      emit(LoadedState(list, showLoading: true, timeEmit: DateTime.now()));
      ApiResponse response = await ProductRepository.listHistoryProduct(param);
      if (response.isSuccess) {
        list = HistoryModel.listFromMap(response.data["History"]);

        await SharedPreferenceUtil.saveListHistory(response.data["History"]);

        emit(LoadedState(list, showLoading: false, timeEmit: DateTime.now()));
      } else {
        emit(LoadedState(list,
            showLoading: false,
            msgError: response.message ?? "error.common",
            timeEmit: DateTime.now()));
      }
    } catch (e) {
      //emit(LoadedState(list, showLoading: false, timeEmit: DateTime.now()));
    }
  }
}
