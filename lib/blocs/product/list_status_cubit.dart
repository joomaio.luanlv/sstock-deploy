import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListStatusCubit extends Cubit<BaseState> {
  ListStatusCubit() : super(InitState());

  void getListStatus() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getListStatus();
      if (response.isSuccess) {
        List<StatusModel> list =
            StatusModel.listFromMap(response.data["ListStatus"]).toList();
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
