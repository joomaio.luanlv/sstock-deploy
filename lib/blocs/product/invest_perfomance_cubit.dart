import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InvestPerfomanceCubit extends Cubit<BaseState> {
  InvestPerfomanceCubit() : super(InitState());

  void getInvestPerfomance(Map<String, dynamic> param) async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getInvestPerfomance(param);
      if (response.isSuccess) {
        List<ChartInfoModel> data =
            ChartInfoModel.listFromMap(response.data["Peformance"]);
        emit(LoadedState(data));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
