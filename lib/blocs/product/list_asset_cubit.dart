import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListAssetCubit extends Cubit<BaseState> {
  ListAssetCubit() : super(InitState());

  void getListAsset() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.listAsset();

      if (response.isSuccess) {
        List<AssetModel> list = AssetModel.listFromMap(response.data["assets"]).toList();

        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}

class ListInvestingCubit extends Cubit<BaseState> {
  ListInvestingCubit() : super(InitState());
  void getInvesting() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.listAsset();

      if (response.isSuccess) {
        List<InvestingModel> listInvesting =
            InvestingModel.listFromMap(response.data["investing"]).toList();

        emit(LoadedState(listInvesting));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}
