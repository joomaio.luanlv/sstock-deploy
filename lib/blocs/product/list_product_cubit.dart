import 'package:app_sstock/blocs/base_bloc/base.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/data/network/network.dart';
import 'package:app_sstock/data/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListProductCubit extends Cubit<BaseState> {
  ListProductCubit() : super(InitState());

  void getListProduct() async {
    try {
      emit(LoadingState());
      ApiResponse response = await ProductRepository.getListProduct();
      if (response.isSuccess) {
        List<ProductModel> list = ProductModel.listFromMap(response.data["ListProduct"]["Data"])
                .where((element) => element.active == true)
                .toList();
        emit(LoadedState(list));
      } else {
        emit(ErrorState<String>(response.message ?? "error.common"));
      }
    } catch (e) {
      emit(ErrorState("error.common"));
    }
  }
}

class DetailProductCubit extends Cubit<BaseState> {
  DetailProductCubit() : super(InitState());

  void getDetailProduct(ProductModel? productModel, int uId) async {
    try {
      emit(LoadedState<ProductModel?>(productModel, showLoading: true, timeEmit: DateTime.now()));
      ApiResponse response = await ProductRepository.getDetailProduct(productModel?.id, uId);
      if (response.isSuccess) {
        ProductModel? productModel = ProductModel.fromMap(response.data["Product"]["Data"]);
        InterestRateProductModel? interestRateProductModel = InterestRateProductModel.fromMap(response.data["Chart"]["Data"][0]);
        
        productModel.interestRateProductModel = interestRateProductModel;
        emit(LoadedState(productModel, showLoading: false, timeEmit: DateTime.now()));
        return;
      }
      // else {
      //   emit(ErrorState<String>(response.message ?? "error.common"));
      // }

    } catch (e) {
      print("===getDetailProduct =====${e}");
      // emit(ErrorState("error.common"));
    }
    emit(LoadedState(productModel, showLoading: false, timeEmit: DateTime.now()));
  }
}
