import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/screen/screen.dart';
import 'package:app_sstock/ui/widget/custom_dialog.dart';
import 'package:flutter/material.dart';

class NavigationService {
  NavigationService._internal();

  static final NavigationService instance = NavigationService._internal();

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  pushReplacement(String routeName) {
    navigatorKey.currentState?.popUntil((route) => route.isFirst);
    navigatorKey.currentState?.pushReplacementNamed(routeName);
  }

  popToRootView() {
    navigatorKey.currentState?.popUntil((route) => route.isFirst);
  }

  pop() {
    navigatorKey.currentState?.pop();
  }

  popWithParam(param) {
    navigatorKey.currentState?.pop(param);
  }

  bool isShowDialogTokenExpired = false;

  showDialogTokenExpired() {
    if (isShowDialogTokenExpired == true) {
      return;
    }
    BuildContext? context = navigatorKey.currentState?.overlay?.context;
    if (context == null) {
      return;
    }
    isShowDialogTokenExpired = true;
    CustomDialog.showDialogConfirm(context,
            content: "token_expired_message", showCancel: false, showIconClose: false, titleOK: "Đồng ý")
        .then((value) {
      isShowDialogTokenExpired = false;
      //logout(context);
      pushReplacement(Routes.loginScreen);
    });
  }
}
