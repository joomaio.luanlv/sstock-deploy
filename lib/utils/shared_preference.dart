import 'dart:convert';

import 'package:app_sstock/data/model/model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SPrefCache {
  // share preference key
  static const String KEY_TOKEN = "auth_token";
  static const String PREF_KEY_LANGUAGE = "pref_key_language";
  static const String PREF_KEY_USER_INFO = "pref_key_user_info";
  static const String PREF_KEY_HISTORY_PRODUCT = "pref_key_history_product";
  static const String PREF_KEY_LIST_BANK_INFO = "pref_key_list_bank_info";
  static const String PREF_KEY_IS_KEEP_LOGIN = "pref_key_is_keep_login";
  static const String PREF_KEY_PACKAGE_DUE = "pref_key_package_due";
}

class SharedPreferenceUtil {
  static Future saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SPrefCache.KEY_TOKEN, token);
  }

  static Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(SPrefCache.KEY_TOKEN) ?? '';
  }

  static Future saveKeepLogin(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(SPrefCache.PREF_KEY_IS_KEEP_LOGIN, value);
  }

  static Future<bool> isKeepLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(SPrefCache.PREF_KEY_IS_KEEP_LOGIN) ?? false;
  }

  static Future saveUserInfo(UserModel user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SPrefCache.PREF_KEY_USER_INFO, json.encode(user.toMap()));
  }

  static Future<UserModel> getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = prefs.getString(SPrefCache.PREF_KEY_USER_INFO);
    if (data == null) {
      return UserModel(id: -1);
    }
    return UserModel.fromMap(json.decode(data));
  }

  static Future saveListHistory(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SPrefCache.PREF_KEY_HISTORY_PRODUCT, json.encode(data));
  }

  static Future<List<HistoryModel>?> getListHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = prefs.getString(SPrefCache.PREF_KEY_HISTORY_PRODUCT);
    if (data == null) {
      return null;
    }
    return HistoryModel.listFromMap(json.decode(data));
  }

  static Future savePackageDue(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(
        SPrefCache.PREF_KEY_PACKAGE_DUE, json.encode(data));
  }
  static Future<List<PackageDueModel>?> getListPackageDue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = prefs.getString(SPrefCache.PREF_KEY_PACKAGE_DUE);
    if (data == null) {
      return null;
    }
    return PackageDueModel.listFromMap(json.decode(data));
  }

  static Future saveListBankInfo(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SPrefCache.PREF_KEY_LIST_BANK_INFO, json.encode(data));
  }

  static Future<List<BankInfoModel>> getListBankInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = prefs.getString(SPrefCache.PREF_KEY_LIST_BANK_INFO);
    if (data == null) {
      return [];
    }
    return BankInfoModel.listFromMap(json.decode(data));
  }

  static Future clearData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }
}
