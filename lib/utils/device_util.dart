import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:app_sstock/blocs/cubit.dart';
import 'package:app_sstock/data/model/model.dart';
import 'package:app_sstock/routes.dart';
import 'package:app_sstock/ui/widget/widget.dart';
import 'package:app_sstock/utils/navigator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class DeviceUtil {
  static String FCM_TOPIC_COMMON = "FCM_TOPIC_COMMON";

  static String getOS() {
    if (Platform.isAndroid) {
      return "Android";
    } else if (Platform.isIOS) {
      return "iOS";
    }
    return "";
  }

  static Future<String?> getFcmToken() async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
    String? token = await _firebaseMessaging.getToken();
    return token;
  }

  static void fcmSubscribe(String topic) async {
    try {
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
      await _firebaseMessaging.subscribeToTopic(topic);
    } catch (e) {
      print("===fcmSubscribe =====${e}");
    }
  }

  static void fcmUnSubscribe(String topic) async {
    try {
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
      await _firebaseMessaging.unsubscribeFromTopic(topic);
    } catch (e) {
      print("===fcmSubscribe =====${e}");
    }
  }

  static int _lastOnResumeCall = 0;
  static String? _lastUniqId;

  static Future<void> listenerNotification(BuildContext context) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.requestPermission();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse: (NotificationResponse details) {
      if (details.payload != null) {
        var dataExtend;
        try {
          dataExtend = jsonDecode(details.payload!)["data"];
        } catch (e) {}
        handleNavigation(
            context, NotificationModel.fromMap(jsonDecode(details.payload!)),
            dataExtend: dataExtend);
      }
      return;
    });

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      print("===getInitialMessage =====${message}");
      if (message != null) {
        var dataExtend;
        try {
          dataExtend = (message.data)["data"];
        } catch (e) {}
        handleNavigation(context, NotificationModel.fromMap(message.data),
            dataExtend: dataExtend);
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      // print('Message data: ${message.data}');
      // log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      Future _showNotificationWithDefaultSound(RemoteMessage message) async {
        try {
          var title = message.notification!.title;
          var body = message.notification!.body;
          // StyleInformation styleInformation = await getStyleInformation(message.notification, message.data);
          var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
              'com.develop.vitus', 'com.develop.vitus',
              // styleInformation: styleInformation,
              playSound: true,
              enableVibration: true,
              importance: Importance.max,
              styleInformation: BigTextStyleInformation(''),
              priority: Priority.high);
          var iOSPlatformChannelSpecifics = new DarwinNotificationDetails();
          var platformChannelSpecifics = new NotificationDetails(
              android: androidPlatformChannelSpecifics,
              iOS: iOSPlatformChannelSpecifics);
          var flutterLocalNotificationsPlugin =
              new FlutterLocalNotificationsPlugin();
          await flutterLocalNotificationsPlugin.show(
            DateTime.now().millisecond,
            title,
            body,
            platformChannelSpecifics,
            payload: jsonEncode(message.data),
          );
        } catch (e) {
          print("===_showNotificationWithDefaultSound =====${e}");
        }
      }

      if (_lastUniqId != message.messageId) {
        _lastUniqId = message.messageId;
        try {
          var context = NavigationService.instance.navigatorKey.currentContext!;
          if (checkToShowNotification(context, message.data)) {
            _showNotificationWithDefaultSound(message);
            BlocProvider.of<TotalUnreadNotifyCubit>(context).getTotalUnread();
            BlocProvider.of<ListNotificationCubit>(context).getList();
          }
        } catch (e) {}
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("===onMessageOpenedApp =====${message}");
      if (message != null) {
        var dataExtend;
        try {
          dataExtend = (message.data)["data"];
        } catch (e) {}
        handleNavigation(context, NotificationModel.fromMap(message.data),
            dataExtend: dataExtend);
      }
    });
  }

  static void fcmSubscribeListTopic(BuildContext context) {
    // TODO @nambuidanh fcmSubscribeListTopic
    // BaseState state = BlocProvider.of<UserInfoCubit>(context).state;
    // if (state is LoadedState<LoginResponse>) {
    //   var user = state.data;
    //   user?.packageUserModel?.notifySubs?.forEach((element) {
    //     if (element != null && element.isNotEmpty) {
    //       DeviceUtil.fcmSubscribe(element);
    //     }
    //   });
    //   user?.packageUserModel?.notifyUnsubs?.forEach((element) {
    //     if (element != null && element.isNotEmpty) {
    //       DeviceUtil.fcmUnSubscribe(element);
    //     }
    //   });
    // }
  }

  static void changeModeUI(bool isDark) {
    // Future.delayed(Duration.zero,(){
    //   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //       statusBarColor: Colors.transparent,
    //       statusBarIconBrightness: isDark ? Brightness.dark: Brightness.light,
    //       statusBarBrightness: isDark ? Brightness.light: Brightness.dark));
    // });
  }

  static void handleNavigation(BuildContext context, NotificationModel? data,
      {bool fromListNotification = false,
      Function? getArguments,
      dynamic dataExtend}) async {
    try {
      try {
        BlocProvider.of<ListNotificationCubit>(context)
            .watchNotification(data?.id);
      } catch (e) {}
      final String? type = data?.type?.toString();

      // 1		Mở nội dung hàng này</option>
      // 2		Mở lịch đào tạo (chỗ mega sale)
      // 3		Mở quà tặng (chỗ mega sale)
      // 4		Mở zalo (chỗ mega sale)
      // 5		Mở gợi ý dành cho bạn (chỗ trang chủ)
      // 6		Mở danh sách top bán hàng</option>
      // 7		Mở danh sách top nhập hàng</option>
      // 8		Mở lịch sử giao dịch< khi có các giao dịch mua, bán các gói từ trial từ 2 trở lên và các gói mega sale
      // 9		Mở màn hình F1 tuyến dưới <khi có thằng mới nhập ID hoặc link gt thành công>
      // 10		Mở đơn hàng < khi có oder>
      // 11		Mở mega sale trial < khi có giao dịch số lương 1 trial, nếu lớn hơn 1 thì vào lịch sử giao dịch>
      // 12		Mở mega salle < khi có giao dịch mega sale>
      // 13   mở Webiew gắn link
      // 14  mở ra app khác ( zalo ,zoom ) gắn link
      // <option value=""15"">Mở mega lịch sử tin nhắn</option>
      // <option value=""16"">Mở lịch đào tạo</option>
      // <option value=""17"">Mở quy trình đào tạo</option>
      // <option value=""18"">Mở màn hình chat</option>"
      //   19	Mở lích sử giao dịch nạp tiền <khi có thông báo nạp tiền thành công>
      // 20	Mở lịch sử giao dịch rút tiền <khi có thông báo rút tiền về tài khoản thành công>
      // 21	Lịch sử giao dịch rút tiền <bấm vào thông báo rút tiền ở trạng thái đang chờ duyệt>
      // 22	Mở thông tin định danh ví <khi gửi thông tin lên admin và admin duyệt>
      // 23	Mở lịch sử giao dịch được cộng tiền <khi tuyến dưới mua hàng>
      // 24	Mở lịch sử giao dịch trừ tiền <khi mua hàng>
      switch (type) {
        case "1":
          Navigator.pushNamed(context, Routes.mainInvestScreen);
          break;
        default:
        // Navigator.pushNamed(
        //   context,
        //   Routes.listNotificationScreen,
        // );
      }
    } catch (e) {
      print("==x =====${e}");
    }
  }

  static bool checkToShowNotification(
      BuildContext context, Map<String, dynamic> data) {
    return true;
  }
}
