import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  // define all your color
  static const Color white = Color(0xffffffff);
  static const Color black = Color(0xff000000);
  static const Color base_color = Color(0xFF00A95E);
  static const Color ff63C856 = Color(0xff63C856);
  static const Color base_color_border_textfield = Color(0xFFD6DCE2);
  static const Color gray = Color(0xFF75818F);
  static const Color colorTitle = Color(0xFF49494A);
  static const Color border = Color(0xFFD4CFCF);
  static const Color ffBDBDBD = Color(0xFFBDBDBD);
  static const Color ff828282 = Color(0xFF828282);
  static const Color disable = Color(0xffF3F3F3);
  static const Color ffDBDBDB = Color(0xffDBDBDB);
  static const Color ff404A69 = Color(0xff404A69);
  static const Color ffF3F3F3 = Color(0xffF3F3F3);
  static const Color ff585858 = Color(0xff585858);
  static const Color ffff2e00 = Color(0xffff2e00);
  static const Color ffD6D6D6 = Color(0xffD6D6D6);
  static const Color ff222222 = Color(0xff222222);
  static const Color ff00A95E = Color(0xff00A95E);
  static const Color ffC8FFC1 = Color(0xffC8FFC1);
  static const Color ffDBF2D7 = Color(0xffDBF2D7);
  static const Color ffF0FFEE = Color(0xffF0FFEE);
  static const Color ffB2171F = Color(0xffB2171F);
  static const Color ffEBEEF2 = Color(0xffEBEEF2);
  static const Color ff999999 = Color(0xff999999);
  static const Color ffFFA11A = Color(0xffFFA11A);
  static const Color ff258EFF = Color(0xFF258EFF);
  static const Color ffF0084F4 = Color(0xFF0084F4);
  static const Color ff23262B = Color(0xff23262B);

  static Color getColorFromHex(String? hexColor) {
    try {

      if(hexColor == null){
        return base_color;
      }
      hexColor = hexColor.toUpperCase().replaceAll("#", "");
      if (hexColor.length == 6) {
        hexColor = "FF" + hexColor;
      }
      return Color(int.parse(hexColor, radix: 16));
    } catch (e) {
      print("===getColorFromHex =====${e}");
    }
    return base_color;
  }

  static const LinearGradient base_color_gradient = LinearGradient(colors: [
    Color(0xFF258EFF),
    Color(0xFF0D49FF),
  ], begin: FractionalOffset.topCenter, end: FractionalOffset.bottomCenter, tileMode: TileMode.mirror);
}
